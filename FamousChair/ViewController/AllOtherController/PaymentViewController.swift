//
//  PaymentViewController.swift
//  FamousChair
//
//  Created by ginger webs on 10/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import Braintree
import PassKit
import SquarePointOfSaleSDK
class PaymentViewController: UIViewController {
    
    @IBOutlet weak var lbl_headerProviderName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLbael: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    
    var serviceStr = String()
    var dateStr = String()
    var timeStr = String()
    var priceStr  = String()
    var providerId = Int()
    var serviceId = Int()
    var providerNameStr = String()
    var selectedPaymentMode = ""
    var payamentArray =  ["paypal","credit-card-template-png","venmo_logo_blue","square","Zelle","g-pay","apple-pay","cash-app"]
    
    var braintreeClient: BTAPIClient!
    var venmoDriver : BTVenmoDriver?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDataUI()
        PaymentVM.callPaypalTokenApi(params: [String : Any](), viewController: self) { (response) in
            self.braintreeClient = BTAPIClient(authorization: response.token ?? "")
        }
        
        
    }
    
    func setDataUI() {
        lbl_headerProviderName.text = providerNameStr
        serviceLabel.text = serviceStr
        priceLbael.text = "$\(priceStr) and up"
        timeLabel.text = timeStr
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date  = formatter.date(from: dateStr)!
        formatter.dateFormat = "EEEE, MMM d"
        dateLabel.text = formatter.string(from: date)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func editButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func paypalCheckout() {
        let payPalDriver = BTPayPalDriver(apiClient: braintreeClient!)
        payPalDriver.viewControllerPresentingDelegate = self
        payPalDriver.appSwitchDelegate = self // Optional
        
        LoaderClass.startLoader()
        
        // Specify the transaction amount here. "2.32" is used in this example.
        let request = BTPayPalRequest(amount: priceStr)
        request.currencyCode = "USD" // Optional; see BTPayPalRequest.h for more options
        
        payPalDriver.requestOneTimePayment(request) { (tokenizedPayPalAccount, error) in
            LoaderClass.stopLoader()
            if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                print("Got a nonce: \(tokenizedPayPalAccount.nonce)")
                
                DispatchQueue.main.async {
                    let parameter = ["provider_id":"\(self.providerId)","service_id":"\(self.serviceId)","booking_slot":self.timeStr,"payment_type":"2","booking_date":self.dateStr,"nonce":tokenizedPayPalAccount.nonce]
                    BookingVM.callBookingServicesApi(params: parameter, viewController: self, completion: {})
                }
            } else if error != nil {
                print(error)
               
            } else {
                print("Buyer canceled payment approval")
            }
        }
    }
    
    func venmoCheckout() {
        LoaderClass.startLoader()
        self.braintreeClient = BTAPIClient(authorization: "sandbox_9qr57wst_q6876692zwbfxjhs")
        self.venmoDriver = BTVenmoDriver(apiClient: braintreeClient)
        self.venmoDriver?.authorizeAccountAndVault(false, completion: { (venmoAccount, error) in
            LoaderClass.stopLoader()
            guard let venmoAccount = venmoAccount else {
                print("Error: \(error)")
                return
            }
             print(venmoAccount.nonce)
            let parameter = ["provider_id":"\(self.providerId)","service_id":"\(self.serviceId)","booking_slot":self.timeStr,"payment_type":"5","booking_date":self.dateStr,"nonce":venmoAccount.nonce]
                BookingVM.callBookingServicesApi(params: parameter, viewController: self, completion: {})
           
           
            
        })
        
    }
    
    func applePayCheckout() {
        let paymentNetworks = [PKPaymentNetwork.amex, .discover, .masterCard, .visa]
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
            let request = PKPaymentRequest()
            request.currencyCode = "USD"
            request.countryCode = "US"
            request.merchantIdentifier = "merchant.com.famouschair.user"
            request.merchantCapabilities = PKMerchantCapability.capability3DS
            request.supportedNetworks = paymentNetworks // 5
            let paymentItem = PKPaymentSummaryItem.init(label: "Product", amount: NSDecimalNumber(value: Double(priceStr) ?? 0.0))
            request.paymentSummaryItems = [paymentItem] // 6
            guard let paymentVC = PKPaymentAuthorizationViewController(paymentRequest: request) else {
                self.createAlert(title: "Error", message: "Unable to present Apple Pay authorization.")
                return
            }
                paymentVC.delegate = self
                self.present(paymentVC, animated: true, completion: nil)
        } else {
            self.createAlert(title: "Error", message: "Unable to make Apple Pay transaction.")
        }
    }

    
    
    func squareCheckout(){
        let callbackURL = URL(string: "famouschairuser://callback")!
        //sandbox-sq0idb-_OJ0r9YGTUyPw6Ka38htsA
        //sq0idp-r4LpaXz9NiR8T85khm8p8w
        SCCAPIRequest.setApplicationID("sq0idp-r4LpaXz9NiR8T85khm8p8w")
        var apiRequest = SCCAPIRequest()
        do {
            // Specify the amount of money to charge.
            let money = try SCCMoney(amountCents: 100, currencyCode: "USD")
            
            // Create the request.
            apiRequest =
                try SCCAPIRequest(
                    callbackURL: callbackURL,
                    amount: money,
                    userInfoString: nil,
                    locationID: nil,
                    notes: "Product",
                    customerID: nil,
                    supportedTenderTypes: .all,
                    clearsDefaultFees: false,
                    returnsAutomaticallyAfterPayment: false,
                    disablesKeyedInCardEntry: false,
                    skipsReceipt: false
            )
        }catch let error as NSError {
            print(error.localizedDescription)
        }
        
        // Open Point of Sale to complete the payment.
        do{
            try SCCAPIConnection.perform(apiRequest)
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
    }
    
    @IBAction func confirmBooking(_ sender: Any) {
        //        provider_id,service_id,booking_slot,payment_type,booking_date
        if selectedPaymentMode == ""{
            self.createAlert(title: "Famous Chair", message: "Please select payment type (Only cash method is available)")
        }else{
            let parameter = ["provider_id":"\(providerId)","service_id":"\(serviceId)","booking_slot":timeStr,"payment_type":"1","booking_date":dateStr]
            BookingVM.callBookingServicesApi(params: parameter, viewController: self, completion: {})
        }
    }
}

extension PaymentViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return payamentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "debitCell", for: indexPath) as! PaymentTableViewCell
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTableViewCell", for: indexPath) as! PaymentTableViewCell
            cell.paymentImage.image = UIImage(named: "\(payamentArray[indexPath.row])")
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "Famous-Chair", message: "Are you sure! Do you want to make payment", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
            if indexPath.row == 0{
                self.paypalCheckout()
            }else if indexPath.row == 2{
                self.venmoCheckout()
            }else if indexPath.row == 3{
                self.squareCheckout()
            }else if indexPath.row == 6{
                self.applePayCheckout()
            }
            else if indexPath.row == 7{
                self.createAlert(title: "Famous Chair", message: "Cash mode selected successfully")
                self.selectedPaymentMode = "Cash"
            }else{
                self.createAlert(title: "Famous Chair", message: "Coming soon...")
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
}


extension PaymentViewController: BTAppSwitchDelegate,BTViewControllerPresentingDelegate {
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        print("Presenting")
    }
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        print("Dismissed")
    }
    
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
        
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
        
    }
    
    
}


extension PaymentViewController:
PKPaymentAuthorizationViewControllerDelegate{
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
//        let parameter = ["provider_id":"\(providerId)","service_id":"\(serviceId)","booking_slot":timeStr,"payment_type":"4","booking_date":dateStr]
//        BookingVM.callBookingServicesApi(params: parameter, viewController: self, completion: {})
        
        completion(PKPaymentAuthorizationResult(status: PKPaymentAuthorizationStatus.success, errors: []))
    }
}
