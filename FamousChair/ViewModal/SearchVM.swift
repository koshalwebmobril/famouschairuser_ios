//
//  SearchVM.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class SearchVM: NSObject {
    
     class func callSearchApi(params: [String:Any],viewController:UIViewController, completion: @escaping (SearchModal) -> Void)  {
                   ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.search_url, dict: params,viewController: viewController) { (response) in
                    if let data = response{
                       
                       let searchData :SearchModal = SearchModal.init(data:JSON(data))
                       if searchData.error == "success"{
                           completion(searchData)
                       }else{
                           viewController.createAlert(title: "Famous Chair", message: searchData.message ?? "")
                       }
                    }
           }
       }

    
    
}
