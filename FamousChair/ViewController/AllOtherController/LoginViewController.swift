//
//  LoginViewController.swift
//  FamousChair
//
//  Created by ginger webs on 06/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var loginView: UIView!
    
       var loginVM : LoginVM?

    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextfield.delegate = self
        passwordTextfield.delegate = self
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
       navigationItem.leftBarButtonItem = backButton
         loginVM = LoginVM()
        loginVM?.controller = self
        
        loginView.layer.masksToBounds =  true
        loginView.layer.cornerRadius = 20
        loginView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]

    }
    
     override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(true, animated: true)

     }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        loginVM?.validations(email: emailTextfield.text ?? "", password: passwordTextfield.text ?? "", controller: self)
    }
    

    @IBAction func appleButtonAction(_ sender: Any) {
        loginVM?.googleLogin()
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    @IBAction func facebookButton(_ sender: Any) {
        loginVM?.facebookLogin()
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    

    
}
  
//MARK : Textfield delegate
extension LoginViewController:  UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
            let _char = string.cString(using: String.Encoding.utf8)
            let isBackSpace: Int = Int(strcmp(_char, "\\b"))
            if isBackSpace == -92 {
                return true
            }
            
            if textField.text!.isEmpty && string == " " {
                return false
            }
            
            return true
        }
}
