//
//  SearchViewController.swift
//  FamousChair
//
//  Created by ginger webs on 07/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchImgView: UIImageView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    var isFromSearch = true
    var address = String()
    var searchText = String()
    var searchData : SearchModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addressLabel.text = address
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(searchAction))
        searchImgView.addGestureRecognizer(tapGesture)
        
        if !isFromSearch {
            searchViewHeight.constant = 0.0
            searchView.isHidden = true
            let parameter = ["search_string":searchText]
            SearchVM.callSearchApi(params: parameter, viewController: self) { (responseObject) in
                self.searchData = responseObject
                if self.searchData?.result.count ?? 0 > 0{
                    self.tableView.isHidden = false
                    self.blankView.isHidden = true
                }else{
                    self.tableView.isHidden = true
                    self.blankView.isHidden = false
                }
                
                self.tableView.reloadData()
                
            }
                
            }
        
        
    }
    
    @objc func searchAction(){
        txtSearch.resignFirstResponder()
        if txtSearch.text!.isEmpty{
            self.createAlert(title: "Famous Chair", message: "Please enter search keyword")
        }else{
            
            let parameter = ["search_string":txtSearch.text ?? ""]
            SearchVM.callSearchApi(params: parameter, viewController: self) { (responseObject) in
                self.searchData = responseObject
                if self.searchData?.result.count ?? 0 > 0{
                    self.tableView.isHidden = false
                    self.blankView.isHidden = true
                }else{
                    self.tableView.isHidden = true
                    self.blankView.isHidden = false
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text!.isEmpty{
            self.createAlert(title: "Famous Chair", message: "Please enter search keyword")
        }else{
            
            let parameter = ["search_string":txtSearch.text ?? ""]
            SearchVM.callSearchApi(params: parameter, viewController: self) { (responseObject) in
                self.searchData = responseObject
                if self.searchData?.result.count ?? 0 > 0{
                    self.tableView.isHidden = false
                    self.blankView.isHidden = true
                }else{
                    self.tableView.isHidden = true
                    self.blankView.isHidden = false
                }
                self.tableView.reloadData()
                
            }
        }
        return true
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func checkAvailbilityAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        
        
        let ProviderVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
        ProviderVC.providerId = searchData?.result[indexPath?.row ?? 0].providerId ?? 0
        self.navigationController?.pushViewController(ProviderVC, animated: true)
    }
    
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchData?.result.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as! SearchTableViewCell
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(searchData?.result[indexPath.row].image ?? "")", imageView: cell.providerImgview,placeHolder: "barberImage")
        cell.providerName.text = searchData?.result[indexPath.row].name ?? ""
        cell.ratingView.rating = searchData?.result[indexPath.row].rating ?? 0.0
        cell.descriptionLabel.text = searchData?.result[indexPath.row].serviceName ?? ""
        cell.priceLabel.text = String(format: "$%.2f", searchData?.result[indexPath.row].price ?? 0.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
}
