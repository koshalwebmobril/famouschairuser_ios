//
//  AddNewPostVC.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 21/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Photos
import ImglyKit
import PhotoEditorSDK

class AddNewPostVC: UIViewController {
    
    @IBOutlet weak var imgView_placeholder: UIImageView!
    @IBOutlet weak var txt_post_title: UITextField!
   
    var imagePicker = UIImagePickerController()
//    var selectedItems = [YPMediaItem]()
    var thumbnailImage = UIImage()
    var thumb_data = Data()
    var post_data = Data()
    var thumb_name = ""
    var isImageSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(uploadImageOrVideo))
        imgView_placeholder.addGestureRecognizer(tapGesture)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareNowAction(_ sender: Any) {
        if !isImageSelected{
            self.createAlert(title: "Famous Chair", message: "Please select image or video.")
        }else{
            let postTxt = encode(txt_post_title.text ?? "")
            let parameter = ["post_title":postTxt]
            InstagramVM.callPostSharApi(params: parameter, post_path: post_data, imageName: "post_path", thumbnail: thumb_name, thumbnail_data: thumb_data, viewController: self, completion: {})
        }
    }
    
    
    @IBAction func uploadAction(_ sender: Any) {
        uploadImageOrVideo()
    }
    
    @objc func uploadImageOrVideo() {
      let configuration = Configuration { builder in
        // Setup global colors
        builder.theme.backgroundColor = .white
        builder.theme.menuBackgroundColor = UIColor.lightGray
        self.customizeCameraController(builder)
        self.customizePhotoEditorViewController(builder)
      }

      let cameraViewController = CameraViewController(configuration: configuration)
      cameraViewController.modalPresentationStyle = .fullScreen
      cameraViewController.locationAccessRequestClosure = { locationManager in
        locationManager.requestWhenInUseAuthorization()
      }

      // Set a global tint color, that gets inherited by all views
      if let window = UIApplication.shared.delegate?.window! {
        window.tintColor = .baseColor
      }

      cameraViewController.completionBlock = { [unowned cameraViewController] image, url in
        if let image = image {
          let photo = Photo(image: image)
          let photoEditModel = cameraViewController.photoEditModel
          cameraViewController.present(self.createCustomizedPhotoEditViewController(with: photo, configuration: configuration, and: photoEditModel), animated: true, completion: nil)
        }
        if let data = url{
            let photoEditModel = cameraViewController.photoEditModel
            let videoEditor = VideoEditViewController(videoAsset: Video(url: data), configuration: configuration, photoEditModel: photoEditModel)
            videoEditor.modalPresentationStyle = .fullScreen
            videoEditor.delegate = self
            cameraViewController.present(videoEditor, animated: true, completion: nil)
        }
      }

      cameraViewController.dataCompletionBlock = { [unowned cameraViewController] data in
        if let data = data {
          let photo = Photo(data: data)
          let photoEditModel = cameraViewController.photoEditModel
          cameraViewController.present(self.createCustomizedPhotoEditViewController(with: photo, configuration: configuration, and: photoEditModel), animated: true, completion: nil)
        }
      }

      present(cameraViewController, animated: true, completion: nil)
    }
    fileprivate func customizeCameraController(_ builder: ConfigurationBuilder) {
      builder.configureCameraViewController { options in
        // Enable/Disable some features
        options.cropToSquare = false
        options.showFilterIntensitySlider = false
        options.tapToFocusEnabled = true
        options.showCancelButton = true

        // Use closures to customize the different view elements
        options.cameraRollButtonConfigurationClosure = { button in
          button.layer.borderWidth = 2.0
            button.layer.borderColor = UIColor.baseColor.cgColor
        }

        options.timeLabelConfigurationClosure = { label in
            label.textColor = .baseColor
        }

        options.recordingModeButtonConfigurationClosure = { button, _ in
          button.setTitleColor(UIColor.gray, for: .normal)
            button.setTitleColor(.baseColor, for: .selected)
        }

      }
    }

    fileprivate func customizePhotoEditorViewController(_ builder: ConfigurationBuilder) {
      // Customize the main editor
      builder.configurePhotoEditViewController { options in
        options.titleViewConfigurationClosure = { titleView in
          if let titleLabel = titleView as? UILabel {
            titleLabel.text = "Selfie-Editor"
          }
        }

        options.actionButtonConfigurationClosure = { cell, _ in
          cell.contentTintColor = UIColor.baseColor
        }
      }
    }
    private func createCustomizedPhotoEditViewController(with photo: Photo, configuration: Configuration, and photoEditModel: PhotoEditModel) -> PhotoEditViewController {
       let photoEditViewController = PhotoEditViewController(photoAsset: photo, configuration: configuration, photoEditModel: photoEditModel)
       photoEditViewController.modalPresentationStyle = .fullScreen
       photoEditViewController.view.tintColor = UIColor(red: 0.11, green: 0.44, blue: 1.00, alpha: 1.00)
       photoEditViewController.toolbar.backgroundColor = UIColor.gray
       photoEditViewController.delegate = self

       return photoEditViewController
     }
    
    
    func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
}

extension AddNewPostVC:UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count ?? 0 == 200{
            return false
        }
        return true
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .fullScreen
            imagePicker.mediaTypes = ["public.image", "public.movie"]
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = ["public.image", "public.movie"]
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String else {return}
        
        print(mediaType)
        if mediaType == "public.image"{
            self.thumb_name = ""
            self.thumb_data = Data()
            let image = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
            self.imgView_placeholder.image = image
            self.post_data = image.jpegData(compressionQuality: 0.4) ?? Data()
            
        }else{
            self.thumb_name = "thumbnail"
            if let url = (info[UIImagePickerController.InfoKey.mediaURL] as? URL) {
                do{self.post_data = try Data(contentsOf: url)}catch{print("data to url conversion failed")}
                ApiManager.getThumbnailImageFromVideoUrl(url: url) { (thumbnailImage) in
                    if let image = thumbnailImage{
                        self.imgView_placeholder.image = image
                        self.thumb_data = image.jpegData(compressionQuality: 0.4) ?? Data()
                    }
                }
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}


// YPImagePickerDelegate
extension AddNewPostVC: PhotoEditViewControllerDelegate, VideoEditViewControllerDelegate  {
    
    
    func shouldAddToSelection(indexPath: IndexPath, numSelections: Int) -> Bool {
        return true// indexPath.row != 2
    }
    
    func videoEditViewController(_ videoEditViewController: VideoEditViewController, didFinishWithVideoAt url: URL?) {
        if let urlStr = url{
            ApiManager.getThumbnailImageFromVideoUrl(url: urlStr) { (thumbnailImage) in
                if let image = thumbnailImage{
                    self.imgView_placeholder.image = image
                    self.imgView_placeholder.contentMode = .scaleAspectFill
                    self.thumb_data = image.jpegData(compressionQuality: 0.4) ?? Data()
                }
            }
            
            self.isImageSelected = true
            self.thumb_name = "thumbnail"
            do{self.post_data = try Data(contentsOf: urlStr)
            }catch{print("data to url conversion failed")}
            self.dismiss(animated: true, completion: nil)
        }
        
        print("saved")
    }
    
    func videoEditViewControllerDidFailToGenerateVideo(_ videoEditViewController: VideoEditViewController) {
        print("edit")
    }
    
    func videoEditViewControllerDidCancel(_ videoEditViewController: VideoEditViewController) {
        videoEditViewController.dismiss(animated: true, completion: nil)
        print("cancel")
    }
    
    func photoEditViewController(_ photoEditViewController: PhotoEditViewController, didSave image: UIImage, and data: Data) {
        self.thumb_name = ""
        self.thumb_data = Data()
        self.isImageSelected = true
        self.imgView_placeholder.image = image
        self.imgView_placeholder.contentMode = .scaleAspectFill
        self.post_data = data
        self.dismiss(animated: true, completion: nil)
        print("saved")
    }
    
    func photoEditViewControllerDidFailToGeneratePhoto(_ photoEditViewController: PhotoEditViewController) {
        print("edit")
    }
    
    func photoEditViewControllerDidCancel(_ photoEditViewController: PhotoEditViewController) {
        photoEditViewController.dismiss(animated: true, completion: nil)
        print("cancel")
    }
}


