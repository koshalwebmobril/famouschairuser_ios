//
//  StaticData.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import UIKit
class ServiceVM: NSObject {
    
    class func callStaticDataApi(params: [String:Any],viewController:UIViewController, completion: @escaping (StaticModal) -> Void)  {
                ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.staticData, dict: params,viewController: viewController) { (response) in
                if let responseData = response {
                    let staticData = try! JSONDecoder().decode(StaticModal.self, from: responseData)
                    if staticData.error == "success" {
                        completion(staticData)
                    } else {
       
                }
            }
        }
    }
}
