//
//  NotificationModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 18/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class NotificationModal : NSObject {
    let error : String?
    let message :String?
    var arr_notification = [notificationData]()
   
    init(data:JSON) {
        self.error = data["status"].stringValue
        self.message = data["message"].stringValue
        for item in data["data"].arrayValue {
            let value : notificationData = notificationData.init(item: item)
            self.arr_notification.append(value)
        }
    }

}

class notificationData: NSObject {
    let title : String?
    let message :String?
    let date : String?
    
    init(item:JSON) {
        self.title = item["title"].stringValue
        self.message = item["message"].stringValue
        self.date = item["created_at"].stringValue
    }
    
}
