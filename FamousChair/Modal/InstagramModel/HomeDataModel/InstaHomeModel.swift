//
//  InstaHomeModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 21/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import SwiftyJSON
class InstaHomeModel: NSObject {
    let code : Int?
    let message : String?
    let status : Bool?
    var arr_homeData = [InstaHomeData]()
    
    init(response:JSON) {
        self.code = response["code"].intValue
        self.message = response["message"].stringValue
        self.status = response["status"].boolValue
        for data in response["data"].arrayValue{
            let value :InstaHomeData = InstaHomeData.init(data: data)
            if value.hide_status ?? 0 == 0{
            arr_homeData.append(value)
            }
        }
    }

}

class InstaHomeData: NSObject {
    
    let id : Int?
    let posted_userId : Int?
    let user_id :Int?
    let login_userId : Int?
    let post_path : String?
    let thumbnail : String?
    let status : Int?
    var total_like_count : Int?
    let name : String?
    let email : String?
    let device_token : String?
    let hide_status : Int?
    let profile_image : String?
    let paymen_status : String?
    var user_like_status : Int?
    var post_title : String?
    var share_count :Int?
    var comment_count :Int?
    init(data:JSON) {
        self.id = data["id"].intValue
        self.posted_userId = data["posted_user_id"].intValue
        self.user_id = data["user_id"].intValue
        self.login_userId = data["login_user_id"].intValue
        self.post_path = data["post_path"].stringValue
        self.thumbnail = data["thumbnail"].stringValue
        self.status = data["status"].intValue
        self.total_like_count = data["total_like_count"].intValue
        self.name = data["name"].stringValue
        self.email = data["email"].stringValue
        self.device_token = data["device_token"].stringValue
        self.hide_status = data["hide_status"].intValue
        self.profile_image = data["profile_image"].stringValue
        self.paymen_status = data["paymen_status"].stringValue
        self.user_like_status = data["user_like_status"].intValue
        self.post_title = data["post_title"].stringValue
        self.share_count = data["share_count"].intValue
        self.comment_count = data["comment_count"].intValue
    }
}
