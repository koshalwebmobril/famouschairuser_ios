//
//  ne.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 26/11/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

import Foundation
struct UserListLiveData : Codable {
    let id : Int?
    let login_user_id : Int?
    let following_id : Int?
    let name : String?
    let email : String?
    let mobile : String?
    let dob : String?
    let gender : String?
    let profile_img : String?
    let device_token : String?
    let device_type : String?
    let token : String?
    let channelname : String?
    let payment_status : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case login_user_id = "login_user_id"
        case following_id = "following_id"
        case name = "name"
        case email = "email"
        case mobile = "mobile"
        case dob = "dob"
        case gender = "gender"
        case profile_img = "profile_image"
        case device_token = "device_token"
        case device_type = "device_type"
        case token = "token"
        case channelname = "channelname"
        case payment_status = "payment_status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        login_user_id = try values.decodeIfPresent(Int.self, forKey: .login_user_id)
        following_id = try values.decodeIfPresent(Int.self, forKey: .following_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        profile_img = try values.decodeIfPresent(String.self, forKey: .profile_img)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        channelname = try values.decodeIfPresent(String.self, forKey: .channelname)
        payment_status = try values.decodeIfPresent(String.self, forKey: .payment_status)
    }

}
