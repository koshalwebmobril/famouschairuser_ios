//
//  SocialLoginModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 07/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
struct SocialLoginModal : Codable {
    let error : Bool?
    let message : String?
    let result : socialData?

    enum CodingKeys: String, CodingKey {

        case error = "error"
        case message = "message"
        case result = "result"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        error = try values.decodeIfPresent(Bool.self, forKey: .error)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent(socialData.self, forKey: .result)
    }

}
