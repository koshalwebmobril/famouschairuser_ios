//
//  AllCategoriesViewController.swift
//  FamousChair
//
//  Created by Tannu on 15/07/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class AllCategoriesViewController: UIViewController {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionview: UICollectionView!
    var address = String()
    var categoryData : HomeModal?
    override func viewDidLoad() {
        super.viewDidLoad()
        addressLabel.text = address
        let parameter = [String:Any]()
        HomeVM.callGetAllServicesApi(params: parameter, viewController: self) { (responseObject) in
            self.categoryData = responseObject
            self.scrollView.isHidden = false
            self.collectionview.reloadData()
        }
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVC.isFromSearch = true
        searchVC.address = addressLabel.text ?? ""
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    
}

extension AllCategoriesViewController: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryData?.result.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllCatagoriesCollectionViewCell", for: indexPath) as! AllCatagoriesCollectionViewCell
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(categoryData?.result[indexPath.row].image ?? "")", imageView: cell.category_imageView,placeHolder: "barberImage")
        cell.categoty_name.text = categoryData?.result[indexPath.row].name ?? ""
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let providerVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        providerVC.searchText = categoryData?.result[indexPath.row].name ?? ""
        providerVC.isFromSearch = false
        providerVC.address = addressLabel.text ?? ""
        self.navigationController?.pushViewController(providerVC, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((collectionView.frame.size.width / 2) - 80), height: 150.0 )
    }
    
    
}
