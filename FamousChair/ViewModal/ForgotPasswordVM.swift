//
//  LoginVM.swift
//  FamousChair
//
//  Created by ginger webs on 01/07/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVM: NSObject {
  //  static let shared = ForgotPasswordVM()
    var controller = ForgotPasswordViewController()
    var constant = WebServiceApi()
    var emailValue = String()
    //MARKL: Validations
    
    func validationOnScreen(email: String) {
        emailValue = email
         if email == "" {
            controller.createAlert(title: "Famous Chair", message: "Please enter email address")
       
         } else if !email.isValidEmail() {
            controller.createAlert(title: "Famous Chair", message: "Please enter valid email address")
        
         } else {
            let param = ["email" : email,"user_type":"2"]
               forgotApi(param: param)
    
         }
}

}

extension ForgotPasswordVM {
    
    func forgotApi(param : [String : String]) {
        ApiManager.shared.postApiRequest(url: WebServiceApi.forgotPassword, dict: param,viewController: controller) { (response) in
            if let responseData = response {
                let forgotData = try! JSONDecoder().decode(ForgotPasswordModal.self, from: responseData)
                if forgotData.status == "success" {
                self.controller.saveUserId(id: forgotData.result?.id ?? 0)
           //     self.controller.saveToken(token: (forgotData.result?.token ?? ""))
                let Vc = mainStoryBoard.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
                    Vc.email = self.emailValue
                     self.controller.navigationController?.pushViewController(Vc, animated: true)
                } else {
                    self.controller.createAlert(title: "Famous Chair", message: forgotData.message ?? "")
                    
            }
        }
    }
}
}
