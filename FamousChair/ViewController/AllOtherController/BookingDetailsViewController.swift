//
//  BookingDetailsViewController.swift
//  FamousChair
//
//  Created by ginger webs on 11/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class BookingDetailsViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var reviewBtn: UIButton!
    @IBOutlet weak var providerLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var bookingId = String()
    var serviceId = String()
    var providerId = String()
    var isFromHome = false
    
    var bookingData : BookingModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromHome{
            reviewBtn.isHidden = true
        }else{
            reviewBtn.isHidden = false
        }
        
        BookingVM.callBookingDetailsServicesApi(params: bookingId, viewController: self) {(responseObject) in
            self.bookingData = responseObject
            self.setDataUI()
        }
    }
    
    func setDataUI(){
        mainView.isHidden = false
        providerLabel.text = bookingData?.bookingData?.providerName ?? ""
        timeLabel.text = bookingData?.bookingData?.timeSlot ?? ""
        if self.bookingData?.ratingData?.rating ?? 0.0 != 0.0{
            reviewBtn.setTitle("View Review", for: .normal)
        }
        //1=>Accepted,2=>Declined,3=>Pending,4=>Release or closed
        if bookingData?.bookingData?.booking_status ?? 0 == 1{
            reviewBtn.isHidden = true
            statusLabel.text = "Accepted"
        }else if bookingData?.bookingData?.booking_status ?? 0 == 2{
            statusLabel.text = "Cancelled"
            cancelBtn.isHidden = true
            reviewBtn.isHidden = true
        }else if bookingData?.bookingData?.booking_status ?? 0 == 3{
            reviewBtn.isHidden = true
            statusLabel.text = "Pending"
        }else{
            cancelBtn.isHidden = true
            reviewBtn.isHidden = false
            statusLabel.text = "Closed"
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date  = formatter.date(from: bookingData?.bookingData?.bookingDate ?? "")!
        formatter.dateFormat = "EEEE, MMM d"
        
        dateLabel.text = formatter.string(from: date)
        priceLabel.text = "$\(bookingData?.bookingData?.price ?? "")"
        serviceLabel.text = bookingData?.bookingData?.serviceName ?? ""
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(bookingData?.bookingData?.image ?? "")", imageView: imageView, placeHolder: "")
        
        
    }

    @IBAction func cancelBooking(_ sender: Any) {
//        booking_id,status(2=>cancelled)
        
        let alert = UIAlertController(title: "Famous Chair", message: "Are you sur! Do you want to cancel booking?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
            let parameter = ["booking_id":self.bookingId,"status":"2"]
            BookingVM.callCancelBookingServicesApi(params: parameter, viewController: self, completion: {})
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitReview(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewsViewController") as! ReviewsViewController
        vc.serviceId = self.serviceId
        vc.providerId = self.providerId
        vc.reviewData = self.bookingData?.ratingData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
