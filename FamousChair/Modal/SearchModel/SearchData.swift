//
//  SearchData.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class SearchData :NSObject {
    var id : Int?
    var name:String?
    var image : String?
    var price : Double?
    var rating : Double?
    var serviceName : String?
    var providerId : Int?
    
    init(data:JSON) {
        self.id = data["id"].intValue
        self.name = data["provider_name"].stringValue
        self.image = data["provider_image"].stringValue
        self.serviceName = data["service_name"].stringValue
        self.providerId = data["provider_id"].intValue
        self.rating = data["rating_pont"].doubleValue
        self.price = data["price"].doubleValue
    }
    override init() {
        
    }
   
}
