//
//  AlertMessages.swift
//  FamousChair
//
//  Created by ginger webs on 01/07/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import UIKit

//Screen Size
let screenSize = UIScreen.main.bounds
let screenHeight = screenSize.height
let screenWidth = screenSize.width

//Alert Messages
struct AlertMessages {
    static let alertTitle = "Famous Chair"

    static let invalidEmail = "Please enter valid email address."
    static let emptyEmail = "Please enter email address."
    static let invalidOTP = "Please enter valid OTP."

    static let emptyPassword = "Please enter password."
    static let limitPassword = "Password should be atleast 8 characters long."
    
    static let firstName = "Please enter first name."
    static let lastName = "Please enter last name."
    static let gender = "Please select gender."
    

    static let emptyConfirmPassword = "Please enter confirm password."
    static let passwordsDoNotMatch = "Password and confirm password doesn't match"
    static let changePasswordsDoNotMatch = "Change password and confirm password doesn't match"

    static let emptyPhoneNumber = "Please enter phone number."
    static let limitPhoneNumber = "Please enter valid phone number."
    
    static let location = "Please enter location"
    static let noProfileImage = "Please choose a profile image."
    static let emptyOldPassword = "Please enter current password."
    static let emptyNewPassword = "Please enter new password."
    static let passwordChanged = "Your password has been changed successfully."
    static let emlptyName = "Please enter name."
}
