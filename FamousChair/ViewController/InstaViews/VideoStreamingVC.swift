//
//  VideoStreamingVC.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 15/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import AgoraRtcKit



class VideoStreamingVC: UIViewController {
    @IBOutlet weak var containerView: AGEVideoContainer!
    
    private var agoraKit =  AgoraRtcEngineKit()
    
    private var isSwitchCamera = true {
        didSet {
            agoraKit.switchCamera()
        }
    }
    
    private var videoSessions = [VideoSession]() {
        didSet {
            updateBroadcastersView()
        }
    }
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadAgoraKit()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    deinit {
        leaveChannel()
    }
    

}

// MARK: - AgoraRtcEngineKit
private extension VideoStreamingVC {
    func loadAgoraKit() {
        // Step 1, set delegate
        agoraKit.delegate = self
        // Step 2, set communication mode
        agoraKit.setChannelProfile(.communication)
        
        // Step 3, enable the video module
        agoraKit.enableVideo()
       
       
        agoraKit.startPreview()
        
      
        // Step 5, join channel and start group chat
        // If join  channel success, agoraKit triggers it's delegate function
        // 'rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int)'
        agoraKit.joinChannel(byToken: KeyCenter.Token, channelId: "Sandeep", info: nil, uid: 0, joinSuccess: nil)
        setIdleTimerActive(false)
    }
    
    
    func setIdleTimerActive(_ active: Bool) {
        UIApplication.shared.isIdleTimerDisabled = !active
    }
   
    
    // Update views
    func updateBroadcastersView() {
        // video views layout
        if videoSessions.count == 1 {
            containerView.reload(level: 0, animated: true)
        } else {
            var rank: Int
            var row: Int
            
            if videoSessions.count == 0 {
                containerView.removeLayout(level: 0)
                return
            } else if videoSessions.count == 1 {
                rank = 1
                row = 1
            } else if videoSessions.count == 2 {
                rank = 1
                row = 2
            } else {
                rank = 2
                row = Int(ceil(Double(videoSessions.count) / Double(rank)))
            }
            
            let itemWidth = CGFloat(1.0) / CGFloat(rank)
            let itemHeight = CGFloat(1.0) / CGFloat(row)
            let itemSize = CGSize(width: itemWidth, height: itemHeight)
            let layout = AGEVideoLayout(level: 0)
                        .itemSize(.scale(itemSize))
            
            containerView
                .listCount { [unowned self] (_) -> Int in
                    return self.videoSessions.count
                }.listItem { [unowned self] (index) -> UIView in
                    return self.videoSessions[index.item].hostingView
                }
            
            containerView.setLayouts([layout], animated: true)
        }
    }
    
    func leaveChannel() {
        // Step 1, release local AgoraRtcVideoCanvas instance
        agoraKit.setupLocalVideo(nil)
        // Step 2, leave channel and end group chat
        agoraKit.leaveChannel(nil)
        // Step 3, please attention, stop preview after leave channel
        agoraKit.stopPreview()
        
      setIdleTimerActive(true)
    }
    
    
    
}


// MARK: - AgoraRtcEngineDelegate
extension VideoStreamingVC: AgoraRtcEngineDelegate {
    
    /// Occurs when the local user joins a specified channel.
    /// - Parameters:
    ///   - engine: the Agora engine
    ///   - channel: channel name
    ///   - uid: User ID. If the uid is specified in the joinChannelByToken method, the specified user ID is returned. If the user ID is not specified when the joinChannel method is called, the server automatically assigns a uid.
    ///   - elapsed: Time elapsed (ms) from the user calling the joinChannelByToken method until the SDK triggers this callback.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
        print( "Join channel: \(channel)")
    }
    
    /// Occurs when the connection between the SDK and the server is interrupted.
    func rtcEngineConnectionDidInterrupted(_ engine: AgoraRtcEngineKit) {
        print("Connection Interrupted")
    }
    
    /// Occurs when the SDK cannot reconnect to Agora’s edge server 10 seconds after its connection to the server is interrupted.
    func rtcEngineConnectionDidLost(_ engine: AgoraRtcEngineKit) {
        print("Connection Lost")
    }
    
    /// Reports an error during SDK runtime.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print( "Occur error: \(errorCode.rawValue)")
    }
    
   
   
    
    
}
