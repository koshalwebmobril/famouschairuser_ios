//
//  ProfileVM.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import UIKit
class ProfileVM: NSObject {
    
    class func callGetProfileApi(params: [String:Any],viewController:UIViewController, completion: @escaping (ProfileModal) -> Void)  {
                ApiManager.shared.getHeaderRequestApi(url: WebServiceApi.getProfile_url, dict: params,viewController: viewController) { (response) in
                if let responseData = response {
                    let profileData = try! JSONDecoder().decode(ProfileModal.self, from: responseData)
                    if profileData.error == "success" {
                        completion(profileData)
                    } else {
       
                }
            }
        }
    }

   class func callUpdateApi(param:[String:Any],imageData:Data,imageName:String,viewController:UIViewController){
    ApiManager.shared.uploadImageApi(urlmethod: WebServiceApi.updateProfile_url, parameter: param, userImage: imageData, imageName: imageName,viewController: viewController) { (response) in
        if let responseData = response {
            let updateData = try! JSONDecoder().decode(ContactModal.self, from: responseData)
            if updateData.error == "success" {
                let alert = UIAlertController(title: "Famous Chair", message: updateData.message ?? "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                    
                    viewController.navigationController?.popViewController(animated: true)
                }))
                viewController.present(alert, animated: true, completion: nil)
            } else {
                viewController.createAlert(title: "Famous Chair", message: updateData.message ?? "")
            }
        }
    }
    }

}
