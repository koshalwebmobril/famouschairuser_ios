//
//  ProviderVM.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 04/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class ProviderVM: NSObject {
    class func callGetproviderServicesApi(params: [String:Any],viewController:UIViewController, completion: @escaping (ProviderModal) -> Void)  {
                ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.getProviderServices_url, dict: params,viewController: viewController) { (response) in
                 if let data = response{
                    
                    let providerData :ProviderModal = ProviderModal.init(data:JSON(data))
                    if providerData.error == "success"{
                        completion(providerData)
                    }else{
                        viewController.createAlert(title: "Famous Chair", message: providerData.message ?? "")
                    }
                 }
        }
    }
    
    class func callServicesDetailsApi(params: [String:Any],viewController:UIViewController, completion: @escaping (CalenderModel) -> Void)  {
                ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.serviceDetail_url, dict: params,viewController: viewController) { (response) in
                 if let data = response{
                    
                    let calenderData :CalenderModel = CalenderModel.init(calenderData: JSON(data))
                    if calenderData.error == "success"{
                        completion(calenderData)
                    }else{
                        viewController.createAlert(title: "Famous Chair", message: calenderData.message ?? "")
                    }
                 }
        }
    }
}
