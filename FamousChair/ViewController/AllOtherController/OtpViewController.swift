//
//  OtpViewController.swift
//  FamousChair
//
//  Created by ginger webs on 10/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class OtpViewController: UIViewController {

    @IBOutlet weak var sixthTextfield: UITextField!
    @IBOutlet weak var fifthTextfield: UITextField!
    @IBOutlet weak var fourthTextfield: UITextField!
    @IBOutlet weak var thirdTextfield: UITextField!
    @IBOutlet weak var secondTextfield: UITextField!
    @IBOutlet weak var firstTextfield: UITextField!
    
    var email = String()
    var otpVM : OtpVM?
    var forgotPasswordVM: ForgotPasswordVM?
    override func viewDidLoad() {
        super.viewDidLoad()
        forgotPasswordVM = ForgotPasswordVM()
        
        firstTextfield.delegate = self
        secondTextfield.delegate = self
        thirdTextfield.delegate = self
        fourthTextfield.delegate = self
         fifthTextfield.delegate = self
         sixthTextfield.delegate = self
        
        self.firstTextfield.keyboardType = UIKeyboardType.decimalPad
        self.secondTextfield.keyboardType = UIKeyboardType.decimalPad
        self.thirdTextfield.keyboardType = UIKeyboardType.decimalPad
        self.fourthTextfield.keyboardType = UIKeyboardType.decimalPad
        self.fifthTextfield.keyboardType = UIKeyboardType.decimalPad
         self.sixthTextfield.keyboardType = UIKeyboardType.decimalPad
        
        firstTextfield.addTarget(self, action: #selector(textFieldchange), for:  UIControl.Event.editingChanged)
        secondTextfield.addTarget(self, action: #selector(textFieldchange), for:  UIControl.Event.editingChanged)
        thirdTextfield.addTarget(self, action: #selector(textFieldchange), for:  UIControl.Event.editingChanged)
        fourthTextfield.addTarget(self, action: #selector(textFieldchange), for:  UIControl.Event.editingChanged)
        fifthTextfield.addTarget(self, action: #selector(textFieldchange), for:  UIControl.Event.editingChanged)
        sixthTextfield.addTarget(self, action: #selector(textFieldchange), for:  UIControl.Event.editingChanged)
        
             
        firstTextfield.addTarget(self, action: #selector(editTextField), for:  UIControl.Event.editingChanged)
        secondTextfield.addTarget(self, action: #selector(editTextField), for:  UIControl.Event.editingChanged)
        thirdTextfield.addTarget(self, action: #selector(editTextField), for:  UIControl.Event.editingChanged)
        fourthTextfield.addTarget(self, action: #selector(editTextField), for:  UIControl.Event.editingChanged)
        fifthTextfield.addTarget(self, action: #selector(editTextField), for:  UIControl.Event.editingChanged)
        sixthTextfield.addTarget(self, action: #selector(editTextField), for:  UIControl.Event.editingChanged)
         otpVM = OtpVM()
        otpVM?.controller = self
        otpVM?.emailValue = email
    }
    
     override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(false, animated: true)

     }
    
    
    @objc func editTextField(textField: UITextField) {
        let value = textField.text
        if (value?.utf16.count) == 0 {
            switch textField {
            case firstTextfield:
                secondTextfield.resignFirstResponder()
                firstTextfield.resignFirstResponder()
            case secondTextfield:
                thirdTextfield.resignFirstResponder()
                firstTextfield.becomeFirstResponder()
            case thirdTextfield:
                fourthTextfield.resignFirstResponder()
                secondTextfield.becomeFirstResponder()
            case fourthTextfield:
                fifthTextfield.resignFirstResponder()
                thirdTextfield.becomeFirstResponder()
                case fifthTextfield:
                     sixthTextfield.resignFirstResponder()
                     fourthTextfield.becomeFirstResponder()
                case sixthTextfield:
                     sixthTextfield.resignFirstResponder()
                     fifthTextfield.becomeFirstResponder()
                
            default:
                break
            }
        }
    }
    
    
    @objc func textFieldchange(textfield: UITextField, string: String) {
        let text = textfield.text
        if (text?.utf16.count)! >= 1 {
            if (text?.utf16.count)! >= 1{
                switch textfield {
                case firstTextfield:
                    secondTextfield.becomeFirstResponder()
                case secondTextfield:
                    thirdTextfield.becomeFirstResponder()
                case thirdTextfield:
                    fourthTextfield.becomeFirstResponder()
                case fourthTextfield:
                    fifthTextfield.becomeFirstResponder()
                case fifthTextfield:
                    sixthTextfield.becomeFirstResponder()
                case sixthTextfield:
                        sixthTextfield.resignFirstResponder()
                    
                    if text?.utf16.count == 0 {
                        fifthTextfield.becomeFirstResponder()
                    }
                default:
                    break
                }
            } else {
                
            }
            
        }
    }

    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        otpVM?.validationOnScreen(first: firstTextfield.text ?? "", second: secondTextfield.text ?? "", third: thirdTextfield.text ?? "", fourth: fourthTextfield.text ?? "", fifth: fifthTextfield.text ?? "", sixth: sixthTextfield.text ?? "", controller: self)
    }
    
    @IBAction func resendOtpAction(_ sender: Any) {
        
        forgotPasswordVM?.validationOnScreen(email: email)
    }
    
}

extension OtpViewController: UITextFieldDelegate {
private func textFieldDidBeginEditing(textField: UITextField) {
    textField.text = ""
}
}
