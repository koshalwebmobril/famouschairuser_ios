//
//  ContactModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
struct ContactModal : Codable {
    let error : String?
    let message : String?
    

    enum CodingKeys: String, CodingKey {

        case error = "status"
        case message = "message"
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        error = try values.decodeIfPresent(String.self, forKey: .error)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        
    }

}
