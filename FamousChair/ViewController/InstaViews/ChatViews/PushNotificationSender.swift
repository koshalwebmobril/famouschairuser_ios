//
//  PushNotificationSender.swift
//  zipHawk
//
//  Created by WebMobril on 02/03/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import Foundation

class PushNotificationSender {
    func sendPushNotification(to token: String, title: String, body: String, dict: [String: Any]) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["to" : token,
                                           "notification" : ["title" : title, "body" : body],
                                           "data" : dict//["user" : "test_id"]
        ]
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
      //  request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: .fragmentsAllowed)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAAyqfHh0U:APA91bHVPI8IG0JxXkdmJhHLgpH9PKalKIzhD2JE6KKUv1yS4VJkQpBLQmo1PfDk2LectBZJVoI5szcbR4wlmF2Ji27pS-U7cv28sZcy6PN7whq2ECAfELnJkxSOMrgmjNDPFTzwXRWJ", forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
    
    func sendGroupPushNotification(to tokens: [String], title: String, body: String, dict: [String: Any]) {
        let urlString = "https://fcm.googleapis.com/fcm/notification"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["registration_ids" : tokens,
                                           "notification_key_name" : title,
                                           "operation": "create",
                                           "notification" : ["title" : title, "body" : body],
                                           "data" : dict//["user" : "test_id"]
        ]
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("870398265157", forHTTPHeaderField: "project_id")
        request.setValue("key=AAAAyqfHh0U:APA91bHVPI8IG0JxXkdmJhHLgpH9PKalKIzhD2JE6KKUv1yS4VJkQpBLQmo1PfDk2LectBZJVoI5szcbR4wlmF2Ji27pS-U7cv28sZcy6PN7whq2ECAfELnJkxSOMrgmjNDPFTzwXRWJ", forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                        self.sendPushNotification(to: "AAAAyqfHh0U:APA91bHVPI8IG0JxXkdmJhHLgpH9PKalKIzhD2JE6KKUv1yS4VJkQpBLQmo1PfDk2LectBZJVoI5szcbR4wlmF2Ji27pS-U7cv28sZcy6PN7whq2ECAfELnJkxSOMrgmjNDPFTzwXRWJ", title: title, body: body, dict: dict)
//                        
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
    
}
