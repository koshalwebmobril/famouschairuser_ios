//
//  LikedUserModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 26/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import SwiftyJSON

class LikedUserModel: NSObject {
    let code :Int?
    var arr_userlist = [UserData]()
    init(data:JSON) {
        self.code = data["code"].intValue
        for item in data["data"].arrayValue{
            let value:UserData = UserData.init(userData: item)
            self.arr_userlist.append(value)
        }
    }

}

class UserData: NSObject {
    let id :Int?
    let name:String?
    let profile_image:String?
    let user_id :String?
    var follow_status :Int?
    init(userData:JSON) {
        self.id = userData["id"].intValue
        self.name = userData["name"].stringValue
        self.profile_image = userData["profile_image"].stringValue
        self.user_id = userData["user_id"].stringValue
        self.follow_status = userData["following_status"].intValue
        
    }
    
}
