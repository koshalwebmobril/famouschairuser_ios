//
//  LiveRoomViewController.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 24/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import AgoraRtcKit
import SDWebImage
import Firebase
import FirebaseDatabase
import IQKeyboardManagerSwift

protocol LiveVCDataSource: NSObjectProtocol {
    func liveVCNeedAgoraKit() -> AgoraRtcEngineKit
    func liveVCNeedSettings() -> Settings
}

class LiveRoomViewController: UIViewController, FirebaseContactManagerDelegate {
    
    @IBOutlet weak var sendMessageViewBottomCons: NSLayoutConstraint!
    @IBOutlet weak var btn_Exit: UIButton!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendMessageView: UIView!
    @IBOutlet weak var textuserImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userTextView: TextViewExtension!
    @IBOutlet weak var profileImage: UILabel!
    @IBOutlet weak var userProfileImg: UIImageView!
    @IBOutlet weak var broadcastersView: AGEVideoContainer!
    @IBOutlet weak var placeholderView: UIImageView!
    @IBOutlet weak var videoMuteButton: UIButton!
    @IBOutlet weak var audioMuteButton: UIButton!
    @IBOutlet weak var beautyEffectButton: UIButton!
    
    @IBOutlet var sessionButtons: [UIButton]!
    
    var messages:[Message] = []
    var driverId = Int()
    var userId = Int()
    var msgType = ""
    var lastMsg = ""
    var message = ""
    var profileImg = ""
    var postId = ""
    var postUsrId = ""
    var channelRef: DatabaseReference = Database.database().reference()
    
    private let beautyOptions: AgoraBeautyOptions = {
        let options = AgoraBeautyOptions()
        options.lighteningContrastLevel = .normal
        options.lighteningLevel = 0.7
        options.smoothnessLevel = 0.5
        options.rednessLevel = 0.1
        return options
    }()
    
    private var agoraKit: AgoraRtcEngineKit {
        return dataSource!.liveVCNeedAgoraKit()
    }
    
    private var settings: Settings {
        return dataSource!.liveVCNeedSettings()
    }
    
    private var isMutedVideo = false {
        didSet {
            // mute local video
            agoraKit.muteLocalVideoStream(isMutedVideo)
            videoMuteButton.isSelected = isMutedVideo
        }
    }
    
    private var isMutedAudio = false {
        didSet {
            // mute local audio
            agoraKit.muteLocalAudioStream(isMutedAudio)
            audioMuteButton.isSelected = isMutedAudio
        }
    }
    
    private var isBeautyOn = false {
        didSet {
            // improve local render view
            agoraKit.setBeautyEffectOptions(isBeautyOn,
                                            options: isBeautyOn ? beautyOptions : nil)
            beautyEffectButton.isSelected = isBeautyOn
        }
    }
    
    private var isSwitchCamera = false {
        didSet {
            agoraKit.switchCamera()
        }
    }
    
    private var videoSessions = [VideoSession]() {
        didSet {
            
            placeholderView.isHidden = (videoSessions.count == 0 ? false : true)
            // update render view layout
            updateBroadcastersView()
        }
    }
    
    private let maxVideoSession = 4
    
    weak var dataSource: LiveVCDataSource?
    var usersName = String()
    var userImage = String()
    var isComeFrom = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(exitLiveChannel), name:Notification.Name( "ExitUser"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(getExitUserApi), name:Notification.Name( "ExitCurrentUser"), object: nil)
        userId = getUserId()
        self.tabBarController?.tabBar.isHidden = true
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        userTextView.delegate = self
        self.textuserImage.contentMode = .scaleAspectFill
        if isComeFrom == "User" {
            profileImage.text = usersName
            userImage = WebServiceApi.baseImage_url + userImage
            self.userProfileImg.sd_setImage(with: URL(string:WebServiceApi.baseImage_url + profileImg),placeholderImage: UIImage(named:
                "userImg"))
            self.textuserImage.sd_setImage(with: URL(string: userImage),placeholderImage: UIImage(named:"userImg"))
            tableViewBottomConstraint.constant = 0
            sendMessageView.isHidden = false
        } else {
            var userImage = getStringValue(key: UserDefaultKeys.userImage)
            profileImage.text = defalut.string(forKey: "name")  ?? ""
            tableViewBottomConstraint.constant = 0
            userImage = WebServiceApi.baseImage_url + userImage
            self.userProfileImg.sd_setImage(with: URL(string: userImage),placeholderImage: UIImage(named:"userImg"))
//            tableViewBottomConstraint.constant = -10
//            sendMessageView.isHidden = true
        }
//        let userImage = defalut.string(forKey: "profile_img")  ?? ""
//        let userImageItem = WebServiceApi.baseImage_url + userImage
//        self.textuserImage.sd_setImage(with: URL(string: userImageItem),placeholderImage: UIImage(named:
//            "userImg"))
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: Notification.Name("Exit"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: UIWindow.keyboardWillHideNotification, object: nil)
        
        if SessionManager.sharedInstance.str_tocken == ""{
            let alert = UIAlertController(title: "Famous Chair", message: "Working on live streaming....", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }else{
            updateButtonsVisiablity()
            loadAgoraKit()
            FireBaseManager.defaultManager.messageManagerDelegate = self
            FireBaseManager.defaultManager.messages = []
            FireBaseManager.defaultManager.fetch_Live_Messages(node: SessionManager.sharedInstance.str_ChannelName/*KeyCenter.ChannelName ?? ""*/)
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @objc func handleKeyboard(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyBoardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                let isKeyboardShowing = notification.name == UIWindow.keyboardWillShowNotification
                self.sendMessageViewBottomCons.constant = isKeyboardShowing ? keyBoardHeight : 0
                // bottomSendViewConstraint.constant = 10
                tableView.contentInset = isKeyboardShowing ? UIEdgeInsets(top: 0, left: 0, bottom: keyBoardHeight, right: 0) : UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                // self.scrollAtBottom()
                UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                    self.view.layoutIfNeeded()
                }) { (completed) in
                    
                }
            }
        }
    }
    
    
    
    @IBAction func postButtonAction(_ sender: Any) {
        guard let message = self.userTextView.text else { return }
        if !message.trimmingCharacters(in: .whitespaces).isEmpty{
            //Text Message
            self.msgType = "Text"
            self.lastMsg = message
            self.message = message
            self.sendMessage()
            self.userTextView.text = ""
            userTextView.resignFirstResponder()
            
        }
    }
    
    func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    //MARK:
    //MARK: Send data Api
    
    func sendMessage() {
//        let userImage = defalut.string(forKey: "profile_img")  ?? ""
        let userImages = WebServiceApi.baseImage_url + getStringValue(key: UserDefaultKeys.userImage)
        let userName = defalut.string(forKey: "name")  ?? ""
        let nodeStr = SessionManager.sharedInstance.str_ChannelName //KeyCenter.ChannelName  ?? ""
        
        let messageItem: NSMutableDictionary = [
            "from":"\(userId)",
            "message": encode(self.message),
            "name": userName,
            "user_img":userImages
        ]
        
        FireBaseManager.defaultManager.sendLiveNewMsg(atNode: nodeStr, detailDict: messageItem as NSMutableDictionary, recentDict: nil)
        
    }
    
    
    func scrollAtBottom() {
        if messages.count > 0 {
            let lastIndex = IndexPath(row: messages.count-1, section: 0)
            //print(lastIndex)
            tableView.scrollToRow(at: lastIndex, at: .bottom, animated: false)
        }
        //        if messages.count > 0 {
        //       // let lastIndex = IndexPath(row: messages.count-1, section: 0)
        //        let indexPath = IndexPath(item: 0, section: 0)
        //        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        //        }
    }
    
    
    @objc func onDidReceiveData(_ notification:Notification) {
        getExitUserApi()
        FireBaseManager.defaultManager.deleteLiveOldChat(node: SessionManager.sharedInstance.str_ChannelName ?? "")
        KeyCenter.Token?.removeAll()
        KeyCenter.ChannelName?.removeAll()
    }
    
    @IBAction func tapExitButton(_ sender: Any) {
        
        if isComeFrom == "User" {
            leaveChannel()
            self.navigationController?.popViewController(animated: true)
        }else {
            getExitUserApi()
        }
        FireBaseManager.defaultManager.deleteLiveOldChat(node: SessionManager.sharedInstance.str_ChannelName ?? "")
        UserDefaults.standard.set("", forKey: "liveTocken")
        UserDefaults.standard.set("", forKey: "channelName")
        KeyCenter.Token?.removeAll()
        KeyCenter.ChannelName?.removeAll()
    }
    
    
    //ExitApi...............................
    @objc func exitLiveChannel(notification:Notification) {
        let userData = notification.object
        InstagramVM.exitLiveUserApi(param: [String : Any](), viewController: self) { (responseObject) in
            if responseObject{
                self.leaveChannel()
                NotificationCenter.default.post(name: NSNotification.Name("joinLiveStreaming"), object: nil, userInfo: userData as! [AnyHashable : Any])
            }
        }
    }
    
    
    //MARK: - ui action
    @IBAction func doSwitchCameraPressed(_ sender: UIButton) {
        isSwitchCamera.toggle()
    }
    
    @IBAction func doBeautyPressed(_ sender: UIButton) {
        isBeautyOn.toggle()
    }
    
    @IBAction func doMuteVideoPressed(_ sender: UIButton) {
//        self.placeholderView.isHidden = false
        isMutedVideo.toggle()
    }
    
    @IBAction func doMuteAudioPressed(_ sender: UIButton) {
        isMutedAudio.toggle()
    }
    
    @IBAction func doLeavePressed(_ sender: UIButton) {
        if isComeFrom == "User" {
            self.navigationController?.popViewController(animated: true)
        }else {
            getExitUserApi()
        }
        //               FireBaseManager.defaultManager.deleteLiveOldChat(node: SessionManager.sharedInstance.str_ChannelName ?? "")
        KeyCenter.Token?.removeAll()
        KeyCenter.ChannelName?.removeAll()
    }
    
}


private extension LiveRoomViewController {
    func updateBroadcastersView() {
        // video views layout
        if videoSessions.count == maxVideoSession {
            broadcastersView.reload(level: 0, animated: true)
        } else {
            var rank: Int
            var row: Int
            
            if videoSessions.count == 0 {
                broadcastersView.removeLayout(level: 0)
                self.placeholderView.isHidden = false
                return
            } else if videoSessions.count == 1 {
                rank = 1
                row = 1
            } else if videoSessions.count == 2 {
                rank = 1
                row = 2
            } else {
                rank = 2
                row = Int(ceil(Double(videoSessions.count) / Double(rank)))
            }
            
            let itemWidth = CGFloat(1.0) / CGFloat(rank)
            let itemHeight = CGFloat(1.0) / CGFloat(row)
            let itemSize = CGSize(width: itemWidth, height: itemHeight)
            let layout = AGEVideoLayout(level: 0)
                .itemSize(.scale(itemSize))
            
            broadcastersView
                .listCount { [unowned self] (_) -> Int in
                    return self.videoSessions.count
            }.listItem { [unowned self] (index) -> UIView in
                return self.videoSessions[index.item].hostingView
            }
            
            broadcastersView.setLayouts([layout], animated: true)
        }
    }
    
    func updateButtonsVisiablity() {
        guard let sessionButtons = sessionButtons else {
            return
        }
        
        let isHidden = settings.role == .audience
        
        for item in sessionButtons {
            item.isHidden = isHidden
        }
    }
    
    func setIdleTimerActive(_ active: Bool) {
        UIApplication.shared.isIdleTimerDisabled = !active
    }
    
    //ExitApi...............................
    @objc func getExitUserApi() {
        InstagramVM.exitLiveUserApi(param: [String : Any](), viewController: self) { (responseObject) in
            if responseObject{
                self.leaveChannel()
            }
        }
    }
}

private extension LiveRoomViewController {
    func getSession(of uid: UInt) -> VideoSession? {
        for session in videoSessions {
            if session.uid == uid {
                return session
            }
        }
        return nil
    }
    
    func videoSession(of uid: UInt) -> VideoSession {
        if let fetchedSession = getSession(of: uid) {
            return fetchedSession
        } else {
            let newSession = VideoSession(uid: uid)
            videoSessions.append(newSession)
            return newSession
        }
    }
}

//MARK: - Agora Media SDK
private extension LiveRoomViewController {
    func loadAgoraKit() {
        guard let channelId = settings.roomName else {
            return
        }
        
        setIdleTimerActive(false)
        
        // sTMbFhyYcYW5QCrN2tXCRFfvZ0DJM5
        
        // Step 1, set delegate to inform the app on AgoraRtcEngineKit events
        agoraKit.delegate = self
        // Step 2, set live broadcasting mode
        // for details: https://docs.agora.io/cn/Video/API%20Reference/oc/Classes/AgoraRtcEngineKit.html#//api/name/setChannelProfile:
        agoraKit.setChannelProfile(.liveBroadcasting)
        // set client role
        agoraKit.setClientRole(settings.role)
        
        // Step 3, Warning: only enable dual stream mode if there will be more than one broadcaster in the channel
        agoraKit.enableDualStreamMode(true)
        
        // Step 4, enable the video module
        agoraKit.enableVideo()
        // set video configuration
        agoraKit.setVideoEncoderConfiguration(
            AgoraVideoEncoderConfiguration(
                size: settings.dimension,
                frameRate: settings.frameRate,
                bitrate: AgoraVideoBitrateStandard,
                orientationMode: .adaptative
            )
        )
        
        // if current role is broadcaster, add local render view and start preview
        if settings.role == .broadcaster {
            addLocalSession()
            agoraKit.startPreview()
        }
        
        // Step 5, join channel and start group chat
        // If join  channel success, agoraKit triggers it's delegate function
        // 'rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int)'
        agoraKit.joinChannel(byToken: SessionManager.sharedInstance.str_tocken, channelId: SessionManager.sharedInstance.str_ChannelName, info: nil, uid: 0, joinSuccess: nil)
        
        // Step 6, set speaker audio route
        agoraKit.setEnableSpeakerphone(true)
    }
    
    func addLocalSession() {
        let localSession = VideoSession.localSession()
        localSession.updateInfo(fps: settings.frameRate.rawValue)
        videoSessions.append(localSession)
        agoraKit.setupLocalVideo(localSession.canvas)
    }
    
    func leaveChannel() {
        // Step 1, release local AgoraRtcVideoCanvas instance
        agoraKit.setupLocalVideo(nil)
        // Step 2, leave channel and end group chat
        agoraKit.leaveChannel(nil)
        // Step 3, if current role is broadcaster,  stop preview after leave channel
        if settings.role == .broadcaster {
            agoraKit.stopPreview()
        }
        
        setIdleTimerActive(true)
        
        navigationController?.popViewController(animated: true)
    }
}

extension LiveRoomViewController : FirebaseMessageManagerDelegate {
    func fireBaseManagerDidCompleteFetchingMessages() {
        //   messages = []
        messages = FireBaseManager.defaultManager.messages
        if messages.count > 0 {
            self.tableView.backgroundView = nil
        }else{
            
        }
        tableView.reloadData()
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
               self.scrollAtBottom()
               }
    }
    
}

extension LiveRoomViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension LiveRoomViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

// MARK: - AgoraRtcEngineDelegate
extension LiveRoomViewController: AgoraRtcEngineDelegate {
    
    /// Occurs when the first local video frame is displayed/rendered on the local video view.
    ///
    /// Same as [firstLocalVideoFrameBlock]([AgoraRtcEngineKit firstLocalVideoFrameBlock:]).
    /// @param engine  AgoraRtcEngineKit object.
    /// @param size    Size of the first local video frame (width and height).
    /// @param elapsed Time elapsed (ms) from the local user calling the [joinChannelByToken]([AgoraRtcEngineKit joinChannelByToken:channelId:info:uid:joinSuccess:]) method until the SDK calls this callback.
    ///
    /// If the [startPreview]([AgoraRtcEngineKit startPreview]) method is called before the [joinChannelByToken]([AgoraRtcEngineKit joinChannelByToken:channelId:info:uid:joinSuccess:]) method, then `elapsed` is the time elapsed from calling the [startPreview]([AgoraRtcEngineKit startPreview]) method until the SDK triggers this callback.
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstLocalVideoFrameWith size: CGSize, elapsed: Int) {
        if let selfSession = videoSessions.first {
            selfSession.updateInfo(resolution: size)
        }
    }
    
    /// Reports the statistics of the current call. The SDK triggers this callback once every two seconds after the user joins the channel.
    func rtcEngine(_ engine: AgoraRtcEngineKit, reportRtcStats stats: AgoraChannelStats) {
        if let selfSession = videoSessions.first {
            selfSession.updateChannelStats(stats)
        }
    }
    
    
    /// Occurs when the first remote video frame is received and decoded.
    /// - Parameters:
    ///   - engine: AgoraRtcEngineKit object.
    ///   - uid: User ID of the remote user sending the video stream.
    ///   - size: Size of the video frame (width and height).
    ///   - elapsed: Time elapsed (ms) from the local user calling the joinChannelByToken method until the SDK triggers this callback.
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid: UInt, size: CGSize, elapsed: Int) {
        guard videoSessions.count <= maxVideoSession else {
            return
        }
        
        let userSession = videoSession(of: uid)
        userSession.updateInfo(resolution: size)
        agoraKit.setupRemoteVideo(userSession.canvas)
    }
    
    /// Occurs when a remote user (Communication)/host (Live Broadcast) leaves a channel. Same as [userOfflineBlock]([AgoraRtcEngineKit userOfflineBlock:]).
    ///
    /// There are two reasons for users to be offline:
    ///
    /// - Leave a channel: When the user/host leaves a channel, the user/host sends a goodbye message. When the message is received, the SDK assumes that the user/host leaves a channel.
    /// - Drop offline: When no data packet of the user or host is received for a certain period of time (20 seconds for the Communication profile, and more for the Live-broadcast profile), the SDK assumes that the user/host drops offline. Unreliable network connections may lead to false detections, so Agora recommends using a signaling system for more reliable offline detection.
    ///
    ///  @param engine AgoraRtcEngineKit object.
    ///  @param uid    ID of the user or host who leaves a channel or goes offline.
    ///  @param reason Reason why the user goes offline, see AgoraUserOfflineReason.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        var indexToDelete: Int?
        for (index, session) in videoSessions.enumerated() where session.uid == uid {
            indexToDelete = index
            break
        }
        
        if let indexToDelete = indexToDelete {
            let deletedSession = videoSessions.remove(at: indexToDelete)
            deletedSession.hostingView.removeFromSuperview()
            
            // release canvas's view
            deletedSession.canvas.view = nil
        }
    }
    
    /// Reports the statistics of the video stream from each remote user/host.
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteVideoStats stats: AgoraRtcRemoteVideoStats) {
        if let session = getSession(of: stats.uid) {
            session.updateVideoStats(stats)
        }
    }
    
    /// Reports the statistics of the audio stream from each remote user/host.
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteAudioStats stats: AgoraRtcRemoteAudioStats) {
        if let session = getSession(of: stats.uid) {
            session.updateAudioStats(stats)
        }
    }
    
    /// Reports a warning during SDK runtime.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurWarning warningCode: AgoraWarningCode) {
        print("warning code: \(warningCode.description)")
    }
    
    /// Reports an error during SDK runtime.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print("warning code: \(errorCode.description)")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didVideoMuted muted: Bool, byUid uid: UInt) {
        print(muted)
        if muted{
            self.placeholderView.isHidden = false
        }else{
            self.placeholderView.isHidden = true
        }
    }
}

extension LiveRoomViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let recentCell = tableView.dequeueReusableCell(withIdentifier: "LiveChatTableViewCell", for: indexPath) as! LiveChatTableViewCell
        // recentCell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        recentCell.userChat.text = decode(messages[indexPath.row].msgText ?? "")
        setProfileImage(userImage: recentCell.userProfile, urlString: messages[indexPath.row].imgURL ?? "")
        recentCell.userNameLabel.text = messages[indexPath.row].senderName
        // Let's say you have a variable called cellHeight
        UIView.animate(withDuration: 0.5, animations: {
            recentCell.alpha = 1
        })
        
        return recentCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cellCount = tableView.visibleCells.count
        let alphaInterval:CGFloat = 1.0 / CGFloat(cellCount / 2)
        
        for (index,cell) in (tableView.visibleCells as [UITableViewCell]).enumerated() {
            
            if index < cellCount / 2 {
                cell.alpha = CGFloat(index) * alphaInterval
            } else {
                cell.alpha = CGFloat(cellCount - index) * (alphaInterval)
            }
        }
    }
    func setProfileImage(userImage: UIImageView, urlString: String) {
        if urlString != "" {
            //   let userProfileImage = WebServiceApi.imageUrl + urlString
            userImage.sd_setImage(with: URL(string: urlString), placeholderImage:UIImage(named: "userImg"), options: .highPriority, completed: { (image, error, cache, url) in
                userImage.image = image
            })
        } else {
            userImage.image = UIImage(named: "userImg")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let cellCount = tableView.visibleCells.count
        let alphaInterval:CGFloat = 1.0 / CGFloat(cellCount / 2)
        
        for (index,cell) in (tableView.visibleCells as [UITableViewCell]).enumerated() {
            
            if index < cellCount / 2 {
                cell.alpha = CGFloat(index) * alphaInterval
            } else {
                cell.alpha = CGFloat(cellCount - index) * (alphaInterval)
            }
        }
    }
    
}




