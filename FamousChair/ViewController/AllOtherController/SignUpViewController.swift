//
//  SignUpViewController.swift
//  FamousChair
//
//  Created by ginger webs on 10/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices

class SignUpViewController: UIViewController {

    @IBOutlet weak var appleView: UIView!
    @IBOutlet weak var signupView: UIView!
    var signupVM : SignUpVM?
    override func viewDidLoad() {
        super.viewDidLoad()
        signupVM = SignUpVM()
        signupVM?.controller = self
        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         signupView.layer.masksToBounds =  true
               signupView.layer.cornerRadius = 20
               signupView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        setupProviderLoginView()
    }
    
    
    /// - Tag: add_appleid_button
    func setupProviderLoginView() {
        if #available(iOS 13.0, *) {
            let authorizationButton = ASAuthorizationAppleIDButton()
//            authorizationButton.frame = appleView.bounds
            authorizationButton.addTarget(self, action: #selector(handleAuthorizationAppleIDButtonPress), for: .touchUpInside)
            self.appleView.addSubview(authorizationButton)
            authorizationButton.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                authorizationButton.centerXAnchor.constraint(equalTo: self.appleView.centerXAnchor),
                authorizationButton.centerYAnchor.constraint(equalTo: self.appleView.centerYAnchor),
                authorizationButton.widthAnchor.constraint(equalToConstant: appleView.frame.width),
                authorizationButton.heightAnchor.constraint(equalToConstant: 50)
                ])
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
    

    @IBAction func signInButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func gmailButtonAction(_ sender: Any) {
        signupVM?.googleLogin()
    }
 
    @IBAction func facebookButtonAction(_ sender: Any) {
        signupVM?.facebookLogin()
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(Vc, animated: true)
    }
}




extension SignUpViewController : ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding {
    @objc
    func handleAuthorizationAppleIDButtonPress() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            self.createAlert(title: "Famous Chair", message: "Your iOS version doesn't support sign in with Apple.")
        }
        
    }
    
    private func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        
        if #available(iOS 13.0, *) {
            let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
            let authorizationController = ASAuthorizationController(authorizationRequests: requests)
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
                   authorizationController.performRequests()
        } else {
            
             print("login already with apple.....")
        }
        
        
    }
    
    @available(iOS 13.0, *)
        
        func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
            switch authorization.credential {
            case let appleIDCredential as ASAuthorizationAppleIDCredential:
                
                // Create an account in your system.
                let userIdentifier = appleIDCredential.user
                let userFirstName = appleIDCredential.fullName?.givenName
                            let userLastName = appleIDCredential.fullName?.familyName
                            let userEmail = appleIDCredential.email
                
                let param = ["email" : "\(userEmail ?? "")",
                             "name" : "\(userFirstName ?? "") \(userLastName ?? "")",
                             "user_type" : "3",
                             "social_token" : "\(userIdentifier)",
                             "device_type" : "2",
                             "profile_image":"",
                             "mobile":"",
                             "device_token" : "123"] as [String : String]
                
                
                LoginVM().socialLogin(param: param, photoLink: "")
            
            case let passwordCredential as ASPasswordCredential:
            
                // Sign in using an existing iCloud Keychain credential.
                let username = passwordCredential.user
                let password = passwordCredential.password
                
               print("username:\(username) and password:\(password)")
               
                
            default:
                break
            }
        }

        
        @available(iOS 13.0, *)
        func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
            return self.view.window!
        }
        
        @available(iOS 13.0, *)
        func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            
    
        print("Apple sign in error")
        }
}


