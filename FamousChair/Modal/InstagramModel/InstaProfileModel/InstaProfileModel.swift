//
//  ProfileModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 27/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import SwiftyJSON

class InstaProfileModel: NSObject {
    let error : String?
    let message : String?
    var result : UserProfileData?
    init(response:JSON) {
        self.error = response["status"].stringValue
        self.message = response["message"].stringValue
        self.result = UserProfileData.init(item:response["data"])
    }
}
class UserProfileData :NSObject {
    var follower_count : Int?
    var following_count : Int?
    var total_post_count : Int?
    var following_status : Int?
    var userData : UserProfile?
    var arr_total_post = [InstaHomeData]()
    init(item:JSON) {
        self.follower_count = item["follower_count"].intValue
        self.following_count = item["following_count"].intValue
        self.total_post_count = item["total_post_count"].intValue
        self.following_status = item["following_status"].intValue
        self.userData = UserProfile.init(data: item["user_profile"])
        for item in item["total_post"].arrayValue{
            let value = InstaHomeData.init(data: item)
            arr_total_post.append(value)
        }
    }
}

class UserProfile: NSObject {
    let image :String?
    let name : String?
    let email : String?
    let userId : Int?
    let device_token : String?
    init(data:JSON) {
        self.image = data["profile_image"].stringValue
        self.name = data["name"].stringValue
        self.device_token = data["device_token"].stringValue
        self.email = data["email"].stringValue
        self.userId = data["id"].intValue
    }
}

class TotalPost: NSObject {
    let post_path :String?
    let post_title : String?
    let thumbnail : String?
    init(data:JSON) {
        self.post_path = data["post_path"].stringValue
        self.post_title = data["post_title"].stringValue
        self.thumbnail = data["thumbnail"].stringValue
    }
}
