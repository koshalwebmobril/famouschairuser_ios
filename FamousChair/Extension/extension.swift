//
//  extension.swift
//  WhatCanISingUser
//
//  Created by netset on 15/11/18.
//  Copyright © 2018 netset. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

//StoryBoards
let homeStoryBoard = UIStoryboard(name: "Home", bundle: nil)
let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
extension UIViewController {
    
    
    func saveToken(token:String){
        UserDefaults.standard.set(token, forKey: "token")
    }
    func getLoginToken() -> String {
        return UserDefaults.standard.value(forKey: "token") as? String ?? ""
    }
    
    func saveUserId(id:Int){
        UserDefaults.standard.set(id, forKey: "userId")
    }
    func getUserId() -> Int {
        return ((UserDefaults.standard.value(forKey: "userId")) as? Int) ?? 0
    }
    func saveuserName(name:String){
        UserDefaults.standard.set(name, forKey: "userName")
    }
    func getUserName() -> String {
        return (UserDefaults.standard.value(forKey: "userName")) as? String ?? ""
    }
}


class LoaderClass {
    
    class func startLoader() {
        let activityData = ActivityData(size: nil, message: nil, messageFont: nil, type: NVActivityIndicatorType.ballRotateChase, color: .white, padding: 0, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR, textColor: nil)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    
    class func stopLoader() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}




extension UIViewController{
    func convertToLocalDateFormatOnlyTime(utcDate:String) -> String
    {
        if utcDate.count == 0
        {
            return ""
        }
        else
        {
            // create dateFormatter with UTC time format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            let dt = dateFormatter.date(from: utcDate)
            dateFormatter.timeZone = TimeZone.current
            
            let finalDateStr = dateFormatter.string(from: dt!)
            dateFormatter.dateFormat = "yyyy/MM/dd'T'HH:mm:ss.SSSXXXXX"
            
            //  return dateFormatter.date(from: differenceDate!)
            return Date().offsetLong(date: dateFormatter.date(from: finalDateStr)!)
        }
    }
    
}


func convertDateFormaterSecond(_ date: String) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    let date = dateFormatter.date(from: date)
    dateFormatter.dateFormat = "dd MMM, yyyy"
        return  dateFormatter.string(from: date!)
    
}

func convertDateFormater(_ date: String) -> String
{
    print(date)
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd MMM, yyyy"
    let date = dateFormatter.date(from: date)
    dateFormatter.dateFormat = "dd/MM/yyyy"
    return  dateFormatter.string(from: date!)
    
}



func convertIntoDate(utcDate:String)-> String {
    if utcDate.count == 0
    {
        return ""
    }
    else
    {
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyyy"
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: utcDate)
       // dateFormatter.timeZone = TimeZone.current
    
        dateFormatter.dateFormat = "MMM dd,yyyy-MMM dd,yyyy"
        let finalDateStr = dateFormatter.string(from: dt!)
        
        
        
        //  return dateFormatter.date(from: differenceDate!)
       // return Date().offsetLong(date: dateFormatter.date(from: finalDateStr)!)
        return finalDateStr
    }
    }


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    func offsetLong(date: Date) -> String {
        if years(from: date)   > 0
        {
            if months(from: date)  < 12
            {
                return months(from: date) > 1 ? "\(months(from: date)) months ago" : "\(months(from: date)) month ago"
            }
            else
            {
                return years(from: date) > 1 ? "\(years(from: date)) years ago" : "\(years(from: date)) year ago"
            }
        }
        if months(from: date)  > 0
        {
            if weeks(from: date)   < 4
            {
                return weeks(from: date) > 1 ? "\(weeks(from: date)) weeks ago" : "\(weeks(from: date)) week ago"
            }
            else
            {
                return months(from: date) > 1 ? "\(months(from: date)) months ago" : "\(months(from: date)) month ago"
            }
        }
        if weeks(from: date)   > 0
        {
            if days(from: date)    < 7
            {
                return days(from: date) > 1 ? "\(days(from: date)) days ago" : "\(days(from: date)) day ago"
            }
            else
            {
                return weeks(from: date) > 1 ? "\(weeks(from: date)) weeks ago" : "\(weeks(from: date)) week ago"
            }
        }
        if days(from: date)    > 0
        {
            if hours(from: date)   < 24
            {
                return hours(from: date) > 1 ? "\(hours(from: date)) hours ago" : "\(hours(from: date)) hour ago"
            }
            else
            {
                return days(from: date) > 1 ? "\(days(from: date)) days ago" : "\(days(from: date)) day ago"
            }
        }
        if hours(from: date)   > 0
        {
            if minutes(from: date) < 59
            {
                return minutes(from: date) > 1 ? "\(minutes(from: date)) min ago" : "\(minutes(from: date)) min ago"
            }
            else
            {
                return hours(from: date) > 1 ? "\(hours(from: date)) hours ago" : "\(hours(from: date)) hour ago"
            }
        }
        if minutes(from: date) > 0
        {
            if seconds(from: date) < 59
            {
                return seconds(from: date) > 1 ? "\(seconds(from: date)) sec ago" : "\(seconds(from: date)) sec ago"
            }
            else
            {
                return minutes(from: date) > 1 ? "\(minutes(from: date)) min ago" : "\(minutes(from: date)) min ago"
            }
        }
        if seconds(from: date) > 0
        {
            return seconds(from: date) > 1 ? "\(seconds(from: date)) sec ago" : "\(seconds(from: date)) sec ago"
        }
        
        return "just now"
    }
    
    
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   == 1
        {
            return "\(years(from: date)) year"
        }
        else if years(from: date)   > 1
        {
            return "\(years(from: date)) years"
        }
        if months(from: date)  == 1
        {
            return "\(months(from: date)) month"
        }
        else if months(from: date)  > 1
        {
            return "\(months(from: date)) month"
        }
        if weeks(from: date)   == 1
        {
            return "\(weeks(from: date)) week"
        }
        else if weeks(from: date)   > 1
        {
            return "\(weeks(from: date)) weeks"
        }
        if days(from: date)    == 1
        {
            return "\(days(from: date)) day"
        }
        else if days(from: date)    > 1
        {
            return "\(days(from: date)) days"
        }
        if hours(from: date)   == 1
        {
            return "\(hours(from: date)) hour"
        }
        else if hours(from: date)   > 1
        {
            return "\(hours(from: date)) hours"
        }
        if minutes(from: date) == 1
        {
            return "\(minutes(from: date)) minute"
        }
        else if minutes(from: date) > 1
        {
            return "\(minutes(from: date)) minutes"
        }
        return ""
    }
}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}

extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }


    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor.init(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = shadowRadius
        }
    }
    
}
//extension NSMutableAttributedString {
//    @discardableResult func bold(_ text: String,size:Int,colorStr:UIColor) -> NSMutableAttributedString {
//        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "Avenir-Book", size: CGFloat(size))!,.foregroundColor : colorStr]
//        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
//        append(boldString)
//
//        return self
//    }
//
//    @discardableResult func boldHeavy(_ text: String,size:Int,colorStr:UIColor) -> NSMutableAttributedString {
//        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "Avenir-Heavy", size: CGFloat(size))!,.foregroundColor : colorStr]
//        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
//        append(boldString)
//
//        return self
//    }
//
//    @discardableResult func normalFont(_ text: String,size:Int,colorStr:UIColor) -> NSMutableAttributedString {
//        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "Avenir-Medium", size: CGFloat(size))!,.foregroundColor : colorStr]
//        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
//        append(boldString)
//
//        return self
//    }
//
//    @discardableResult func boldWithUnderLine(_ text: String,size:Int,colorStr:UIColor) -> NSMutableAttributedString {
//        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Avenir-Book", size: CGFloat(size))!,.underlineStyle :  NSUnderlineStyle.styleSingle.rawValue,.foregroundColor : colorStr]
//
//        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
//        append(boldString)
//
//        return self
//    }
//
//    @discardableResult func boldWithUnderLineHeavy(_ text: String,size:Int,colorStr:UIColor) -> NSMutableAttributedString {
//        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "Avenir-Heavy", size: CGFloat(size))!,.underlineStyle :  NSUnderlineStyle.styleSingle.rawValue,.foregroundColor : colorStr]
//
//        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
//        append(boldString)
//
//        return self
//    }
//
//    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
//        let normal = NSAttributedString(string: text)
//        append(normal)
//
//        return self
//    }
//}

extension UIView {
    func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
}


class UIPlaceholderTextView: UITextView {
    
    var placeholderLabel: UILabel?
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    func sharedInit() {
        refreshPlaceholder()
        NotificationCenter.default.addObserver(self, selector: #selector(textChanged), name: UITextView.textDidChangeNotification, object: nil)
    }
    
    @IBInspectable var placeholder: String? {
        didSet {
            refreshPlaceholder()
        }
    }
    
    @IBInspectable var placeholderColor: UIColor? = .darkGray {
        didSet {
            refreshPlaceholder()
        }
    }
    
    @IBInspectable var placeholderFontSize: CGFloat = 14 {
        didSet {
            refreshPlaceholder()
        }
    }
    
    func refreshPlaceholder() {
        if placeholderLabel == nil {
            placeholderLabel = UILabel()
            let contentView = self.subviews.first ?? self
            
            contentView.addSubview(placeholderLabel!)
            placeholderLabel?.translatesAutoresizingMaskIntoConstraints = false
            
            placeholderLabel?.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: textContainerInset.left + 4).isActive = true
            placeholderLabel?.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: textContainerInset.right + 4).isActive = true
            placeholderLabel?.topAnchor.constraint(equalTo: contentView.topAnchor, constant: textContainerInset.top).isActive = true
            placeholderLabel?.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: textContainerInset.bottom)
        }
        placeholderLabel?.text = placeholder
        placeholderLabel?.textColor = placeholderColor
        placeholderLabel?.font = UIFont.systemFont(ofSize: placeholderFontSize)
    }
    
    @objc func textChanged() {
        if self.placeholder?.isEmpty ?? true {
            return
        }
        
        UIView.animate(withDuration: 0.25) {
            if self.text.isEmpty {
                self.placeholderLabel?.alpha = 1.0
            } else {
                self.placeholderLabel?.alpha = 0.0
            }
        }
    }
    
    override var text: String! {
        didSet {
            textChanged()
        }
    }
    
}
extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

extension UIViewController {
    
    enum deviceType : String{
        case iPad
        case iPhone
        case other
    }
    
    
    func checkDevice () -> deviceType {
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            return .iPad
        } else if UIDevice.current.userInterfaceIdiom == .phone{
            return .iPhone
        } else {
            return .other
        }
        
    }
    
    
    enum VenueDaysList : String {
        case startTime_Wed
        case endTime_Thurs
        case endTime_Sun
        case endTime_Mon
        case startTime_Fri
        case startTime_Thurs
        case endTime_Wed
        case startTime_Sat
        case endTime_Fri
        case endTime_Tues
        case startTime_Sun
        case startTime_Mon
        case startTime_Tues
        case endTime_Sat
    }
    
}

extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
}

extension StringProtocol {
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
    var firstCapitalized: String {
        guard let first = first else { return "" }
        return String(first).capitalized + dropFirst()
    }
}

extension UIViewController {
    
    
    func createAlert (title:String, message:String)
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in})
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    func setStatusBarColor(){
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 88/255.0, green: 179/255.0, blue: 228/255.0, alpha: 1.0)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 88/255.0, green: 179/255.0, blue: 228/255.0, alpha: 1.0)
        }
    }
}

extension UIColor{
   
    public class var baseColor: UIColor {
        return UIColor(red: 88/255.0, green: 179/255.0, blue: 228/255.0, alpha: 1.0)
        }
    
}
