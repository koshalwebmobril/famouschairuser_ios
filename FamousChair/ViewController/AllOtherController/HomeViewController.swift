//
//  HomeViewController.swift
//  FamousChair
//
//  Created by ginger webs on 06/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import CoreLocation
import FSPagerView
import SwiftyJSON

class HomeViewController: UIViewController {
    
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var searchImgView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var aboutLabel: UILabel!
    var locManager = CLLocationManager()
    
    var _selectedCells : NSMutableArray = []
    var homeData : HomeModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        NotificationCenter.default.addObserver(self, selector: #selector(clickOnNotificationWindow), name:Notification.Name( "clickOnNotificationWindow"), object: nil)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(searchAction))
        searchImgView.addGestureRecognizer(tapGesture)
        
        
        self.locationManagerMethod()
        
        pagerView.automaticSlidingInterval = 3.0
        pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.isInfinite = true
        pagerView.itemSize = FSPagerView.automaticSize
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        let parameter = ["device_token":getStringValue(key: UserDefaultKeys.fcmToken)]
        HomeVM.callHomeApi(params: parameter, viewController: self) { (responseObject) in
            self.homeData = responseObject
            self.scrollView.isHidden = false
            self.setAboutUSData()
            self.collectionview.reloadData()
            self.pagerView.reloadData()
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
//    @objc func clickOnNotificationWindow(){
//        let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
//        tabBarController.selectedIndex = 1
//        let appdelegate = UIApplication.shared.delegate as! AppDelegate
//        appdelegate.window!.rootViewController = tabBarController
//    }
    
    func setAboutUSData(){
        
        aboutLabel.attributedText  = (homeData?.abouut_us ?? "").htmlToAttributedString
    }
    
    @objc func searchAction(){
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVC.address = addressLabel.text ?? ""
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    @IBAction func viewAllButtonAction(_ sender: Any) {
        let categoryVC = self.storyboard?.instantiateViewController(withIdentifier: "AllCategoriesViewController") as! AllCategoriesViewController
        categoryVC.address = addressLabel.text ?? ""
        self.navigationController?.pushViewController(categoryVC, animated: true)
    }
}


extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout,UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVC.address = addressLabel.text ?? ""
        self.navigationController?.pushViewController(searchVC, animated: true)
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeData?.result.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\( homeData?.result[indexPath.row].image ?? "")", imageView: cell.category_imageView,placeHolder: "Image")
        cell.category_name.text = homeData?.result[indexPath.row].name ?? ""
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let providerVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        providerVC.searchText = homeData?.result[indexPath.row].name ?? ""
        providerVC.isFromSearch = false
        providerVC.address = addressLabel.text ?? ""
        self.navigationController?.pushViewController(providerVC, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((collectionView.frame.size.width / 2) - 80), height: 150)
    }
    
    
}

extension HomeViewController : CLLocationManagerDelegate {
    fileprivate func locationManagerMethod() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        locManager.distanceFilter = 100
        locManager.startUpdatingLocation()
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == .authorizedAlways){
            
            
            print("location authorized......")
            
        }else{
            
            let alert = UIAlertController(title: "Allow Location Access", message: "Famous Chair needs access to your location to show shops nearby you. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)
            
            
            alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)")
                    })
                }
            }))
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        geocode(latitude: locations.last?.coordinate.latitude ?? 0.0, longitude: locations.last?.coordinate.longitude ?? 0.0) { placemark, error in
            
            guard let placemark = placemark, error == nil else { return }
            // you should always update your UI in the main thread
            DispatchQueue.main.async {
                let newLocationAddress = String(format:"%@ %@ %@ %@ %@ %@",(placemark.subLocality ?? ""), (placemark.subAdministrativeArea ?? ""), (placemark.locality ?? ""),(placemark.administrativeArea ?? ""), (placemark.country ?? ""), (placemark.postalCode ?? ""))
                
                print("Complete Address Format - \(newLocationAddress)")
                
                self.addressLabel.text = newLocationAddress
            }
        }
        
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
}


extension HomeViewController :FSPagerViewDelegate,FSPagerViewDataSource{
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return homeData?.bannerArray.count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(homeData?.bannerArray[index].image ?? "")", imageView: cell.imageView!, placeHolder: "")
        
        cell.imageView?.contentMode = .scaleToFill
        cell.imageView?.clipsToBounds = true
        
        
        return cell
        
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
}
