//
//  CommentModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 26/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import SwiftyJSON
class CommentModel: NSObject {
    let code :Int?
    let commentCount:Int?
    var arr_commentList = [CommentData]()
    init(data:JSON) {
        self.code = data["code"].intValue
        self.commentCount = data["data"]["comment_count"].intValue
        for item in data["data"]["comments"].arrayValue{
            let value:CommentData = CommentData.init(userData: item)
            self.arr_commentList.append(value)
        }
    }
}

class CommentData: NSObject {
    let id :Int?
    let comment_userId : String?
    let name:String?
    let profile_image:String?
    let user_id :String?
    var comment : String?
    let commentTime: String?
    init(userData:JSON) {
        self.id = userData["comment_id"].intValue
        self.comment_userId = userData["id"].stringValue
        self.name = userData["name"].stringValue
        self.profile_image = userData["profile_image"].stringValue
        self.user_id = userData["user_id"].stringValue
         self.comment = userData["comment"].stringValue
         self.commentTime = userData["postedtime"].stringValue
    }
    
}

