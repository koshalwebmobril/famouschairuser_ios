//
//  SearchModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class SearchModal : NSObject {
    let error : String?
    let message :String?
    var result = [SearchData]()
   
    init(data:JSON) {
        self.error = data["status"].stringValue
        self.message = data["message"].stringValue
        for item in data["data"].arrayValue{
            let searchData :SearchData = SearchData.init(data: item)
            result.append(searchData)
            
        }
    }

}
