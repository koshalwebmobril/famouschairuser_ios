
import Foundation
struct NotificationData : Codable {
	let code : Int?
	let status : String?
	let message : String?
	let data : ListData?

	enum CodingKeys: String, CodingKey {

		case code = "code"
		case status = "status"
		case message = "message"
		case data = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		data = try values.decodeIfPresent(ListData.self, forKey: .data)
	}

}
