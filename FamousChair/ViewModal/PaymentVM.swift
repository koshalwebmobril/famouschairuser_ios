//
//  PaymentVM.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 25/11/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class PaymentVM: NSObject {
    /**
     @developer : Sandeep Kumar
     @description - Paypal Token  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    
    class func callPaypalTokenApi(params: [String:Any],viewController:UIViewController, completion: @escaping (PayapalTokenModel) -> Void)  {
        ApiManager.shared.getHeaderRequestApi(url: WebServiceApi.generate_paypal_url, dict: params, viewController: viewController) { (response) in
            if let responseData = response {
                let paypalData = try! JSONDecoder().decode(PayapalTokenModel.self, from: responseData)
                completion(paypalData)
            } else {
                
            }
            
        }
    }
}
