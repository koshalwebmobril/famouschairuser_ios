//
//  CommentedListVC.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 14/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class CommentedListVC: UIViewController {

    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var lbl_commentCount: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var postId = String()
    var selectedIndex =  -1
    var arr_comment :CommentModel?
    var isUpdate = false
    var commentId = String()
    var commentUpdateIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(deleteComment))
        longPressGesture.minimumPressDuration = 0.5
        self.tableView.tableHeaderView?.addGestureRecognizer(longPressGesture)
        
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(getStringValue(key: UserDefaultKeys.userImage))", imageView:self.profileImageView, placeHolder: "userImg")
        
        InstagramVM.callGetCommentApi(params: ["post_id":postId], viewController: self, completion: {responseObject in
            self.arr_comment = responseObject
            let strCount: String = (self.arr_comment?.arr_commentList.count ?? 0 == 0||self.arr_comment?.arr_commentList.count ?? 0 == 1) ? "\(self.arr_comment?.arr_commentList.count ?? 0) Comment" : "\(self.arr_comment?.arr_commentList.count ?? 0) Comments"
            self.lbl_commentCount.text = "\(strCount)"
            if self.arr_comment?.arr_commentList.count ?? 0 > 0{
                self.tableView.isHidden = false
                self.blankView.isHidden = true
            }else{
            self.tableView.isHidden = true
                self.blankView.isHidden = false
            }
           
            self.tableView.reloadData()
        })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func replyButtonAction(_ sender: UIButton) {
        
        txtComment.text = "@\(self.arr_comment?.arr_commentList[sender.tag].name ?? "") "
        txtComment.becomeFirstResponder()

    }
    
    @IBAction func sendCommentAction(_ sender: Any) {
        txtComment.resignFirstResponder()
        
        if txtComment.text!.isEmpty{
            self.createAlert(title: "Famous Chair", message: "Write your comment.")
        }else{
            let message = encode(txtComment.text ?? "")
            if isUpdate{
                InstagramVM.callEditCommentApi(params: ["comment_id":commentId,"comment":message], viewController: self) { (responseObject) in
                    if responseObject{
                        self.txtComment.text = ""
                        self.arr_comment?.arr_commentList[self.commentUpdateIndex].comment = message
                        self.tableView.reloadData()
                    }
                }
            }else{
                InstagramVM.callMakeCommentApi(params: ["post_id":"\(postId)","comment":message], viewController: self, completion: {status in
                    if status{
                        self.txtComment.text = ""
                        InstagramVM.callGetCommentApi(params: ["post_id":self.postId], viewController: self, completion: {responseObject in
                            self.arr_comment = responseObject
                            let strCount: String = (self.arr_comment?.arr_commentList.count ?? 0 == 0||self.arr_comment?.arr_commentList.count ?? 0 == 1) ? "\(self.arr_comment?.arr_commentList.count ?? 0) Comment" : "\(self.arr_comment?.arr_commentList.count ?? 0) Comments"
                            self.lbl_commentCount.text = "\(strCount)"
                            self.tableView.isHidden = false
                            self.blankView.isHidden = true
                            self.tableView.reloadData()
                        })
                    }
                })
            }
        }
    }
    
    @IBAction func creatNewPostAction(_ sender: Any) {
    }
    

    @objc func deleteComment(sender : UILongPressGestureRecognizer){
        if let tag = sender.view?.tag {
            let section = Int(tag)
            let alert = UIAlertController(title: "Famous Chair", message: "Please select operation to perform on comment", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete Comment", style: .default, handler: { (UIAlertAction) in
                InstagramVM.callDeleteCommentApi(params: ["comment_id" : "\(self.arr_comment?.arr_commentList[section].id ?? 0)"], viewController: self) { (response) in
                    print("deleted:\(self.arr_comment?.arr_commentList[section].id ?? 0)")
                    self.arr_comment?.arr_commentList.remove(at:section)
                    self.tableView.reloadData()
                }
            }))
            if getStringValue(key: UserDefaultKeys.userId) == self.arr_comment?.arr_commentList[section].comment_userId ?? ""{
            alert.addAction(UIAlertAction(title: "Update Comment", style: .default, handler: {action in
                self.txtComment.text = self.arr_comment?.arr_commentList[section].comment ?? ""
                self.commentId = "\(self.arr_comment?.arr_commentList[section].id ?? 0)"
                self.isUpdate = true
                self.commentUpdateIndex = Int(tag)
            }))
            }
            
            alert.addAction(UIAlertAction(title: "Not Now", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    
    @objc func deleteRowComment(sender : UILongPressGestureRecognizer){
        if let tag = sender.view?.tag {
            let section = Int(tag)
            let alert = UIAlertController(title: "Famous Chair", message: "Are you sure! Do you want to delete comment?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
                InstagramVM.callDeleteCommentApi(params: ["comment_id" : "\(self.arr_comment?.arr_commentList[section].id ?? 0)"], viewController: self) { (response) in
                    print("deleted")
                    self.arr_comment?.arr_commentList.remove(at:section)
                    self.tableView.reloadData()
                }
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
}



extension CommentedListVC:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count ?? 0 == 200{
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text!.isEmpty{
            self.createAlert(title: "Famous Chair", message: "Write your comment.")
        }else{
        InstagramVM.callMakeCommentApi(params: ["post_id":"\(postId)","comment":"\(txtComment.text ?? "")"], viewController: self, completion: {status in
            if status{
                self.txtComment.text = ""
                InstagramVM.callGetCommentApi(params: ["post_id":self.postId], viewController: self, completion: {responseObject in
                    self.arr_comment = responseObject
                    let strCount: String = (self.arr_comment?.arr_commentList.count ?? 0 == 0||self.arr_comment?.arr_commentList.count ?? 0 == 1) ? "\(self.arr_comment?.arr_commentList.count ?? 0) Comment" : "\(self.arr_comment?.arr_commentList.count ?? 0) Comments"
                    self.lbl_commentCount.text = "\(strCount)"
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                })
            }
        })
        }
        return true
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return arr_comment?.arr_commentList.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as! CommentCell
        cell.contentView.tag = indexPath.row
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(deleteComment))
               longPressGesture.minimumPressDuration = 0.5
        cell.contentView.addGestureRecognizer(longPressGesture)
        cell.replyBtn.tag = indexPath.row
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(arr_comment?.arr_commentList[indexPath.row].profile_image ?? "")", imageView: cell.profileImage, placeHolder: "userImg")
        let attr = [NSAttributedString.Key.foregroundColor : UIColor.black,NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16)] as [NSAttributedString.Key : Any]
        let attr1 = [NSAttributedString.Key.foregroundColor : UIColor.darkGray,NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15)] as [NSAttributedString.Key : Any]
        let nameStr = NSMutableAttributedString(string: arr_comment?.arr_commentList[indexPath.row].name ?? "", attributes: attr)
        let commentStr = NSAttributedString(string:String(format: "  %@", decode(arr_comment?.arr_commentList[indexPath.row].comment ?? "") ?? "") , attributes: attr1)
        nameStr.append(commentStr)
        cell.nameLabel.attributedText = nameStr
        cell.timeLabel.text = stringFromTimeInterval(interval: convertStringToTimeintervalFormater(arr_comment?.arr_commentList[indexPath.row].commentTime ?? ""))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }

    
}


extension CommentedListVC{
    func stringFromTimeInterval(interval: TimeInterval) -> String {

        let time = NSInteger(interval)


        let minutes = (time / 60) % 60
        let hours = (time / 3600)
        
        var timeStr = String()
        
        if hours > 0 && hours < 23{
         
            timeStr = String(format: "%dhw", Int(hours))
         
        }else if hours > 23{
         
            let day = ceil(Double(hours/24))
        
            timeStr = String(format: "%dd", Int(day))
         
        }else if minutes > 0 && minutes < 59{
             timeStr = String(format: "%d min", Int(minutes))
        }else{
             timeStr = "1s"
        }
        return timeStr

    }
    
    func convertStringToTimeintervalFormater(_ date: String) -> TimeInterval
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateS = dateFormatter.date(from: date)
        let dateC = dateS?.toLocalTime()
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd h:mm a"
        let todayDate = formatter.string(from: Date())
        let currentDate = formatter.date(from: todayDate)!
        let convertDate = formatter.date(from: dateFormatter.string(from: dateC ?? Date()))
        
        let finalDate = currentDate.timeIntervalSince(convertDate!)
        return finalDate

    }
}


extension Date {
    
    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
    
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
}
