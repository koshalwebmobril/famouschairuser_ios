

import Foundation
struct otpData : Codable {
	let id : Int?
	let name : String?
	let email : String?
	let email_verified_at : String?
	let mobile : String?
	let address : String?
	let image : String?
	let otp : String?
	let otp_verify : String?
	let user_status : String?
	let device_type : String?
	let device_tocken : String?
	let user_type : String?
	let login_type : String?
	let type_of_service : String?
	let license_number : String?
	let tax_id : String?
	let business_number : String?
	let created_at : String?
	let updated_at : String?
	let token : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case email = "email"
		case email_verified_at = "email_verified_at"
		case mobile = "mobile"
		case address = "address"
		case image = "image"
		case otp = "otp"
		case otp_verify = "otp_verify"
		case user_status = "user_status"
		case device_type = "device_type"
		case device_tocken = "device_tocken"
		case user_type = "user_type"
		case login_type = "login_type"
		case type_of_service = "type_of_service"
		case license_number = "license_number"
		case tax_id = "tax_id"
		case business_number = "business_number"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case token = "token"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at)
		mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		otp = try values.decodeIfPresent(String.self, forKey: .otp)
		otp_verify = try values.decodeIfPresent(String.self, forKey: .otp_verify)
		user_status = try values.decodeIfPresent(String.self, forKey: .user_status)
		device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
		device_tocken = try values.decodeIfPresent(String.self, forKey: .device_tocken)
		user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
		login_type = try values.decodeIfPresent(String.self, forKey: .login_type)
		type_of_service = try values.decodeIfPresent(String.self, forKey: .type_of_service)
		license_number = try values.decodeIfPresent(String.self, forKey: .license_number)
		tax_id = try values.decodeIfPresent(String.self, forKey: .tax_id)
		business_number = try values.decodeIfPresent(String.self, forKey: .business_number)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
		token = try values.decodeIfPresent(String.self, forKey: .token)
	}

}
