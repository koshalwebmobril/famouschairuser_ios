//
//  goLiveApiModal.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 02/11/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import Foundation
struct goLiveApiModal : Codable {
    let token : String?
    let channelName : String?

    enum CodingKeys: String, CodingKey {

        case token = "token"
        case channelName = "channelName"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        channelName = try values.decodeIfPresent(String.self, forKey: .channelName)
    }

}
