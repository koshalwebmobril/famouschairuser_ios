//
//  LiveChatTableViewCell.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 24/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class LiveChatTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userChat: UILabel!
    @IBOutlet weak var userProfile: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
