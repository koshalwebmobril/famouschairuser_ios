//
//  TransactionVM.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 18/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class TransactionVM: NSObject {
 
 class func callTransactionHistoryServicesApi(params: [String:Any],viewController:UIViewController, completion: @escaping (TransactionModal) -> Void)  {
     ApiManager.shared.getHeaderRequestApi(url: WebServiceApi.transactionHistory, dict: params,viewController: viewController) { (response) in
         if let data = response{
             
             let data :TransactionModal = TransactionModal.init(data:JSON(data))
             if data.error == "success"{
                completion(data)
                 
             }else{
                 viewController.createAlert(title: "Famous Chair", message: data.message ?? "")
             }
         }
     }
 }
 
}
