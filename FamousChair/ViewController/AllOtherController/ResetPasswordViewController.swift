//
//  ResetPasswordViewController.swift
//  FamousChair
//
//  Created by ginger webs on 10/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var confirmTextField: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    var email = String()
    var isfromProfile = String()
    
    var resetPasswordVM: ResetPasswordVM?
    override func viewDidLoad() {
        super.viewDidLoad()
        resetPasswordVM = ResetPasswordVM()
        resetPasswordVM?.controller = self
        emailTextField.text = email
    }
     override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(false, animated: true)

     }

    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        resetPasswordVM?.validationOnScreen(email: emailTextField.text ?? "", password: passwordTextfield.text ?? "", confirmPassword: confirmTextField.text ?? "",isFromProfile: isfromProfile)
    }
}
extension ResetPasswordViewController:  UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let _char = string.cString(using: String.Encoding.utf8)
        let isBackSpace: Int = Int(strcmp(_char, "\\b"))
        if isBackSpace == -92 {
            return true
        }
        if textField.textInputMode?.primaryLanguage == "emoji" || !(((textField.textInputMode?.primaryLanguage) != nil)) {
            return false
        }
         return string .rangeOfCharacter(from: .whitespacesAndNewlines) == nil
        return true
    }
}
