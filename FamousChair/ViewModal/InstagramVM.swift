//
//  InstagramVM.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 21/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class InstagramVM: NSObject {
    /**
     @developer : Sandeep Kumar
     @description - Calling Instagram home  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping (InstaHomeModel) -> Void)
     @return -Nil
     */
    class func callHomeDataApi(params: [String:Any],viewController:UIViewController, completion: @escaping (InstaHomeModel) -> Void)  {
        ApiManager.shared.getHeaderRequestApi(url: WebServiceApi.homeData_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let data :InstaHomeModel = InstaHomeModel.init(response:JSON(data))
                completion(data)
            }
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Call get all posted  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping (InstaHomeModel) -> Void)
     @return -Nil
     */
    class func getAllPostedDataApi(params: [String:Any],viewController:UIViewController, completion: @escaping (InstaHomeModel) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.profile_wall_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let data :InstaHomeModel = InstaHomeModel.init(response:JSON(data))
                completion(data)
            }
        }
    }
    
    
    /**
     @developer : Sandeep Kumar
     @description - Calling post share  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callPostSharApi(params: [String:Any], post_path: Data!,imageName: String,thumbnail:String,thumbnail_data:Data! ,viewController:UIViewController, completion: @escaping () -> Void)  {
        ApiManager.shared.uploadVideoAndImageApi(urlmethod: WebServiceApi.post_share_url, parameter: params, post_path: post_path, imageName: imageName, thumbnail: thumbnail, thumbnail_data: thumbnail_data, viewController: viewController) { (response) in
            
            let data = JSON(response ?? Data())
            let alert = UIAlertController(title: "Famous Chair", message: data["message"].stringValue, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                viewController.navigationController?.popToRootViewController(animated: true)
                
            }))
            
            viewController.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling following  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callFollowingApi(params: [String:Any],viewController:UIViewController, completion: @escaping (Bool) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.following_url, dict: params,viewController: viewController) { (response) in
            completion(true)
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling unfollowing  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callUnFollowingApi(params: [String:Any],viewController:UIViewController, completion: @escaping (Bool) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.unfollowing_url, dict: params,viewController: viewController) { (response) in
            completion(true)
        }
    }
    
    
    /**
     @developer : Sandeep Kumar
     @description - Calling like  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callLikeApi(params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.like_url, dict: params,viewController: viewController) { (response) in
            completion()
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling unlike  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callUnLikeApi(params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.unlike_url, dict: params,viewController: viewController) { (response) in
            completion()
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling get my profile  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callGetProfileApi(params: [String:Any],viewController:UIViewController, completion: @escaping (InstaProfileModel) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.get_my_profile_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let data :InstaProfileModel = InstaProfileModel.init(response:JSON(data))
                completion(data)
            }
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling get comments  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callGetCommentApi(params: [String:Any],viewController:UIViewController, completion: @escaping (CommentModel) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.getcomments_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let data :CommentModel = CommentModel.init(data:JSON(data))
                completion(data)
            }
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling make comments Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callMakeCommentApi(params: [String:Any],viewController:UIViewController, completion: @escaping (Bool) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.makecomments_url, dict: params,viewController: viewController) { (response) in
            completion(true)
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling get like user  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callGetLikeUserApi(params: [String:Any],viewController:UIViewController, completion: @escaping (LikedUserModel) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.like_user_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let data :LikedUserModel = LikedUserModel.init(data:JSON(data))
                completion(data)
            }
        }
    }
    
    /**
         @developer : Sandeep Kumar
         @description - Calling edit comment  Api
         @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
         @return -Nil
         */
        class func callEditCommentApi(params: [String:Any],viewController:UIViewController, completion: @escaping (Bool) -> Void)  {
            ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.edit_comment_url, dict: params,viewController: viewController) { (response) in
                if let data = response{
                    let value = JSON(data)
                    if value["status"].stringValue == "error"{
    //                    viewController.createAlert(title: "Famous Chair", message: value["message"].stringValue)
                        completion(false)
                    }else{
    //                    viewController.createAlert(title: "Famous Chair", message: value["message"].stringValue)
                        completion(true)
                    }
                }
            }
        }
        
        
    
    /**
     @developer : Sandeep Kumar
     @description - Calling hide post Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callHidePostApi(params: [String:Any],viewController:UIViewController, completion: @escaping (Bool) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.hidepost_url, dict: params,viewController: viewController) { (response) in
            completion(true)
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling search api  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callSearchPeopleApi(params: [String:Any],viewController:UIViewController, completion: @escaping (SearchUserModel) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.search_people_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let data :SearchUserModel = SearchUserModel.init(response:JSON(data))
                completion(data)
            }
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling following list  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callFollowingListApi(params: [String:Any],viewController:UIViewController, completion: @escaping (FollowerModel) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.following_list_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let data :FollowerModel = FollowerModel.init(response:JSON(data))
                completion(data)
            }
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling follower list  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callFollowerListApi(params: [String:Any],viewController:UIViewController, completion: @escaping (FollowerModel) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.follower_list_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let data :FollowerModel = FollowerModel.init(response:JSON(data))
                completion(data)
            }
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling share post  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callSharePostApi(params: [String:Any],viewController:UIViewController, completion: @escaping (Bool) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.share_post_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let value = JSON(data)
                if value["status"].stringValue == "error"{
                    viewController.createAlert(title: "Famous Chair", message: value["message"].stringValue)
                    completion(false)
                }else{
                    viewController.createAlert(title: "Famous Chair", message: value["message"].stringValue)
                    completion(true)
                }
            }
        }
    }
    
    
    /**
        @developer : Sandeep Kumar
        @description - Calling report post  Api
        @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
        @return -Nil
        */
       class func callReportPostApi(params: [String:Any],viewController:UIViewController, completion: @escaping (Bool) -> Void)  {
           ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.report_post_url, dict: params,viewController: viewController) { (response) in
               if let data = response{
                   let value = JSON(data)
                   if value["status"].stringValue == "error"{
                       viewController.createAlert(title: "Famous Chair", message: value["message"].stringValue)
                       completion(false)
                   }else{
                       viewController.createAlert(title: "Famous Chair", message: value["message"].stringValue)
                       completion(true)
                   }
               }
           }
       }
    
    /**
     @developer : Sandeep Kumar
     @description - Calling delete post  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callDeletePostApi(params: [String:Any],viewController:UIViewController, completion: @escaping (Bool) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.delete_posted_post_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let value = JSON(data)
                if value["status"].stringValue == "error"{
                    viewController.createAlert(title: "Famous Chair", message: value["message"].stringValue)
                    completion(false)
                }else{
                    viewController.createAlert(title: "Famous Chair", message: value["message"].stringValue)
                    completion(true)
                }
            }
        }
    }
    
    
    /**
     @developer : Sandeep Kumar
     @description - Calling delete comment  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    class func callDeleteCommentApi(params: [String:Any],viewController:UIViewController, completion: @escaping (Bool) -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.delete_comment_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
                let value = JSON(data)
                if value["status"].stringValue == "error"{
                    viewController.createAlert(title: "Famous Chair", message: value["message"].stringValue)
                    completion(false)
                }else{
                    viewController.createAlert(title: "Famous Chair", message: value["message"].stringValue)
                    completion(true)
                }
            }
        }
    }
    
    
    /**
     @developer : Sandeep Kumar
     @description - Go Live  Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    
    class func callGoliveApi(params: [String:Any],type:String,viewController:UIViewController, completion: @escaping (Bool) -> Void)  {
        ApiManager.shared.getRequestGoliveApi(url: WebServiceApi.go_live_url, dict: params, viewController: viewController) { (response) in
            if let responseData = response {
                let HomeData = try! JSONDecoder().decode(goLiveApiModal.self, from: responseData)
                print(responseData)
                
                SessionManager.sharedInstance.str_ChannelName = HomeData.channelName ?? "Koahal"
                SessionManager.sharedInstance.str_tocken = HomeData.token ?? "Koahal"
                let paramater = ["token" : HomeData.token ?? "", "channelname" : HomeData.channelName ?? "","live_status":type]
                getLiveDataApi(param: paramater, viewController: viewController)
                
                completion(true)
                
            } else {
                
            }
            //             completion(true)
        }
    }
    
    /**
     @developer : Sandeep Kumar
     @description - Getlive Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    
    class func getLiveDataApi(param : [String : Any],viewController:UIViewController) {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.getliveid_url, dict: param, viewController: viewController, completion: { (response) in
            if let responseData = response {
                
            } else {
                
            }
            
        })
    }
    
    
    /**
     @developer : Sandeep Kumar
     @description - Getlive Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    
    class func getLiveUserApi(param : [String : Any],viewController:UIViewController,completion: @escaping (UserLiveListModal) -> Void) {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.getlive_user_url, dict: param, viewController: viewController, completion: { (response) in
            if let responseData = response {
                let liveData = try! JSONDecoder().decode(UserLiveListModal.self, from: responseData)
                completion(liveData)
            } else {
                
            }
            
        })
    }
    
    
    /**
     @developer : Sandeep Kumar
     @description - Getlive Api
     @paramater - (params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)
     @return -Nil
     */
    
    class func exitLiveUserApi(param : [String : Any],viewController:UIViewController,completion: @escaping (Bool) -> Void) {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.end_live_url, dict: param, viewController: viewController, completion: { (response) in
            if let responseData = response {
                completion(true)
            } else {
                
            }
            
        })
    }
    
    
    
}
