//
//  HomeData.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class HomeData :NSObject {
    var id : Int?
    var providerId : Int?
    var name:String?
    var image : String?
    var price : Double?
    
    init(data:JSON) {
        self.id = data["id"].intValue
        self.name = data["service_name"].stringValue
         self.providerId = data["user_id"].intValue
        self.image = data["service_image"].stringValue
        self.price = data["price"].doubleValue
    }
    override init() {
        
    }
   
}
