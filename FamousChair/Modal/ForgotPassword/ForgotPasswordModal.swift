
import Foundation
struct ForgotPasswordModal : Codable {
	let status : String?
	let message : String?
	let result : forgotPasswordData?
	let otp : Int?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case result = "result"
		case otp = "otp"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		result = try values.decodeIfPresent(forgotPasswordData.self, forKey: .result)
		otp = try values.decodeIfPresent(Int.self, forKey: .otp)
	}

}
