//
//  b.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 26/11/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

import Foundation
struct UserLiveListModal : Codable {
    let code : Int?
    let status : Bool?
    let message : String?
    let data : [UserListLiveData]?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([UserListLiveData].self, forKey: .data)
    }

}
