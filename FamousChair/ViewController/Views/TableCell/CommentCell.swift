//
//  CommentCell.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 14/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var replyBtn: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    @IBOutlet weak var replyProfileImage: UIImageView!
    @IBOutlet weak var replyName: UILabel!
    @IBOutlet weak var replyTime: UILabel!
    @IBOutlet weak var reply2Btn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
