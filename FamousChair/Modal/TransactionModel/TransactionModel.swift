//
//  TransactionModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 18/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class TransactionModal : NSObject {
    let error : String?
    let message :String?
    var arr_history = [Transaction]()
    
    init(data:JSON) {
        self.error = data["status"].stringValue
        self.message = data["message"].stringValue
        for item in  data["data"].arrayValue{
        let transaction = Transaction.init(item: item)
            self.arr_history.append(transaction)
        }
        
    }

}


class Transaction: NSObject {
    let transactionId : String?
    let serviceName: String?
    let price : String?
    let date : String?
    let providerName : String?
    init(item:JSON) {
        self.transactionId = item["transaction_id"].stringValue
        self.date = item["created_at"].stringValue
        self.serviceName = item["service_name"].stringValue
        self.price = item["amount"].stringValue
        self.providerName = item["provider_name"].stringValue
        
    }
    
}
