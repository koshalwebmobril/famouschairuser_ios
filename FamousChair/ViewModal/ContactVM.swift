//
//  ContactVM.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import UIKit
class ContactVM: NSObject {
     func validationOnScreen(subject: String,message: String,viewController:UIViewController) {
        
        if subject == "" {
            viewController.createAlert(title: "Famous Chair", message: "Please enter subject.")
            
        } else if message == "" {
            viewController.createAlert(title: "Famous Chair", message: "Please enter message.")
            
        } else {
            let param = ["subject" : subject,"message":message]
            contactApi(param: param,viewController:viewController )
            
        }
    }

}


extension ContactVM {
    
    func contactApi(param : [String : String],viewController:UIViewController) {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.contactUS, dict: param,viewController: viewController) { (response) in
            if let responseData = response {
                let contactData = try! JSONDecoder().decode(ContactModal.self, from: responseData)
                if contactData.error == "success" {
                    let alert = UIAlertController(title: "Famous Chair", message: contactData.message ?? "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                        viewController.navigationController?.popViewController(animated: true)
                    }))
                    viewController.present(alert, animated: true, completion: nil)
                    
                } else {
                    
                    viewController.createAlert(title: "Famous Chair", message: contactData.message ?? "")
            }
        }
    }
}
}

