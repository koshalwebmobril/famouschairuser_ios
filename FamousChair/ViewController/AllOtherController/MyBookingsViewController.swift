//
//  MyBookingsViewController.swift
//  FamousChair
//
//  Created by ginger webs on 09/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class MyBookingsViewController: UIViewController {
var headersArray = ["Upcoming", "Previous"]
    
    @IBOutlet weak var tableView: UITableView!
   
    @IBOutlet weak var blankImgView: UIImageView!
    var bookingList : BookingList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
    }

     override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(true, animated: true)
        BookingVM.callBookingListServicesApi(params: [String:Any](), viewController: self) { (responseObject) in
            self.bookingList = responseObject
            if self.bookingList?.arr_upcommingbooking.count ?? 0 > 0 || self.bookingList?.arr_prevbooking.count ?? 0 > 0 {
                self.tableView.isHidden = false
                self.blankImgView.isHidden = true
            }else{
                self.tableView.isHidden = true
                self.blankImgView.isHidden = false
            }
            
            self.tableView.reloadData()
            
        }

     }
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func viewDetailAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)!
        if indexPath.section == 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingDetailsViewController") as! BookingDetailsViewController
            vc.bookingId = bookingList?.arr_upcommingbooking[indexPath.row].bookingId ?? ""
            vc.serviceId = bookingList?.arr_upcommingbooking[indexPath.row].serviceId ?? ""
            vc.providerId = bookingList?.arr_upcommingbooking[indexPath.row].providerId ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingDetailsViewController") as! BookingDetailsViewController
            vc.bookingId = bookingList?.arr_prevbooking[indexPath.row].bookingId ?? ""
            vc.serviceId = bookingList?.arr_prevbooking[indexPath.row].serviceId ?? ""
            vc.providerId = bookingList?.arr_prevbooking[indexPath.row].providerId ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

extension MyBookingsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return headersArray.count
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return bookingList?.arr_upcommingbooking.count ?? 0
        }else{
            return bookingList?.arr_prevbooking.count ?? 0
        }
         
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var superCell = UITableViewCell()
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyBookingTableViewCell", for: indexPath) as! MyBookingTableViewCell
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(bookingList?.arr_upcommingbooking[indexPath.row].image ?? "")", imageView: cell.serviceImageview, placeHolder: "")
        cell.providerLabel.text = bookingList?.arr_upcommingbooking[indexPath.row].providerName ?? ""
        cell.serviceLabel.text = bookingList?.arr_upcommingbooking[indexPath.row].serviceName ?? ""
        cell.amountLabel.text = "$\(bookingList?.arr_upcommingbooking[indexPath.row].price ?? "")"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date  = formatter.date(from: bookingList?.arr_upcommingbooking[indexPath.row].bookingDate ?? "")!
        formatter.dateFormat = "d MMM YYYY"
        
        cell.dateTimeLabel.text = "\(formatter.string(from: date)), \(bookingList?.arr_upcommingbooking[indexPath.row].timeSlot ?? "")"
          superCell = cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyBookingTableViewCell", for: indexPath) as! MyBookingTableViewCell
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(bookingList?.arr_prevbooking[indexPath.row].image ?? "")", imageView: cell.serviceImageview, placeHolder: "")
        cell.providerLabel.text = bookingList?.arr_prevbooking[indexPath.row].providerName ?? ""
        cell.serviceLabel.text = bookingList?.arr_prevbooking[indexPath.row].serviceName ?? ""
        cell.amountLabel.text = "$\(bookingList?.arr_prevbooking[indexPath.row].price ?? "")"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date  = formatter.date(from: bookingList?.arr_prevbooking[indexPath.row].bookingDate ?? "")!
        formatter.dateFormat = "d MMM YYYY"
        
        cell.dateTimeLabel.text = "\(formatter.string(from: date)), \(bookingList?.arr_prevbooking[indexPath.row].timeSlot ?? "")"
          superCell = cell
        }
        
        
        return superCell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! MyBookingTableViewCell
        cell.headerLabel.text = headersArray[section]

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
