//
//  PaymentTableViewCell.swift
//  FamousChair
//
//  Created by ginger webs on 10/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var paymentImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }

}
