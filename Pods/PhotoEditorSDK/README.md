<p align="center">
	<a href="https://www.photoeditorsdk.com/?utm_campaign=Projects&utm_source=Github&utm_medium=Side_Projects&utm_content=IOS-Build">
		<img src="https://static.photoeditorsdk.com/logo.png" alt="PhotoEditor SDK Logo"/>
	</a>
	<a href="https://www.videoeditorsdk.com/?utm_campaign=Projects&utm_source=Github&utm_medium=Side_Projects&utm_content=IOS-Build">
		<img src="https://videoeditorsdk.com/assets/img/vesdk-logo-s.svg" alt="VideoEditor SDK Logo"/>
	</a>
</p>
<p align="center">
	<a href="https://cocoapods.org/pods/PhotoEditorSDK">
		<img src="https://img.shields.io/cocoapods/v/PhotoEditorSDK.svg" alt="CocoaPods Compatible">
	</a>
	<a href="https://cocoapods.org/pods/VideoEditorSDK">
		<img src="https://img.shields.io/cocoapods/v/VideoEditorSDK.svg" alt="CocoaPods Compatible">
	</a>
	<a href="https://twitter.com/PhotoEditorSDK">
		<img src="https://img.shields.io/badge/twitter-@PhotoEditorSDK-blue.svg?style=flat" alt="Twitter">
	</a>
</p>

# About PhotoEditor SDK & VideoEditor SDK for iOS

Our SDK provides tools for adding photo and/or video editing capabilities to your iOS application with a big variety of filters that can be previewed in realtime. Unlike other apps that allow a live preview of filters, the PhotoEditor SDK and VideoEditor SDK even provides a live preview when using high-resolution images and videos. The framework is written in Swift and allows for easy customization.
Additionally we support adding stickers and text in a non-destructive manner, which means that you can change the position, size, scale and order at any given time, even after applying other effects or cropping the photo.

## Features

* 62 stunning built in filters to choose from.
* Native code: Our rendering engine is based on Apple's Core Image, therefore we dodge all the nasty OpenGL problems other frameworks face.
* iPad support: The PhotoEditor SDK uses auto layout for its views and adapts to each screen size - iPhone or iPad.
* Design filters in Photoshop: With most photo editing frameworks you have to tweak values in code or copy & paste them from Photoshop or your favorite image editor. With our response technology that is a thing of the past. Design your filter in Photoshop, once you are done apply it onto the provided identity image. That will 'record' the filter response - save it, add it as new filter, done!
* Swift: Keeping up with time, we chose Swift as the main development language of the PhotoEditor SDK, leading to leaner easier code.
* Live preview: Filters can be previewed directly in the camera preview.
* Low memory footprint: We were able to reduce our memory footprint significantly.
* Non-destructive: Don't like what you did? No problem, just redo or even discard it.
* Highly customizable: Style the UI as you wish to match your needs.
* Objective-C support: Most of our public API is Objective-C compatible.
* Fast: Our renderer uses hardware acceleration and the GPU, which makes it lightning fast.

<p>
	<a target="_blank" href="https://www.photoeditorsdk.com/?utm_campaign=Projects&utm_source=Github&utm_medium=Side_Projects&utm_content=IOS-Build">
		<img style="display:block" src="https://docs.photoeditorsdk.com/assets/images/guides/ios/v7/product.jpg?utm_campaign=Projects&utm_source=Github&utm_medium=Side_Projects&utm_content=IOS-Build">
	</a>
</p>

## Integration

For a step-by-step guide to integrate PhotoEditor SDK, please visit [docs.photoeditorsdk.com/guides/ios](https://docs.photoeditorsdk.com/guides/ios/?utm_campaign=Projects&utm_source=Github&utm_medium=Side_Projects&utm_content=IOS-Build) or [docs.videoeditorsdk.com/guides/ios](https://docs.videoeditorsdk.com/guides/ios/?utm_campaign=Projects&utm_source=Github&utm_medium=Side_Projects&utm_content=IOS-Build).

## Running serialization tests

To generate outputs and reserializations for an existing settings file, you need to add it to the `SerializationTests` directory within Xcode. You can then run the `SerializationTests` test case and the resulting files will show up as attachments in Xcodes test inspector. The test is repeated for every `json` file in the aforementioned directory.

To quickly run through all existing files, you can execute the existing batch script as well:
```shell
$ ./scripts/run_serialization_tests.sh
```
This cleans the attachments folder, runs the tests using `xcodebuild` and copies the resulting files to the `serialization_test_results/ios/` directory.

## Getting started to contribute

1. Install `mint`, e.g. via [Homebrew](https://brew.sh):
   ```shell
   $ brew install mint
   ```
2. Install all packages from [`Mintfile`](./Mintfile):
   ```shell
   $ mint bootstrap
   ```
3. Generate `PhotoEditorSDK.xcodeproj` and install Git Hooks via [`Makefile`](./Makefile):
   ```shell
   $ make
   ```

## License Terms

Make sure you have a commercial license before releasing your app.
A commercial license is required for any app or service that has any form of monetization: This includes free apps with in-app purchases or ad supported applications. Please contact us if you want to purchase the commercial license.

## Support

Please use our [Service Desk](https://support.photoeditorsdk.com/) if you have any questions or would like to submit bug reports.
