//
//  HomeMOdel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class HomeModal : NSObject {
    let error : String?
    let message :String?
    var result = [HomeData]()
    var bannerArray = [bannerData]()
    var abouut_us :String?
   
    init(data:JSON) {
        self.abouut_us = data["about_us"].stringValue
        self.error = data["status"].stringValue
        self.message = data["message"].stringValue
        for item in data["data"].arrayValue{
            let homeData :HomeData = HomeData.init(data: item)
            result.append(homeData)
            
        }
        for item in data["banners"].arrayValue{
            let name :bannerData = bannerData.init(data: item)
            bannerArray.append(name)
            
        }
    }

}

class bannerData: NSObject {
    var image : String?
    init(data:JSON) {
        self.image = data["image"].stringValue
        
    }
}
