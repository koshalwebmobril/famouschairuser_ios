//
//  ProviderDetailViewController.swift
//  FamousChair
//
//  Created by ginger webs on 07/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import FloatRatingView
class ProviderDetailViewController: UIViewController {

    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var providerTitleLabel: UILabel!
    @IBOutlet weak var providerImgView: UIImageView!
    @IBOutlet weak var providerNameLabel: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var serviceLabel: UILabel!
    var providerId = Int()
    
    var providerDatail : ProviderModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let parameter = ["provider_id":"\(providerId)"]
        ProviderVM.callGetproviderServicesApi(params: parameter, viewController: self) { (responseObject) in
            self.providerDatail = responseObject
            self.setUIData()
            self.tableView.isHidden = false
            self.detailView.isHidden = false
            self.tableView.reloadData()
        }
   
    }
    
    func setUIData(){
        providerTitleLabel.text = providerDatail?.providerData.name ?? ""
        providerNameLabel.text = providerDatail?.providerData.name ?? ""
//        if providerDatail?.arr_services.count ?? 0 > 0{
//            serviceLabel.text = providerDatail?.arr_services[0].service_name
//        }
        ratingView.rating = providerDatail?.providerData.rating ?? 0.0
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(providerDatail?.providerData.providerImage ?? "")", imageView: providerImgView!,placeHolder: "barberImage")
        
    }
    
     override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(true, animated: true)

     }
    

    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func bookNowAction(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        
        let calenderVC = self.storyboard?.instantiateViewController(withIdentifier: "CalenderViewController") as! CalenderViewController
        calenderVC.serviceId = providerDatail?.arr_services[indexPath?.row ?? 0].service_id ?? 0
        calenderVC.providerId = providerDatail?.providerData.id ?? 0
        calenderVC.providerNameStr = providerDatail?.providerData.name ?? ""
        self.navigationController?.pushViewController(calenderVC, animated: true)
    }
    
}

extension ProviderDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return providerDatail?.arr_services.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderDetailTableViewCell", for: indexPath) as! ProviderDetailTableViewCell
        cell.serviceLabel.text = providerDatail?.arr_services[indexPath.row].service_name ?? ""
        cell.descLabel.text = String(format: "$%.2f and up for 1 Hours", providerDatail?.arr_services[indexPath.row].price ?? 0.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
