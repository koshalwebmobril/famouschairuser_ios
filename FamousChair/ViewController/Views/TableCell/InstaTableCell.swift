//
//  InstaTableCell.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 14/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import AVFoundation

class InstaTableCell: UITableViewCell {
    
    @IBOutlet weak var lbl_post_title: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var videoPlayerSuperView: UIView!
    
    @IBOutlet weak var blurVideo: UIVisualEffectView!
    var context = CIContext(options: nil)
    
    var avPlayer: AVPlayer?
    var avPlayerLayer: AVPlayerLayer?
    var paused: Bool = false
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(stopPlayback), name: NSNotification.Name("stopPlayer"), object: nil)
        
        
        // Initialization code
    }
    
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    @objc func stopPlayback(){
        self.avPlayer?.pause()
    }
    
    func startPlayback(){
        self.avPlayer?.play()
    }
    
    
   
    
}
