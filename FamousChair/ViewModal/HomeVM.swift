//
//  HomeVM.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class HomeVM: NSObject {
    
     class func callHomeApi(params: [String:Any],viewController:UIViewController, completion: @escaping (HomeModal) -> Void)  {
                   ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.home_url, dict: params,viewController: viewController) { (response) in
                    if let data = response{
                       
                       let homeData :HomeModal = HomeModal.init(data:JSON(data))
                       if homeData.error == "success"{
                           completion(homeData)
                       }else{
                           viewController.createAlert(title: "Famous Chair", message: homeData.message ?? "")
                       }
                    }
           }
       }
       
       class func callGetAllServicesApi(params: [String:Any],viewController:UIViewController, completion: @escaping (HomeModal) -> Void)  {
                      ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.getAllServices_url, dict: params,viewController: viewController) { (response) in
                       if let data = response{
                          
                          let homeData :HomeModal = HomeModal.init(data:JSON(data))
                          if homeData.error == "success"{
                              completion(homeData)
                          }else{
                              viewController.createAlert(title: "Famous Chair", message: homeData.message ?? "")
                          }
                       }
              }
          }
}
