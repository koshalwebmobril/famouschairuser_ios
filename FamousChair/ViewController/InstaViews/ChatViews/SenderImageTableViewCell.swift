//
//  SenderImageTableViewCell.swift
//  ChatApp
//
//  Created by Tannu on 27/08/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class SenderImageTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_thumbnail: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var senderImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
