//
//  NotificationVM.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 18/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class NotificationVM: NSObject {
 class func callNotificationServicesApi(params: [String:Any],viewController:UIViewController, completion: @escaping (NotificationModal) -> Void)  {
        ApiManager.shared.getHeaderRequestApi(url: WebServiceApi.getNotifications, dict: params,viewController: viewController) { (response) in
            if let data = response{
                
                let notificationData :NotificationModal = NotificationModal.init(data:JSON(data))
                if notificationData.error == "success"{
                    completion(notificationData)
                    
                }else{
                    viewController.createAlert(title: "Famous Chair", message: notificationData.message ?? "")
                }
            }
        }
    }
    
    class func callInstagramNotificationServicesApi(params: [String:Any],viewController:UIViewController, completion: @escaping (NotificationData) -> Void)  {
        ApiManager.shared.getHeaderRequestApi(url: WebServiceApi.postInsta_notification_url, dict: params,viewController: viewController) { (response) in
            if let data = response{
               
                let notificationData = try! JSONDecoder().decode(NotificationData.self, from: data)
                completion(notificationData)
            }
        }
    }
    
}
