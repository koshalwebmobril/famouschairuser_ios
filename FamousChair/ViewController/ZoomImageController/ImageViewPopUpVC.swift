//
//  ImageViewPopUpVC.swift
//  xtensions4us
//
//  Created by deepkohli on 16/12/19.
//  Copyright © 2019 Rachit Kumar. All rights reserved.
//

import UIKit

class ImageViewPopUpVC: UIViewController,EFImageViewZoomDelegate {
    
    
    @IBOutlet weak var zoomImageView: EFImageViewZoom!
    @IBOutlet weak var crossBtn: UIButton!
    
    var zoomIMage = UIImage()
    var imgUrl = String()
    var isFromChat = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.view.isOpaque = false
        //self.view.backgroundColor = .clear
        self.navigationController?.isNavigationBarHidden = true
        zoomImageView.layer.cornerRadius = 10.0
        zoomImageView.clipsToBounds = true
        zoomImageView.imageView.contentMode = .scaleAspectFit
        crossBtn.layer.cornerRadius = crossBtn.frame.size.width / 2
        crossBtn.clipsToBounds = true
        if isFromChat{
            ApiManager.downloadProfileImage(url: "\(imgUrl)", imageView: self.zoomImageView.imageView, placeHolder: "")
        }else{
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(imgUrl)", imageView: self.zoomImageView.imageView, placeHolder: "")
        }
        self.zoomImageView._delegate = self
//        zoomImageView.image = zoomIMage
        zoomImageView.contentMode = .left

        // Do any additional setup after loading the view.
    }
   
    
     @IBAction func action_dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
      }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
