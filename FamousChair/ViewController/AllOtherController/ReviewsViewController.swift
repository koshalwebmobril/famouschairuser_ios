//
//  ReviewsViewController.swift
//  FamousChair
//
//  Created by ginger webs on 11/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import FloatRatingView

class ReviewsViewController: UIViewController,FloatRatingViewDelegate {

    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var txtReview: UITextField!
    var rating = 0.0
    var serviceId = String()
    var providerId = String()
    var reviewData : RatingData?
    override func viewDidLoad() {
        super.viewDidLoad()

        ratingView.delegate = self
        ratingView.type = .halfRatings
        setDataToUI()
    }
    
    func setDataToUI(){
        if reviewData?.comment != ""{
            self.ratingView.isUserInteractionEnabled = false
            self.txtReview.isUserInteractionEnabled = false
            self.ratingView.rating = reviewData?.rating ?? 0.0
            self.txtReview.text = reviewData?.comment ?? ""
            self.btn_submit.isHidden = true
        }
    }


    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func submitAction(_ sender: Any) {
        if rating == 0.0{
            self.createAlert(title: "Famous Chair", message: "Please give rating")
        }else if txtReview.text!.isEmpty{
            self.createAlert(title: "Famous Chair", message: "Please write reviews.")
        } else{
        let parameter = ["provider_id":providerId,"service_id":serviceId,"rating":
            "\(rating ?? 0.0)","comment":txtReview.text ?? ""]
            BookingVM.callRatingServicesApi(params: parameter, viewController: self, completion: {})
        }
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        self.rating = rating
    }
}
