//
//  BookingVM.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 18/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class BookingVM: NSObject {
    class func callBookingServicesApi(params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.bookServices, dict: params,viewController: viewController) { (response) in
            if let data = response{
                
                let bookingData :BookingModal = BookingModal.init(data:JSON(data))
                if bookingData.error == "success"{
                    let alert = UIAlertController(title: "Famous Chair", message: bookingData.message ?? "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                        let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                         appdelegate.window!.rootViewController = tabBarController
                        
                    }))
                    viewController.present(alert, animated: true, completion: nil)
                }else{
                    viewController.createAlert(title: "Famous Chair", message: bookingData.message ?? "")
                }
            }
        }
    }
    
    class func callBookingListServicesApi(params: [String:Any],viewController:UIViewController, completion: @escaping (BookingList) -> Void)  {
        ApiManager.shared.getHeaderRequestApi(url: WebServiceApi.bookingList, dict: params,viewController: viewController) { (response) in
            if let data = response{
                
                let bookingData :BookingList = BookingList.init(data:JSON(data))
                if bookingData.error == "success"{
                    completion(bookingData)
                }else{
                    viewController.createAlert(title: "Famous Chair", message: bookingData.message ?? "")
                }
            }
        }
    }
    
    class func callBookingDetailsServicesApi(params: String,viewController:UIViewController, completion: @escaping (BookingModal) -> Void)  {
        ApiManager.shared.getHeaderRequestApi(url: "\(WebServiceApi.bookingDetails)?booking_id=\(params)", dict: [String:Any](),viewController: viewController) { (response) in
            if let data = response{
                
                let bookingData :BookingModal = BookingModal.init(data:JSON(data))
                if bookingData.error == "success"{
                    completion(bookingData)
                }else{
                    viewController.createAlert(title: "Famous Chair", message: bookingData.message ?? "")
                }
            }
        }
    }
    
    
    class func callRatingServicesApi(params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.addReview, dict: params,viewController: viewController) { (response) in
            if let data = response{
                
                let providerData :ProviderModal = ProviderModal.init(data:JSON(data))
                if providerData.error == "success"{
                    let alert = UIAlertController(title: "Famous Chair", message: providerData.message ?? "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                        
                            let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                             appdelegate.window!.rootViewController = tabBarController
                    }))
                    viewController.present(alert, animated: true, completion: nil)
                    
                }else{
                    viewController.createAlert(title: "Famous Chair", message: providerData.message ?? "")
                }
            }
        }
    }
    
    class func callCancelBookingServicesApi(params: [String:Any],viewController:UIViewController, completion: @escaping () -> Void)  {
        ApiManager.shared.postHeaderApiRequest(url: WebServiceApi.cancelBooking, dict: params,viewController: viewController) { (response) in
            if let data = response{
                
                let providerData :ProviderModal = ProviderModal.init(data:JSON(data))
                if providerData.error == "success"{
                    let alert = UIAlertController(title: "Famous Chair", message: providerData.message ?? "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                        
                        viewController.navigationController?.popViewController(animated: true)
                    }))
                    viewController.present(alert, animated: true, completion: nil)
                    
                }else{
                    viewController.createAlert(title: "Famous Chair", message: providerData.message ?? "")
                }
            }
        }
    }
    
}
