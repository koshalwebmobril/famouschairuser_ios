//
//  ProfileViewController.swift
//  FamousChair
//
//  Created by ginger webs on 09/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    var labelArray = ["HOME", "EDIT PROFILE","TRANSACTIONS HISTORY","CHANGE PASSWORD","PRIVACY POLICY", "TERMS OF SERVICE", "CONTACT US", "LOG OUT"]
    var imageLabel = ["home3", "user3","dollar","lock1","privacy", "terms", "email", "logout"]
    var profileData :ProfileModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if getStringValue(key: "social") == "social"{
            labelArray = ["HOME", "EDIT PROFILE","TRANSACTIONS HISTORY","PRIVACY POLICY", "TERMS OF SERVICE", "CONTACT US", "LOG OUT"]
            imageLabel = ["home3", "user3","dollar","privacy", "terms", "email", "logout"]
        }
    }
    
    func setUIData(){
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(profileData?.result?.profile_image ?? "")", imageView: userImageView!,placeHolder: "userImg")
        nameLabel.text = profileData?.result?.name ?? ""
        emailLabel.text = profileData?.result?.email ?? ""
        saveStringValue(value: "\(profileData?.result?.profile_image ?? "")", key: UserDefaultKeys.userImage)
        saveStringValue(value: "\(profileData?.result?.name ?? "")", key: UserDefaultKeys.name)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let parameter = [String:Any]()
        ProfileVM.callGetProfileApi(params: parameter, viewController: self) { (responsObject) in
            self.profileData = responsObject
            self.setUIData()
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  labelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
        cell.imageCell.image = UIImage(named: "\(imageLabel[indexPath.row])")
        cell.titleLabel.text = labelArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if getStringValue(key: "social") == "social"{
         if indexPath.row == 0 {
             setRoot()
         }else if indexPath.row == 1 {
             let editVC = homeStoryBoard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
             editVC.profileDict = self.profileData
             self.navigationController?.pushViewController(editVC, animated: true)
         } else if indexPath.row == 2 {
             let transactionVc = homeStoryBoard.instantiateViewController(withIdentifier: "TransactionHistoryViewController") as! TransactionHistoryViewController
             self.navigationController?.pushViewController(transactionVc, animated: true)
         }  else if indexPath.row == 3{
             let Vc = homeStoryBoard.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
             Vc.type = "privacy_policy"
             self.navigationController?.pushViewController(Vc, animated: true)
         } else if indexPath.row == 4{
             let Vc = homeStoryBoard.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
             Vc.type = "terms_service"
             self.navigationController?.pushViewController(Vc, animated: true)
         }else if indexPath.row == 5 {
             let Vc = homeStoryBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
             self.navigationController?.pushViewController(Vc, animated: true)
         } else if indexPath.row == 6 {
             let alert = UIAlertController(title: "Famous Chair", message: "Are you sure! Do you want to logout?", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
                 UserDefaults.standard.removeObject(forKey: "isLoggedIn")
                 let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                 let login = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                 let navigationController = UINavigationController(rootViewController: login)
                 let appdelegate = UIApplication.shared.delegate as! AppDelegate
                 appdelegate.window!.rootViewController = navigationController
             }))
             alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
             self.navigationController?.present(alert, animated: true, completion: nil)
             
         }
         }else{
        if indexPath.row == 0 {
            setRoot()
        }else if indexPath.row == 1 {
            let editVC = homeStoryBoard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            editVC.profileDict = self.profileData
            self.navigationController?.pushViewController(editVC, animated: true)
        } else if indexPath.row == 2 {
            let transactionVc = homeStoryBoard.instantiateViewController(withIdentifier: "TransactionHistoryViewController") as! TransactionHistoryViewController
            self.navigationController?.pushViewController(transactionVc, animated: true)
        } else if indexPath.row == 3 {
            let Vc = mainStoryBoard.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
            Vc.email = emailLabel.text ?? ""
            Vc.isfromProfile = "Yes"
            self.navigationController?.pushViewController(Vc, animated: true)
        }  else if indexPath.row == 4{
            let Vc = homeStoryBoard.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            Vc.type = "privacy_policy"
            self.navigationController?.pushViewController(Vc, animated: true)
        } else if indexPath.row == 5{
            let Vc = homeStoryBoard.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            Vc.type = "terms_service"
            self.navigationController?.pushViewController(Vc, animated: true)
        }else if indexPath.row == 6 {
            let Vc = homeStoryBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            self.navigationController?.pushViewController(Vc, animated: true)
        } else if indexPath.row == 7 {
            
            
            let alert = UIAlertController(title: "Famous Chair", message: "Are you sure! Do you want to logout?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
                UserDefaults.standard.removeObject(forKey: "isLoggedIn")
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let login = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                let navigationController = UINavigationController(rootViewController: login)
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.window!.rootViewController = navigationController
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.navigationController?.present(alert, animated: true, completion: nil)
            
        }
        }
    }
    
    
    func setRoot() {
       
        let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = tabBarController
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
