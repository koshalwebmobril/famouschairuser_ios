//
//  ForgotPasswordViewController.swift
//  FamousChair
//
//  Created by ginger webs on 10/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var forgotTextfield: UITextField!
    var forgotPasswordVM: ForgotPasswordVM?
    override func viewDidLoad() {
        super.viewDidLoad()
         forgotPasswordVM = ForgotPasswordVM()
        forgotPasswordVM?.controller = self
      
    }
    
     override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(false, animated: true)

     }

    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        forgotPasswordVM?.validationOnScreen(email: forgotTextfield.text ?? "")
    }

}

extension ForgotPasswordViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.textInputMode?.primaryLanguage == "emoji" || !(((textField.textInputMode?.primaryLanguage) != nil)) {
            return false
        }
        return string .rangeOfCharacter(from: .whitespacesAndNewlines) == nil
        return true
    }
}
