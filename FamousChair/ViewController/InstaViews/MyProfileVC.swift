//
//  MyProfileVC.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 27/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class MyProfileVC: UIViewController {
    
    @IBOutlet weak var message_view: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var btn_message: UIButton!
    @IBOutlet weak var btn_follow: UIButton!
    @IBOutlet weak var table_postList: UITableView!
    @IBOutlet weak var collection_LikeUserList: UICollectionView!
    @IBOutlet weak var lbl_numberOfTotalPost: UILabel!
    @IBOutlet weak var lbl_numberOfTotaFollowers: UILabel!
    @IBOutlet weak var lbl_numberOfTotalFollowing: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_profilePic: UIImageView!
    @IBOutlet weak var btn_EditProfile: UIButton!
    @IBOutlet weak var btn_AddProfilePic: UIButton!
    @IBOutlet weak var lbl_headerName: UILabel!
    @IBOutlet weak var view_bgTable: UIView!
    @IBOutlet weak var lbl_postText: UILabel!
    @IBOutlet weak var lbl_followersText: UILabel!
    @IBOutlet weak var lbl_followingText: UILabel!
    @IBOutlet weak var lbl_totalCount: UILabel!
    
    var str_selectionType = String()
    var str_UserId = String()
    var int_SelecetdIndex = Int()
    var profileData : InstaProfileModel?
    var followerList : FollowerModel?
    var isFromSearch = false
    
    //66,132,208
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialvalue()
        if str_UserId == getStringValue(key: UserDefaultKeys.userId){
            btn_EditProfile.isHidden = false
            btn_follow.isHidden = true
        }else{
            btn_follow.isHidden = false
            btn_message.isHidden  = false
            btn_EditProfile.isHidden = true
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        callHomePagaDataApI()
    }
    
    func setupInitialvalue(){
        btn_AddProfilePic.layer.cornerRadius = btn_AddProfilePic.frame.size.height / 2
        btn_AddProfilePic.clipsToBounds = true
        img_profilePic.layer.cornerRadius = img_profilePic.frame.size.height / 2
        img_profilePic.clipsToBounds = true
        btn_EditProfile.layer.cornerRadius = 8.0
        btn_EditProfile.clipsToBounds = true
        
        
    }
    
    func callHomePagaDataApI(){
        
        InstagramVM.callGetProfileApi(params: ["user_id":str_UserId], viewController: self, completion: {responseObject in
            self.profileData = responseObject
            self.setDataToUI()
        })
        
    }
    
    
    func setDataToUI(){
        self.profileView.isHidden = false
        
        if self.profileData?.result?.following_status ?? 0 == 1{
            btn_follow.setTitle("Following", for: .normal)
        }
        
        lbl_totalCount.text = "\(self.profileData?.result?.arr_total_post.count ?? 0)" + " Posts"
        lbl_followersText.textColor = UIColor.black
        lbl_postText.textColor =  UIColor(red: 66/255, green: 132/255, blue: 208/255, alpha: 1.0)
        lbl_followingText.textColor = UIColor.black
        
        lbl_numberOfTotalPost.textColor = UIColor(red: 66/255, green: 132/255, blue: 208/255, alpha: 1.0)
        lbl_numberOfTotaFollowers.textColor = UIColor.black
        lbl_numberOfTotalFollowing.textColor = UIColor.black
        
        self.lbl_numberOfTotalPost.text = "\(self.profileData?.result?.total_post_count ?? 0)"
        self.lbl_numberOfTotaFollowers.text = "\(self.profileData?.result?.follower_count ?? 0)"
        self.lbl_numberOfTotalFollowing.text  = "\(self.profileData?.result?.following_count ?? 0)"
        if str_UserId == getStringValue(key: UserDefaultKeys.userId){
            saveStringValue(value: "\(profileData?.result?.userData?.image ?? "")", key: UserDefaultKeys.userImage)
            saveStringValue(value: "\(profileData?.result?.userData?.name ?? "")", key: UserDefaultKeys.name)
        }
       if getStringValue(key: UserDefaultKeys.userId) == str_UserId{
            view_bgTable.isHidden = false
            message_view.isHidden = true
        }else{
        if self.profileData?.result?.following_status ?? 0 == 1{
            view_bgTable.isHidden = false
            message_view.isHidden = true
        }else{
            view_bgTable.isHidden = true
            message_view.isHidden = false
        }
        }
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(profileData?.result?.userData?.image ?? "")", imageView: self.img_profilePic, placeHolder: "userImg")
        self.lbl_name.text = profileData?.result?.userData?.name ?? ""
        
        self.lbl_headerName.text = profileData?.result?.userData?.name ?? ""
        self.collection_LikeUserList.reloadData()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func editProfileButtonAction(_ sender: Any) {
        let editVC = homeStoryBoard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        editVC.name = profileData?.result?.userData?.name ?? ""
        editVC.email = profileData?.result?.userData?.email ?? ""
        editVC.userProfile = profileData?.result?.userData?.image ?? ""
        self.navigationController?.pushViewController(editVC, animated: true)
    }
    @IBAction func editProfilePicButtonAction(_ sender: Any) {
    }
    
    @IBAction func messageAction(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        Vc.userName = profileData?.result?.userData?.name ?? ""
        Vc.userProfilePic = profileData?.result?.userData?.image ?? ""
        Vc.driverId = profileData?.result?.userData?.userId ?? 0
        Vc.driver_DeviceToken = profileData?.result?.userData?.device_token ?? ""
        Vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    @IBAction func btnFollowAction(_ sender: UIButton) {
        if sender.currentTitle ?? "" == "Follow"{
            InstagramVM.callFollowingApi(params: ["following_id" : str_UserId], viewController: self, completion: {response in
                if response{
                    self.btn_follow.setTitle("Following", for: .normal)
                    self.profileData?.result?.follower_count = (self.profileData?.result?.follower_count ?? 0) + 1
                    self.view_bgTable.isHidden = false
                    self.message_view.isHidden = true
                    self.profileData?.result?.following_status = 1
                    self.lbl_numberOfTotaFollowers.text  = "\(self.profileData?.result?.follower_count ?? 0)"
                }
            })
        }else{
            InstagramVM.callUnFollowingApi(params: ["unfollowing_id" : str_UserId], viewController: self, completion: {response in
                if response{
                    self.btn_follow.setTitle("Follow", for: .normal)
                     self.profileData?.result?.follower_count = (self.profileData?.result?.follower_count ?? 0) - 1
                    self.view_bgTable.isHidden = true
                    self.message_view.isHidden = false
                    self.profileData?.result?.following_status = 0
                    self.lbl_numberOfTotaFollowers.text  = "\(self.profileData?.result?.follower_count ?? 0)"
                }
            })
        }
        
    }
    
    @IBAction func playVideoAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: collection_LikeUserList)
        let indexPath = collection_LikeUserList.indexPathForItem(at: position)!
        let path = "\(WebServiceApi.baseImage_url)\(profileData?.result?.arr_total_post[indexPath.row].post_path ?? "")"
        let avPlayerController = AVPlayerViewController()
        avPlayerController.player = AVPlayer(url: URL(string: path)!)
        avPlayerController.player?.play()
        present(avPlayerController, animated: true, completion: nil)
    }
    
    
    @IBAction func postButtonAction(_ sender: Any) {
        str_selectionType = "Post"
        lbl_totalCount.text = "\(self.profileData?.result?.total_post_count ?? 0)" + " Posts"
        lbl_followersText.textColor = UIColor.black
        lbl_postText.textColor =  UIColor(red: 66/255, green: 132/255, blue: 208/255, alpha: 1.0)
        lbl_followingText.textColor = UIColor.black
        lbl_numberOfTotalPost.textColor = UIColor(red: 66/255, green: 132/255, blue: 208/255, alpha: 1.0)
        lbl_numberOfTotaFollowers.textColor = UIColor.black
        lbl_numberOfTotalFollowing.textColor = UIColor.black
        table_postList.isHidden = true
        collection_LikeUserList.isHidden = false
        if self.profileData?.result?.following_status ?? 0 == 1{
        callHomePagaDataApI()
        }
        
    }
    @IBAction func followersButtonAction(_ sender: Any) {
        str_selectionType = "Followers"
        lbl_totalCount.text = "\(self.profileData?.result?.follower_count ?? 0)" + " Followers"
        lbl_followersText.textColor = UIColor(red: 66/255, green: 132/255, blue: 208/255, alpha: 1.0)
        lbl_postText.textColor = UIColor.black
        lbl_followingText.textColor = UIColor.black
        lbl_numberOfTotaFollowers.textColor = UIColor(red: 66/255, green: 132/255, blue: 208/255, alpha: 1.0)
        lbl_numberOfTotalPost.textColor = UIColor.black
        lbl_numberOfTotalFollowing.textColor = UIColor.black
        table_postList.isHidden = false
        collection_LikeUserList.isHidden = true
//        if self.profileData?.result?.following_status ?? 0 == 1{
        InstagramVM.callFollowerListApi(params: ["user_id":str_UserId], viewController: self, completion: {responseObject in
            self.followerList = responseObject
            self.lbl_totalCount.text = "\(self.followerList?.result.count ?? 0)" + " Followers"
            self.table_postList.reloadData()
        })
//        }
    }
    @IBAction func followingButtonAction(_ sender: Any) {
        str_selectionType = "Following"
        lbl_totalCount.text = "\(self.profileData?.result?.following_count ?? 0)" + " Following"
        lbl_followersText.textColor = UIColor.black
        lbl_postText.textColor = UIColor.black
        lbl_followingText.textColor = UIColor(red: 66/255, green: 132/255, blue: 208/255, alpha: 1.0)
        lbl_numberOfTotalFollowing.textColor = UIColor(red: 66/255, green: 132/255, blue: 208/255, alpha: 1.0)
        lbl_numberOfTotalPost.textColor = UIColor.black
        lbl_numberOfTotaFollowers.textColor = UIColor.black
        table_postList.isHidden = false
        collection_LikeUserList.isHidden = true
//        if self.profileData?.result?.following_status ?? 0 == 1{
        InstagramVM.callFollowingListApi(params: ["user_id":str_UserId], viewController: self, completion: {responseObject in
            self.followerList = responseObject
            self.lbl_totalCount.text = "\(self.followerList?.result.count ?? 0)" + " Following"
            self.table_postList.reloadData()
        })
//        }
    }
    
    @IBAction func btn_unfollowAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: table_postList)
        let indexPath = table_postList.indexPathForRow(at: position)
        
        if sender.currentTitle ?? "" == "Follow"{
            InstagramVM.callFollowingApi(params: ["following_id":"\(followerList?.result[indexPath?.row ?? 0].id ?? 0)"], viewController: self, completion: {response in
                if response{
                    if getStringValue(key: UserDefaultKeys.userId) == self.str_UserId{
                    
                    self.profileData?.result?.following_count = (self.profileData?.result?.following_count ?? 0) + 1
                    self.lbl_numberOfTotalFollowing.text  = "\(self.profileData?.result?.following_count ?? 0)"
                    }
                    self.followerList?.result[indexPath?.row ?? 0].following_status = 1
                    self.table_postList.reloadData()
                }
                
            })
        }else{
            InstagramVM.callUnFollowingApi(params: ["unfollowing_id" : "\(followerList?.result[indexPath?.row ?? 0].id ?? 0)"], viewController: self, completion: {response in
                if response{
                     self.followerList?.result[indexPath?.row ?? 0].following_status = 0
                    if getStringValue(key: UserDefaultKeys.userId) == self.str_UserId{
                    self.profileData?.result?.following_count = (self.profileData?.result?.following_count ?? 0) - 1
                    if self.str_selectionType == "Followers"{
                       
                    }else{
                        self.lbl_totalCount.text = "\(self.profileData?.result?.following_count ?? 0)" + " Following"
                    self.followerList?.result.remove(at: indexPath?.row ?? 0)
                    }
                    self.lbl_numberOfTotalFollowing.text  = "\(self.profileData?.result?.following_count ?? 0)"
                    }
                    self.table_postList.reloadData()
                }
            })
        }
    }
    
}


extension MyProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return profileData?.result?.arr_total_post.count ?? 0
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionViewCell", for: indexPath) as! ProfileCollectionViewCell
        if profileData?.result?.arr_total_post[indexPath.row].thumbnail ?? "" == ""{
            cell.playBtn.isHidden = true
            ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(profileData?.result?.arr_total_post[indexPath.row].post_path ?? "")", imageView: cell.img_bgView, placeHolder: "userImg")
        }else{
            cell.playBtn.isHidden = false
            ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(profileData?.result?.arr_total_post[indexPath.row].thumbnail ?? "")", imageView: cell.img_bgView, placeHolder: "userImg")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellsAcross: CGFloat = 3
        let spaceBetweenCells: CGFloat = 5
        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        return CGSize(width: collectionView.bounds.width / 3 - 7, height: dim)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewAllPostedController") as! ViewAllPostedController
        vc.userId = self.str_UserId
        vc.postId = profileData?.result?.arr_total_post[indexPath.row].id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
            

    }
    
    
    
}
extension MyProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return followerList?.result.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableCell", for: indexPath) as! LikeUserListTableCell
        
        cell.view_Bg.layer.cornerRadius = 4.0
        cell.view_Bg.layer.borderWidth = 1.0
        cell.view_Bg.layer.borderColor = UIColor.lightGray.cgColor
        if getStringValue(key: UserDefaultKeys.userId) == "\(followerList?.result[indexPath.row].id ?? 0)"{
            cell.btn_profilefollow.isHidden = true
        }else{
            cell.btn_profilefollow.isHidden = false
        if str_selectionType == "Followers"{
            if followerList?.result[indexPath.row].following_status ?? 0 == 0 {
                cell.btn_profilefollow.setTitle("Follow", for: .normal)
            }else{
            cell.btn_profilefollow.setTitle("UnFollow", for: .normal)
            }
        }
        else{
            if followerList?.result[indexPath.row].following_status ?? 0 == 0 {
                cell.btn_profilefollow.setTitle("Follow", for: .normal)
            }else{
            cell.btn_profilefollow.setTitle("Remove", for: .normal)
            }
//            cell.btn_profilefollow.setTitle("Following", for: .normal)
        }
        }
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(followerList?.result[indexPath.row].image ?? "")", imageView: cell.img_profileuser, placeHolder: "userImg")
        cell.lbl_profileuserName.text = followerList?.result[indexPath.row].name ?? ""
        cell.img_profileuser.layer.cornerRadius = cell.img_profileuser.frame.size.width / 2
        cell.img_profileuser.clipsToBounds = true
        cell.btn_profilefollow.tag = indexPath.row
//        cell.btn_profilefollow.addTarget(self, action: #selector(followBtnAction(sender:)), for: .touchUpInside)
        cell.btn_userprofile.tag = indexPath.row
//        cell.btn_userprofile.addTarget(self, action: #selector(userprofileBtnAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    @objc func followBtnAction(sender: UIButton){
    }
    @objc func userprofileBtnAction(sender: UIButton){
    }
    
}
