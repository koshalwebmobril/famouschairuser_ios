//
//  ProfileModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
struct ProfileModal : Codable {
    let error : String?
    let message : String?
    let result : ProfileData?

    enum CodingKeys: String, CodingKey {

        case error = "status"
        case message = "message"
        case result = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        error = try values.decodeIfPresent(String.self, forKey: .error)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent(ProfileData.self, forKey: .result)
    }

}

struct ProfileData :Codable {
    var id : Int?
    var name:String?
    var profile_image : String?
    var email : String?
    var address : String?
    var license_no:String?
    var service_type : String?
    var tax_id : String?
    var business_no : String?
   
    enum Codingkeys:String, CodingKey{
        case id = "id"
        case name = "name"
        case profile_image = "profile_image"
        case email = "email"
        case address = "address"
        case license_no = "license_no"
        case service_type = "service_type"
        case tax_id = "tax_id"
        case business_no = "business_no"
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decodeIfPresent(Int.self, forKey: .id)
        self.name = try values.decodeIfPresent(String.self, forKey: .name)
        self.profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        self.email = try values.decodeIfPresent(String.self, forKey: .email)
        self.address = try values.decodeIfPresent(String.self, forKey: .address)
        self.license_no = try values.decodeIfPresent(String.self, forKey: .license_no)
        self.service_type = try values.decodeIfPresent(String.self, forKey: .service_type)
        self.tax_id = try values.decodeIfPresent(String.self, forKey: .tax_id)
        self.business_no = try values.decodeIfPresent(String.self, forKey: .business_no)
        
    }
}

