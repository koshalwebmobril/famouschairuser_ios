//
//  CommonTableViewPlaceholderView.swift
//  SpeedTester.EU
//
//  Created by Rajeev Sharma on 2018-10-20.
//  Copyright © 2018 TargetIcon Digital Services Pvt. Ltd. All rights reserved.
//

import UIKit

class CommonTableViewPlaceholderView: UIView {

    @IBOutlet internal weak var placeholderIconImageView: UIImageView!
    @IBOutlet internal weak var placeholderTitleLabel: UILabel!
    
    override internal func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instantiate(_ title : String? = nil, image : UIImage? = nil, imageTint : UIColor? = nil) -> CommonTableViewPlaceholderView? {
        guard let viewInstance = CommonTableViewPlaceholderView.nibInstance as? CommonTableViewPlaceholderView else { return nil }
        viewInstance.placeholderTitleLabel.text = title
        viewInstance.placeholderIconImageView.isHidden = image == nil
        if let tint = imageTint {
            viewInstance.placeholderIconImageView.tintColor = tint
            viewInstance.placeholderIconImageView.image = image?.withRenderingMode(.alwaysTemplate)
        } else { viewInstance.placeholderIconImageView.image = image }
        return viewInstance
    }

}
