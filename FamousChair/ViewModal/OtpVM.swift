//
//  LoginVM.swift
//  FamousChair
//
//  Created by ginger webs on 01/07/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import Alamofire

class OtpVM: NSObject {
 //   static let shared = OtpVM()
    var controller = OtpViewController()
    var constant = WebServiceApi()
    var emailValue = String()
    //MARKL: Validations
    
    func validationOnScreen(first: String, second: String, third: String, fourth: String, fifth: String, sixth: String,controller:UIViewController) {
         if first == "" || second == "" || third == "" || fourth == ""  || fifth == "" || sixth == "" {
            controller.createAlert(title: "Famous Chair", message: "Textfield can't be empty.")
             
            } else {
            let optString = first + second + third + fourth + fifth + sixth
            let param = ["email" : emailValue, "otp" : optString]
            OtpApi(param: param,viewController:controller)
        }
    }
}

extension OtpVM {
    
    func OtpApi(param : [String : String],viewController:UIViewController) {
            ApiManager.shared.postApiRequest(url: WebServiceApi.Otp, dict: param,viewController: viewController) { (response) in
            if let responseData = response {
                let otpData = try! JSONDecoder().decode(OtpModal.self, from: responseData)
                if otpData.status == "success" {
                self.controller.saveUserId(id: otpData.result?.id ?? 0)
                self.controller.saveToken(token: (otpData.result?.token ?? ""))
               let Vc = mainStoryBoard.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
                    Vc.email = self.emailValue
                    self.controller.navigationController?.pushViewController(Vc, animated: true)
                } else {
                    viewController.createAlert(title: "Famous Chair", message: otpData.message ?? "")
            }
        }
    }
}
}

