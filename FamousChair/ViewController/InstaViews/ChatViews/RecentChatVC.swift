//
//  RecentChatVC.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 04/11/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import Firebase

class RecentChatVC: UIViewController {
    
    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var recentChats:[RecentChats] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(fetchShowDataWithBlank), name:Notification.Name( "fetchShowDataWithBlank"), object: nil)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func fetchShowDataWithBlank(){
        tableView.isHidden = false
        blankView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        FireBaseManager.defaultManager.recentChatsManagerDelegate = self
        FireBaseManager.defaultManager.fetchRecentChats()
        //        self.observeRecentUpdates()
    }
    
    
    @IBAction func searchAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InstaSearchViewController") as! InstaSearchViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension RecentChatVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentChats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentChatCell", for: indexPath) as! RecentChatCell
        if recentChats[indexPath.row].senderId ?? "" == getStringValue(key: UserDefaultKeys.userId){
            cell.nameLabel.text = recentChats[indexPath.row].recieverName
            cell.lastMsgLabel.text = recentChats[indexPath.row].lastMsg
            setProfileImage(userImage: cell.userImgView, urlString: recentChats[indexPath.row].recieverImg ?? "userImg")
            
        }else{
            cell.nameLabel.text = recentChats[indexPath.row].senderName
            cell.lastMsgLabel.text = recentChats[indexPath.row].lastMsg
            setProfileImage(userImage: cell.userImgView, urlString: recentChats[indexPath.row].senderImg ?? "userImg")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        if recentChats[indexPath.row].senderId ?? "" == getStringValue(key: UserDefaultKeys.userId){
            Vc.userName = recentChats[indexPath.row].recieverName ?? ""
            Vc.userProfilePic = recentChats[indexPath.row].recieverImg ?? "userImg"
            Vc.driverId = Int(recentChats[indexPath.row].recieverId ?? "") ?? 0
            Vc.driver_DeviceToken = recentChats[indexPath.row].device_token ?? ""
        }else{
            Vc.userName = recentChats[indexPath.row].senderName ?? ""
            Vc.userProfilePic = recentChats[indexPath.row].senderImg ?? ""
            Vc.driverId = Int(recentChats[indexPath.row].senderId ?? "") ?? 0
            Vc.driver_DeviceToken = recentChats[indexPath.row].fcm_token ?? ""
            
        }
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    
}
extension RecentChatVC : FirebaseRecentChatsManagerDelegate {
    
    func setProfileImage(userImage: UIImageView, urlString: String) {
        if urlString != "" {
            let userProfileImage = WebServiceApi.baseImage_url + urlString
            userImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage:UIImage(named: "userImg"), options: .highPriority, completed: { (image, error, cache, url) in
                userImage.image = image
            })
        } else {
            userImage.image = UIImage(named: "userImg")
        }
    }
    
    func fireBaseManagerDidCompleteFetchingRecentChats() {
        LoaderClass.stopLoader()
        recentChats = FireBaseManager.defaultManager.recentChats
        print("Chat list:\(recentChats.count)")
        
        if recentChats.count > 0 {
            self.tableView.isHidden = false
            self.blankView.isHidden = true
        }else{
            self.tableView.isHidden = true
            self.blankView.isHidden = false
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
