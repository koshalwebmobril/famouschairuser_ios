//
//  AllCatagoriesCollectionViewCell.swift
//  FamousChair
//
//  Created by Tannu on 15/07/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class AllCatagoriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoty_name: UILabel!
    @IBOutlet weak var category_imageView: UIImageView!
    
}
