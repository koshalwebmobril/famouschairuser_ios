//
//  socialData.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 07/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
struct socialData : Codable {
    let id : Int?
    let name : String?
    let email : String?
    let email_verified_at : String?
    let login_type : String?
    let social_token : String?
    let otp : String?
    let otp_verify : String?
    let service_type : String?
    let location : String?
    let license_no : String?
    let tax_id : String?
    let business_no : String?
    let device_type : String?
    let device_token : String?
    let created_at : String?
    let updated_at : String?
    let token : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case email = "email"
        case email_verified_at = "email_verified_at"
        case login_type = "login_type"
        case social_token = "social_token"
        case otp = "otp"
        case otp_verify = "otp_verify"
        case service_type = "service_type"
        case location = "location"
        case license_no = "license_no"
        case tax_id = "tax_id"
        case business_no = "business_no"
        case device_type = "device_type"
        case device_token = "device_token"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case token = "token"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at)
        login_type = try values.decodeIfPresent(String.self, forKey: .login_type)
        social_token = try values.decodeIfPresent(String.self, forKey: .social_token)
        otp = try values.decodeIfPresent(String.self, forKey: .otp)
        otp_verify = try values.decodeIfPresent(String.self, forKey: .otp_verify)
        service_type = try values.decodeIfPresent(String.self, forKey: .service_type)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        license_no = try values.decodeIfPresent(String.self, forKey: .license_no)
        tax_id = try values.decodeIfPresent(String.self, forKey: .tax_id)
        business_no = try values.decodeIfPresent(String.self, forKey: .business_no)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        token = try values.decodeIfPresent(String.self, forKey: .token)
    }

}
