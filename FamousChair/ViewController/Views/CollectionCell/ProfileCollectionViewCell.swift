//
//  ProfileCollectionViewCell.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 27/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img_bgView: UIImageView!
    
    @IBOutlet weak var playBtn: UIButton!
}
