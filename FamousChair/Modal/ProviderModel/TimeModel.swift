//
//  TimeModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 04/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON

class CalenderModel: NSObject {
    var error :String?
    var message : String?
    var serviceData = ServiceData()
    var service_availibility = [TimeModel]()
    
    
    init(calenderData:JSON) {
        self.error = calenderData["status"].stringValue
        self.message = calenderData["message"].stringValue
        self.serviceData = ServiceData.init(data: calenderData["data"]["service_data"])
        for item in calenderData["data"]["service_availibility"].arrayValue{
            let time = TimeModel.init(timeData: item)
            service_availibility.append(time)
            
        }
        
    }
    
}


class TimeModel: NSObject {
    var arr_morning = [Time]()
    var arr_afternoon = [Time]()
    var arr_evening = [Time]()
    var availabeDate : String?
    
    init(timeData:JSON) {
        availabeDate = timeData["availability_date"].stringValue
        for item in timeData["morning_slot"].arrayValue{
            let time = Time.init(time: item.stringValue)
            arr_morning.append(time)
            
        }
        for item in timeData["afternoon_slot"].arrayValue{
            let time = Time.init(time: item.stringValue)
            arr_afternoon.append(time)
        }
        for item in timeData["evening_slot"].arrayValue{
            let time = Time.init(time: item.stringValue)
            arr_evening.append(time)
        }
    }
    
}

class Time: NSObject {
    var time : String?
    init(time:String) {
        self.time = time
    }
}
