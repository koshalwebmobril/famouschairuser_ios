//
//  NotificationViewController.swift
//  FamousChair
//
//  Created by ginger webs on 09/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var blankImgView: UIImageView!
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var arr_notification : NotificationModal?
    var insta_arr_notification : NotificationData?
    var isFromInstagram = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !isFromInstagram{
            backBtn.isHidden = true
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if isFromInstagram{
            NotificationVM.callInstagramNotificationServicesApi(params: [String:Any](), viewController: self) { (responseObject) in
                self.insta_arr_notification = responseObject
                if self.insta_arr_notification?.data?.notify?.count ?? 0 > 0{
                    self.tableView.isHidden = false
                    self.blankImgView.isHidden = true
                }else{
                    self.tableView.isHidden = true
                    self.blankImgView.isHidden = false
                }
                
                self.tableView.reloadData()
            }
        }else{
            NotificationVM.callNotificationServicesApi(params: [String:Any](), viewController: self) { (responseObject) in
                self.arr_notification = responseObject
                if self.arr_notification?.arr_notification.count ?? 0 > 0{
                    self.tableView.isHidden = false
                    self.blankImgView.isHidden = true
                }else{
                    self.tableView.isHidden = true
                    self.blankImgView.isHidden = false
                }
                
                self.tableView.reloadData()
            }
        }
    }
    
    
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (isFromInstagram == true) ? self.insta_arr_notification?.data?.notify?.count ?? 0 : arr_notification?.arr_notification.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        cell.titleLabel.text = (isFromInstagram == true) ? self.insta_arr_notification?.data?.notify?[indexPath.row].name ?? "" : arr_notification?.arr_notification[indexPath.row].title ?? ""
        cell.messageLabel.text = (isFromInstagram == true) ? self.insta_arr_notification?.data?.notify?[indexPath.row].title ?? "" : arr_notification?.arr_notification[indexPath.row].message ?? ""
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let date  = formatter.date(from: (isFromInstagram == true) ? self.insta_arr_notification?.data?.notify?[indexPath.row].created_at ?? "" : arr_notification?.arr_notification[indexPath.row].date ?? "") ?? Date()
        formatter.dateFormat = "d MMM YYYY"
        cell.dateLabel.text = formatter.string(from: date)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
}
