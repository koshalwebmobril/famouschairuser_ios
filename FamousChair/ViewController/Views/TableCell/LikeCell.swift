//
//  LikeCell.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 14/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class LikeCell: UITableViewCell {

    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
