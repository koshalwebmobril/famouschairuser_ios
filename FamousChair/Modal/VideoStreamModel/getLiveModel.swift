//
//  getLiveModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 25/11/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
struct getLiveModal : Codable {
    let code : Int?
    let status : Bool?
    let message : String?
    let data : [getLiveData]?

    enum CodingKeys: String, CodingKey {

        case code = "code"
        case status = "status"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([getLiveData].self, forKey: .data)
    }

}


struct getLiveData : Codable {
    let id : Int?
    let user_id : Int?
    let channelname : String?
    let token : String?
    let live_status : String?
    let created_at : String?
    let updated_at : String?
    let name : String?
    let email : String?
    let mobile : String?
    let dob : String?
    let gender : String?
    let profile_img : String?
    let profile_wall_img : String?
    let email_verified_at : String?
    let password : String?
    //let otp : String?
    //let verify_otp : String?
    let device_type : String?
    let device_token : String?
    let remember_token : String?
    let influce : String?
    let influencer_doc : String?
    let user_type : Int?
    let social_token : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case channelname = "channelname"
        case token = "token"
        case live_status = "live_status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case name = "name"
        case email = "email"
        case mobile = "mobile"
        case dob = "dob"
        case gender = "gender"
        case profile_img = "profile_img"
        case profile_wall_img = "profile_wall_img"
        case email_verified_at = "email_verified_at"
        case password = "password"
        //case otp = "otp"
        //case verify_otp = "verify_otp"
        case device_type = "device_type"
        case device_token = "device_token"
        case remember_token = "remember_token"
        case influce = "influce"
        case influencer_doc = "influencer_doc"
        case user_type = "user_type"
        case social_token = "social_token"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        channelname = try values.decodeIfPresent(String.self, forKey: .channelname)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        live_status = try values.decodeIfPresent(String.self, forKey: .live_status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        profile_img = try values.decodeIfPresent(String.self, forKey: .profile_img)
        profile_wall_img = try values.decodeIfPresent(String.self, forKey: .profile_wall_img)
        email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        //otp = try values.decodeIfPresent(String.self, forKey: .otp)
    //    verify_otp = try values.decodeIfPresent(String.self, forKey: .verify_otp)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        remember_token = try values.decodeIfPresent(String.self, forKey: .remember_token)
        influce = try values.decodeIfPresent(String.self, forKey: .influce)
        influencer_doc = try values.decodeIfPresent(String.self, forKey: .influencer_doc)
        user_type = try values.decodeIfPresent(Int.self, forKey: .user_type)
        social_token = try values.decodeIfPresent(String.self, forKey: .social_token)
    }

}
