//
//  MyBookingTableViewCell.swift
//  FamousChair
//
//  Created by ginger webs on 09/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class MyBookingTableViewCell: UITableViewCell {
// HeaderCell
    
    @IBOutlet weak var headerLabel: UILabel!
    
    //content cell
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var providerLabel: UILabel!
    @IBOutlet weak var serviceImageview: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }



}
