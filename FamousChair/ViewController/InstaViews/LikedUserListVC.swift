//
//  LikedUserListVC.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 14/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class LikedUserListVC: UIViewController {

    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var lbl_likeCount: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var arr_userlist :LikedUserModel?
    var postId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        InstagramVM.callGetLikeUserApi(params: ["post_id":postId], viewController: self, completion: {responseObject in
            self.arr_userlist = responseObject
            let strCount: String = (self.arr_userlist?.arr_userlist.count ?? 0 == 0||self.arr_userlist?.arr_userlist.count ?? 0 == 1) ? "\(self.arr_userlist?.arr_userlist.count ?? 0) Like" : "\(self.arr_userlist?.arr_userlist.count ?? 0) Likes"
            self.lbl_likeCount.text = "\(strCount)"
            if self.arr_userlist?.arr_userlist.count ?? 0 > 0{
                self.tableView.isHidden = false
                self.blankView.isHidden = true
            }else{
            self.tableView.isHidden = true
                self.blankView.isHidden = false
            }
            self.tableView.reloadData()
        })
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createNewPost(_ sender: Any) {
    }
    
    @IBAction func followAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        if self.arr_userlist?.arr_userlist[indexPath?.row ?? 0].follow_status ?? 0 == 0{
        InstagramVM.callFollowingApi(params: ["following_id":arr_userlist?.arr_userlist[indexPath?.row ?? 0].user_id ?? ""], viewController: self, completion: {response in
            if response{
                self.arr_userlist?.arr_userlist[indexPath?.row ?? 0].follow_status = 1
                self.tableView.reloadData()
            }
            
        })
        }else{
            InstagramVM.callUnFollowingApi(params: ["unfollowing_id" : arr_userlist?.arr_userlist[indexPath?.row ?? 0].user_id ?? ""], viewController: self, completion: {response in
                if response{
                    self.arr_userlist?.arr_userlist[indexPath?.row ?? 0].follow_status = 0
                     self.tableView.reloadData()
                }
            })
        }
        
    }
}


extension LikedUserListVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_userlist?.arr_userlist.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LikeCell", for: indexPath) as! LikeCell
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(arr_userlist?.arr_userlist[indexPath.row].profile_image ?? "")", imageView: cell.profileImage, placeHolder: "userImg")
        cell.nameLabel.text = arr_userlist?.arr_userlist[indexPath.row].name ?? ""
        let titleStr = (self.arr_userlist?.arr_userlist[indexPath.row].follow_status ?? 0 == 0) ? "Follow" :"Following"
        cell.followBtn.setTitle(titleStr, for: .normal)
        if getStringValue(key: UserDefaultKeys.userId) == arr_userlist?.arr_userlist[indexPath.row].user_id ?? ""{
            cell.followBtn.isHidden = true
        }else{
            cell.followBtn.isHidden = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
}
