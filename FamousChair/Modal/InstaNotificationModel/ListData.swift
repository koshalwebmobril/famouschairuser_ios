
import Foundation
struct ListData : Codable {
	let notify : [ListArray]?

	enum CodingKeys: String, CodingKey {

		case notify = "notify"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		notify = try values.decodeIfPresent([ListArray].self, forKey: .notify)
	}

}
