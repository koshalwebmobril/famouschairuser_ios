//
//  RegisterViewController.swift
//  FamousChair
//
//  Created by ginger webs on 10/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController{

    @IBOutlet weak var signupView: UIView!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var nameTextfield: UITextField!
    
    var signUpVM : SignUpVM?
    override func viewDidLoad() {
        super.viewDidLoad()

     let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
    navigationItem.leftBarButtonItem = backButton
         signUpVM = SignUpVM()
//        signUpVM?.controller = self
        nameTextfield.autocapitalizationType = .words
        
        signupView.layer.masksToBounds =  true
        signupView.layer.cornerRadius = 20
        signupView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
    }
    


    @IBAction func signButtonAction(_ sender: Any) {
        
//        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//        self.navigationController?.pushViewController(Vc, animated: true)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        signUpVM?.validationOnScreen(name: nameTextfield.text ?? "", email: emailTextfield.text ?? "", password: passwordTextfield.text ?? "", confirmPassword: confirmPasswordTextField.text ?? "", controller: self)
    }
    

    
}

extension RegisterViewController:  UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let _char = string.cString(using: String.Encoding.utf8)
        let isBackSpace: Int = Int(strcmp(_char, "\\b"))
        if isBackSpace == -92 {
            return true
        }
        if textField.textInputMode?.primaryLanguage == "emoji" || !(((textField.textInputMode?.primaryLanguage) != nil)) {
            return false
        }
         return string .rangeOfCharacter(from: .whitespacesAndNewlines) == nil
        return true
    }
}
