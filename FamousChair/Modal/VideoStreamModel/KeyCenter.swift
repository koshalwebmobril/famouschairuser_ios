//
//  KeyCenter.swift
//  OpenLive
//
//  Created by GongYuhua on 6/25/16.
//  Copyright © 2016 Agora. All rights reserved.
//

struct KeyCenter {
    static let AppId: String = "b7a721723b1c4ce88bb0043f20af4fca" // "1466bb747d7f49b9ac2b0d6d265fbac2"
    // assign token to nil if you have not enabled app certificate
    static var Token: String? = defalut.string(forKey: "liveTocken")  ?? ""
    static var ChannelName: String? = defalut.string(forKey: "channelName")  ?? ""
}
