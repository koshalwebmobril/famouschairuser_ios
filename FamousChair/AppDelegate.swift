//
//  AppDelegate.swift
//  FamousChair
//
//  Created by ginger webs on 05/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import SwiftyJSON
import GoogleSignIn
import Braintree
import SquarePointOfSaleSDK
import ImglyKit
import PhotoEditorSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if let licenseURL = Bundle.main.url(forResource: "pesdk_ios_license", withExtension: "") {
          PESDK.unlockWithLicense(at: licenseURL)
        }
        
        if let licenseURL = Bundle.main.url(forResource: "vesdk_ios_license", withExtension: "") {
          VESDK.unlockWithLicense(at: licenseURL)
        }
        BTAppSwitch.setReturnURLScheme("com.famouschair.user.payments")
        application.registerForRemoteNotifications()
        registerForPushNotifications()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().backgroundColor = .clear
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = true
//        Thread.sleep(forTimeInterval: 3)
        
        GIDSignIn.sharedInstance()?.clientID = "870398265157-q8gpr21q3u21qrnb70akbuhi7nren6tr.apps.googleusercontent.com"
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = .clear
            UINavigationBar.appearance().standardAppearance = navBarAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = navBarAppearance
        } else {
            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
                statusBar.backgroundColor = .clear
            }
            UIApplication.shared.statusBarStyle = .lightContent
        }
        
        if UserDefaults.standard.value(forKey: "isLoggedIn") as? String == "true" {
            let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window!.rootViewController = tabBarController
        }
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.layoutIfNeededOnUpdate = true
        
        
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare("com.famouschair.user.payments") == .orderedSame {
            
            return BTAppSwitch.handleOpen(url, options: options)
        }
        
        if url.scheme?.localizedCaseInsensitiveCompare("famouschairuser") == .orderedSame{
            //            guard SCCAPIResponse.isSquareResponse(url) else {return false}
            
            do {
                let response = try SCCAPIResponse(responseURL: url)
                
                if let error = response.error {
                    // Handle a failed request.
                    self.window?.rootViewController?.createAlert(title: "Error!", message: response.error?.localizedDescription ?? "")
                    print(error.localizedDescription)
                } else {
                    print("Payment successful")
                }
                
            } catch let error as NSError {
                // Handle unexpected errors.
                print(error.localizedDescription)
            }
            return true
        }
        
        return GIDSignIn.sharedInstance().handle(url)
        
        
    }
    
    
    
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name("ExitCurrentUser"), object: nil)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name("ExitCurrentUser"), object: nil)
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Bankroll")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}


extension AppDelegate :MessagingDelegate,UNUserNotificationCenterDelegate  {
    
    func setRoot() {
        
        let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = tabBarController
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current() // 1
            .requestAuthorization(options: [.alert, .sound, .badge]) { // 2
                granted, error in
                print("Permission granted: \(granted)") // 3
                self.getNotificationSettings()
                
        }
    }
    
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings{
            (setting) in
            guard setting.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        let fToken = fcmToken
        print("FCM toekn is : \(fcmToken)")
        saveStringValue(value: fToken, key: UserDefaultKeys.fcmToken)
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
        let userInfo = notification.request.content.userInfo
        let swiftyJson = JSON(userInfo)
        print(swiftyJson)
        if swiftyJson["type"].stringValue == "live notification"{
            
            NotificationCenter.default.post(name: NSNotification.Name("LiveUserRefersh"), object: nil, userInfo: nil)
        }else{
            
        }
        
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        let swiftyJson = JSON(userInfo)
        print(swiftyJson)
        if swiftyJson["type"].stringValue == "live notification"{
            NotificationCenter.default.post(name: NSNotification.Name("ExitUser"), object: userInfo)
        }else if swiftyJson["gcm.notification.booking_status"].stringValue == "1" || swiftyJson["gcm.notification.booking_status"].stringValue == "2" || swiftyJson["gcm.notification.booking_status"].stringValue == "3" || swiftyJson["gcm.notification.booking_status"].stringValue == "4" || swiftyJson["gcm.notification.booking_status"].stringValue == "0"  {
            let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
            tabBarController.selectedIndex = 1
            self.window!.rootViewController = tabBarController

        }else if swiftyJson["gcm.notification.notification_type"].stringValue == "follow"{
            let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
            tabBarController.selectedIndex = 4
            self.window!.rootViewController = tabBarController
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)  {
                NotificationCenter.default.post(name: NSNotification.Name("followPost"), object: nil, userInfo: userInfo)
            }
            
        }else if swiftyJson["gcm.notification.notification_type"].stringValue == "like"{
            let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
            tabBarController.selectedIndex = 4
            self.window!.rootViewController = tabBarController
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)  {
                NotificationCenter.default.post(name: NSNotification.Name("likePost"), object: nil, userInfo: userInfo)
            }
        }else if swiftyJson["gcm.notification.notification_type"].stringValue == "comment"{
            let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
            tabBarController.selectedIndex = 4
            self.window!.rootViewController = tabBarController
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)  {
                NotificationCenter.default.post(name: NSNotification.Name("commentPost"), object: nil, userInfo: userInfo)
            }
        }else if swiftyJson["type"].stringValue == "chat"{
            let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
            tabBarController.selectedIndex = 4
            self.window!.rootViewController = tabBarController
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)  {
                NotificationCenter.default.post(name: .openChatWindow, object: nil, userInfo: userInfo)
            }
        }else{
            
        }
        
        completionHandler()
        
    }
}


extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
