//
//  InstaSearchViewController.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 29/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class InstaSearchViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txt_search: UITextField!
    var searchData : SearchUserModel?
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        
    }
    
    func setUI(){
        let searchView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: txt_search.frame.height))
        let button = UIButton(frame: CGRect(x: 10, y: 10, width: 20, height: 20))
        button.setImage(UIImage(named: "search"), for: .normal)
        button.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        searchView.addSubview(button)
        txt_search.leftView = searchView
        txt_search.leftViewMode = .always
    }
    

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func searchAction(){
        txt_search.resignFirstResponder()
                if txt_search.text!.isEmpty{
                    self.createAlert(title: "Famous Chair", message: "Please enter search keyword")
                }else{
                    
                    let parameter = ["search_string":txt_search.text ?? ""]
                    InstagramVM.callSearchPeopleApi(params: parameter, viewController: self) { (responseObject) in
                        self.searchData = responseObject
                        if self.searchData?.result.count ?? 0 > 0{
                            self.tableView.isHidden = false
                            self.blankView.isHidden = true
                        }else{
                            self.tableView.isHidden = true
                            self.blankView.isHidden = false
                        }
                        self.tableView.reloadData()
                        
                    }
                }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text!.isEmpty{
            self.createAlert(title: "Famous Chair", message: "Please enter search keyword")
        }else{
            
            let parameter = ["search_string":txt_search.text ?? ""]
            InstagramVM.callSearchPeopleApi(params: parameter, viewController: self) { (responseObject) in
                self.searchData = responseObject
                if self.searchData?.result.count ?? 0 > 0{
                    self.tableView.isHidden = false
                    self.blankView.isHidden = true
                }else{
                    self.tableView.isHidden = true
                    self.blankView.isHidden = false
                }
                self.tableView.reloadData()
                
            }
        }
        return true
        
    }
}

extension InstaSearchViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchData?.result.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchUserCell
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(searchData?.result[indexPath.row].image ?? "")", imageView: cell.userImgView, placeHolder: "userImg")
        cell.lbl_name.text = searchData?.result[indexPath.row].name ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        vc.str_UserId = searchData?.result[indexPath.row].userId ?? ""
        vc.isFromSearch = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
