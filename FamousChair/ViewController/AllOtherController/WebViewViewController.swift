//
//  WebViewViewController.swift
//  FamousChair
//
//  Created by ginger webs on 13/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webViewScreen: WKWebView!
    var urlData = String()
     var type = String()
     var webUrl: URL!
    var staticData : StaticModal?


    override func viewDidLoad() {
        super.viewDidLoad()
        if type == "privacy_policy" {
            titleLabel.text = "Privacy Policy"
        } else {
              titleLabel.text = "Terms of Service"
        }
        
        ServiceVM.callStaticDataApi( params: ["page_type":type], viewController: self) { (responseObject) in
            self.staticData = responseObject
            self.setUIData()
        }
    }
    
    func setUIData(){
//        textView.attributedText = (staticData?.result?[0].content ?? "").htmlToAttributedString
           let fontName =  "Arial"
              let fontSize = 20
              let headerString = "<header><meta name='viewport', content='width=device-width, initial-scale=0.8, maximum-scale=0.8, minimum-scale=0.8, user-scalable=no'></header>"
              let backgroundColor = "transparent"
           let strVal = "<span style=\"background-color:\(backgroundColor); font-family: \(fontName); font-size: \(fontSize)\">\(staticData?.result?[0].content ?? "")</span>"
        
//        let str = (staticData?.result?[0].content ?? "").htmlToAttributedString
//        self.webViewScreen.
           self.webViewScreen.loadHTMLString(headerString + strVal, baseURL: nil)
       }
    

    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
