//
//  ProviderModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 04/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class ProviderModal : NSObject {
    let error : String?
    let message :String?
    var providerData = ProviderData()
    var arr_services = [ServiceData]()
    
   
    init(data:JSON) {
        self.error = data["status"].stringValue
        self.message = data["message"].stringValue
        self.providerData = ProviderData.init(data: data["data"]["provider_data"])
        for item in data["data"]["service_data"].arrayValue{
            let serviceData :ServiceData = ServiceData.init(data: item)
            arr_services.append(serviceData)
            
        }
    }

}

class ProviderData :NSObject {
    var id : Int?
    var name:String?
    var providerImage : String?
    var rating : Double?
    
    init(data:JSON) {
        self.id = data["provider_id"].intValue
        self.name = data["name"].stringValue
        self.providerImage = data["profile_image"].stringValue
        self.rating = data["rating_points"].doubleValue
    }
    override init() {
        
    }
   
}

class ServiceData :NSObject {
    var service_id : Int?
    var service_name:String?
    var service_image : String?
    var price : Double?
    
    init(data:JSON) {
        self.service_id = data["service_id"].intValue
        self.service_name = data["service_name"].stringValue
        self.service_image = data["service_image"].stringValue
        self.price = data["price"].doubleValue
    }
    override init() {
        
    }
   
}
