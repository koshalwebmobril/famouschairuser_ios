//
//  SearchTableViewCell.swift
//  FamousChair
//
//  Created by ginger webs on 07/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import FloatRatingView

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var providerImgview: UIImageView!
    
     
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    

}
