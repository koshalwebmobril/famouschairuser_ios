//
//  InstaCollectionViewCell.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 14/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class InstaCollectionViewCell: UICollectionViewCell {
    //Profile cell
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var liveBtn: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    
    //Main profile cell
    
    @IBOutlet weak var profileImage: UIImageView!
}
