//
//  CustomTabBarController.swift
//  FamousChair
//
//  Created by ginger webs on 10/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    var home: HomeViewController!
    var booking: MyBookingsViewController!
    var notification: NotificationViewController!
    var profile: ProfileViewController!
    var instagram : InstaViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
              self.navigationController?.setNavigationBarHidden(true, animated: true)
           home = HomeViewController()
           booking = MyBookingsViewController()
        notification = NotificationViewController()
        profile = ProfileViewController()
        instagram = InstaViewController()
        UITabBar.appearance().tintColor = UIColor.init(red: 88/255, green: 179/255, blue: 228/255, alpha: 1.0)
        for item in self.tabBar.items! {
            item.image = item.image?.withRenderingMode(.alwaysOriginal)
            
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let index = viewControllers?.index(of: viewController) else {
            return false
        }

        return true
    }
    
    func getCurrentViewController() -> UIViewController {
        var newVC = UIViewController()
        
        if let window = UIApplication.shared.delegate?.window {
            if let viewController = window?.rootViewController {
              
                if(viewController is UINavigationController) {
                    newVC = (viewController as! UINavigationController).visibleViewController!
                }
                else if viewController is UITabBarController {
                    
                    let view = viewController as! UITabBarController
                    
                    newVC = view.selectedViewController!
                    
                    if(newVC is UINavigationController) {
                        newVC = (newVC as! UINavigationController).visibleViewController!
                    }
                }
            }
        }
        return newVC
    }

}
