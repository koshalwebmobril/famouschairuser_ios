
import Foundation
struct ListArray : Codable {
	let id : Int?
	let user_id : Int?
	let user_id_to : Int?
	let notification_type : String?
	let title : String?
	let description : String?
	let read_status : String?
	let status : String?
	let created_at : String?
	let updated_at : String?
	let post_id : Int?
	let user_type : String?
	let name : String?
	let email : String?
	let password : String?
	let address : String?
	let license_no : String?
	let service_type : String?
	let tax_id : String?
	let business_no : String?
	let mobile : String?
	let otp : String?
	let rating : String?
	let profile_image : String?
	let remember_token : String?
	let deleted : String?
	let device_type : String?
	let device_token : String?
	let social_token : String?
	let apple_login_id : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case user_id_to = "user_id_to"
		case notification_type = "notification_type"
		case title = "title"
		case description = "description"
		case read_status = "read_status"
		case status = "status"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case post_id = "post_id"
		case user_type = "user_type"
		case name = "name"
		case email = "email"
		case password = "password"
		case address = "address"
		case license_no = "license_no"
		case service_type = "service_type"
		case tax_id = "tax_id"
		case business_no = "business_no"
		case mobile = "mobile"
		case otp = "otp"
		case rating = "rating"
		case profile_image = "profile_image"
		case remember_token = "remember_token"
		case deleted = "deleted"
		case device_type = "device_type"
		case device_token = "device_token"
		case social_token = "social_token"
		case apple_login_id = "apple_login_id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		user_id_to = try values.decodeIfPresent(Int.self, forKey: .user_id_to)
		notification_type = try values.decodeIfPresent(String.self, forKey: .notification_type)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		read_status = try values.decodeIfPresent(String.self, forKey: .read_status)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
		post_id = try values.decodeIfPresent(Int.self, forKey: .post_id)
		user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		password = try values.decodeIfPresent(String.self, forKey: .password)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		license_no = try values.decodeIfPresent(String.self, forKey: .license_no)
		service_type = try values.decodeIfPresent(String.self, forKey: .service_type)
		tax_id = try values.decodeIfPresent(String.self, forKey: .tax_id)
		business_no = try values.decodeIfPresent(String.self, forKey: .business_no)
		mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
		otp = try values.decodeIfPresent(String.self, forKey: .otp)
		rating = try values.decodeIfPresent(String.self, forKey: .rating)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		remember_token = try values.decodeIfPresent(String.self, forKey: .remember_token)
		deleted = try values.decodeIfPresent(String.self, forKey: .deleted)
		device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
		device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
		social_token = try values.decodeIfPresent(String.self, forKey: .social_token)
		apple_login_id = try values.decodeIfPresent(String.self, forKey: .apple_login_id)
	}

}
