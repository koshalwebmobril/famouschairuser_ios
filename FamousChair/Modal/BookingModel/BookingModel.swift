//
//  BookingModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 18/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class BookingModal : NSObject {
    let error : String?
    let message :String?
    let bookingId : String?
    let bookingData : BookingData?
    let ratingData : RatingData?
    init(data:JSON) {
        self.error = data["status"].stringValue
        self.message = data["message"].stringValue
        self.bookingId = data["data"]["id"].stringValue
        self.bookingData = BookingData.init(item: data["data"])
        self.ratingData = RatingData.init(item: data["review_data"])
        
    }

}

class RatingData: NSObject {
    let comment : String?
    let date : String?
    let rating : Double?
    init(item:JSON) {
    self.comment = item["comment"].stringValue
    self.date = item["created_at"].stringValue
    self.rating = item["rating"].doubleValue
    }
}

class BookingData: NSObject {
    let bookingId : String?
    let serviceId : String?
    let providerId : String?
    let timeSlot : String?
    let bookingDate : String?
    let serviceName: String?
    let price : String?
    let image : String?
    let providerName : String?
    let booking_status:Int?
    init(item:JSON) {
        self.bookingId = item["booking_id"].stringValue
        self.providerId = item["provider_id"].stringValue
        self.serviceId = item["service_id"].stringValue
        self.timeSlot = item["booking_slot"].stringValue
        self.bookingDate = item["booking_date"].stringValue
        self.serviceName = item["service_name"].stringValue
        self.price = item["price"].stringValue
        self.image = item["service_image"].stringValue
        self.providerName = item["provider_name"].stringValue
        self.booking_status = item["booking_status"].intValue
        
    }
    
}
