//
//  StaticModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
struct StaticModal : Codable {
    let error : String?
    let message : String?
    let result : [StaticData]?

    enum CodingKeys: String, CodingKey {

        case error = "status"
        case message = "message"
        case result = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        error = try values.decodeIfPresent(String.self, forKey: .error)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent([StaticData].self, forKey: .result)
    }

}
