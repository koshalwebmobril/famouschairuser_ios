//
//  AfterSplashViewController.swift
//  FamousChair
//
//  Created by ginger webs on 10/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CryptoKit
import CommonCrypto

class AfterSplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

     testApi()
    }
    
     override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(true, animated: true)

     }

    @IBAction func signUpButtonAction(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    @IBAction func signInButtonAction(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    func testApi(){
            
            
            
            let baseURL = "https://api.agora.io/v1/apps/b7a721723b1c4ce88bb0043f20af4fca/cloud_recording/acquire"
        let key = "f8e3afab10ce4ad9ac7b9e0cc0ba24a2"
        let secret = "574259cb95a543829e3c9711fa7cf21a"
        let finalStr = key + ":" + secret
        
//        let dtFormat = DateFormatter()
//        dtFormat.dateFormat = "E, d MMM yyyy HH:mm:ss"
//        let date = dtFormat.string(from: Date())
//        let dateStr = date + " " + "GMT"
//        let username = "info@famouschair.com"
        
        let base64Str = finalStr.toBase64()
//        let hmc = finalStr.hmac(algorithm: .SHA1, key: "\(finalStr)")
//        let Str = "hmac username=\"\(username)\", algorithm=\"hmac-sha1\", headers=\"Date\", signature=\"\(hmc)\""
            
        let header: HTTPHeaders = ["Authorization":"Basic " + base64Str,"Content-Type":"application/json"]
           
            print(header)
            AF.request(baseURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { response in
                
                switch response.result {
                case .success(let value):
                        let swiftyData = JSON(value)
                    print(swiftyData)
                case .failure(let error):
                                    print(error)
                    
                }
            }
        }

}

enum HMACAlgorithm {
    case MD5, SHA1, SHA224, SHA256, SHA384, SHA512

    func toCCHmacAlgorithm() -> CCHmacAlgorithm {
        var result: Int = 0
        switch self {
        case .MD5:
            result = kCCHmacAlgMD5
        case .SHA1:
            result = kCCHmacAlgSHA1
        case .SHA224:
            result = kCCHmacAlgSHA224
        case .SHA256:
            result = kCCHmacAlgSHA256
        case .SHA384:
            result = kCCHmacAlgSHA384
        case .SHA512:
            result = kCCHmacAlgSHA512
        }
        return CCHmacAlgorithm(result)
    }

    func digestLength() -> Int {
        var result: CInt = 0
        switch self {
        case .MD5:
            result = CC_MD5_DIGEST_LENGTH
        case .SHA1:
            result = CC_SHA1_DIGEST_LENGTH
        case .SHA224:
            result = CC_SHA224_DIGEST_LENGTH
        case .SHA256:
            result = CC_SHA256_DIGEST_LENGTH
        case .SHA384:
            result = CC_SHA384_DIGEST_LENGTH
        case .SHA512:
            result = CC_SHA512_DIGEST_LENGTH
        }
        return Int(result)
    }
}



extension String {
    func hmac(algorithm: HMACAlgorithm, key: String) -> String {
       let cKey = key.cString(using: String.Encoding.utf8)
        let cData = self.cString(using: String.Encoding.utf8)
        var result = [CUnsignedChar](repeating: 0, count: Int(algorithm.digestLength()))
        CCHmac(algorithm.toCCHmacAlgorithm(), cKey!, Int(strlen(cKey!)), cData!, Int(strlen(cData!)), &result)
        let hmacData:NSData = NSData(bytes: result, length: (Int(algorithm.digestLength())))
        let hmacBase64 = hmacData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength76Characters)
        return String(hmacBase64)
    }
}
