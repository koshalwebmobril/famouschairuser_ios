//
//  ViewAllPostedController.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 23/11/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import AVKit
class ViewAllPostedController: UIViewController {
    
    @IBOutlet weak var lbl_topline: UILabel!
       @IBOutlet weak var btn_report_height: NSLayoutConstraint!
       @IBOutlet weak var btn_report: UIButton!
       @IBOutlet weak var btn_hidePost: UIButton!
       @IBOutlet weak var txtCharge: UITextField!
       @IBOutlet weak var addLabel: UILabel!
       @IBOutlet weak var priceTxtHieghtConstraint: NSLayoutConstraint!
       @IBOutlet weak var addHeightContraint: NSLayoutConstraint!
       @IBOutlet weak var publicRadio: UIButton!
       @IBOutlet weak var privateRadio: UIButton!
       @IBOutlet weak var goliveView: UIView!
       @IBOutlet weak var optionView: UIView!
       @IBOutlet weak var darkView: UIView!
       @IBOutlet weak var tableView: UITableView!
       private let refreshControl = UIRefreshControl()
       
    var userId = String()
     var postId = Int()
       var paused: Bool = false
       var aboutToBecomeInvisibleCell = -1
       var isPaid = Bool()
       var visibleIP : IndexPath?
       var hideIndexPath :IndexPath?
       var arr_homeData = [InstaHomeData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideDarkView))
        darkView.addGestureRecognizer(tapGesture)
        InstagramVM.getAllPostedDataApi(params: ["user_id":self.userId], viewController: self) { (respounseObject) in
            self.arr_homeData = respounseObject.arr_homeData
            for (index,item) in self.arr_homeData.enumerated(){
                if self.postId == item.id ?? 0{
                    self.arr_homeData.remove(at: index)
                    self.arr_homeData.insert(item, at: 0)
                }
            }
            self.tableView.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name("stopPlayer"), object: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func hideDarkView(){
           darkView.isHidden = true
           optionView.isHidden = true
           goliveView.isHidden = true
       }
       
       
       @objc func commentList(_ sender :UITapGestureRecognizer){
           let position = sender.location(in: tableView)
           let indexPath = tableView.indexPathForRow(at: position)
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "CommentedListVC") as! CommentedListVC
           vc.postId = "\(arr_homeData[indexPath?.row ?? 0].id ?? 0)"
           self.navigationController?.pushViewController(vc, animated: true)
       }
       
       @objc func likeList(_ sender :UITapGestureRecognizer){
           let position = sender.location(in: tableView)
           let indexPath = tableView.indexPathForRow(at: position)
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "LikedUserListVC") as! LikedUserListVC
           vc.postId = "\(arr_homeData[indexPath?.row ?? 0].id ?? 0)"
           self.navigationController?.pushViewController(vc, animated: true)
       }
       
       @objc func sharePost(_ sender :UITapGestureRecognizer){
           let code = "Famous Chair"
           
           let myWebsite = NSURL(string:"https://webmobril.org/dev/famous_chair")
           
           let appURL = "Click on link to download app : \(myWebsite!)"
           
           let shareStr = String(format: "%@ \n\n %@ \n\n",code,appURL)
           let shareAll = [shareStr] as [Any]
           let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
           activityViewController.popoverPresentationController?.sourceView = self.view
           self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func menuButtonAction(_ sender: UIButton) {
        
        darkView.isHidden = false
        optionView.isHidden = false
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        hideIndexPath = indexPath!
        if "\(arr_homeData[indexPath?.row ?? 0].posted_userId ?? 0)" == getStringValue(key: UserDefaultKeys.userId){
            btn_report.isHidden = true
            lbl_topline.isHidden = true
            btn_hidePost.setTitle("Delete Post", for: .normal)
            btn_hidePost.setImage(UIImage(named: "delete_icon"), for: .normal)
            btn_report_height.constant = -10
        }else{
            btn_report.isHidden = false
            lbl_topline.isHidden = false
            btn_hidePost.setTitle("Hide Post", for: .normal)
            btn_hidePost.setImage(UIImage(named: "Hide-post-icon2_100x100"), for: .normal)
            btn_report_height.constant = 50
        }
        
    }
       
    @IBAction func reportPostAction(_ sender: UIButton) {
        InstagramVM.callReportPostApi(params: ["post_id":"\(arr_homeData[hideIndexPath?.row ?? 0].id ?? 0)"], viewController: self, completion: {response in
            if response{
            }
        })
        
    }
    
    @IBAction func likeBtnAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        if arr_homeData[indexPath?.row ?? 0].user_like_status ?? 0 == 0{
            InstagramVM.callLikeApi(params: ["post_id":"\(arr_homeData[indexPath?.row ?? 0].id ?? 0)","like":"1"], viewController: self, completion: {self.arr_homeData[indexPath?.row ?? 0].user_like_status = 1
                self.arr_homeData[indexPath?.row ?? 0].total_like_count = (self.arr_homeData[indexPath?.row ?? 0].total_like_count ?? 0) + 1
                self.tableView.reloadData()
                
            })
        }else{
            InstagramVM.callUnLikeApi(params: ["post_id":"\(arr_homeData[indexPath?.row ?? 0].id ?? 0)"], viewController: self, completion: {self.arr_homeData[indexPath?.row ?? 0].user_like_status = 0
                self.arr_homeData[indexPath?.row ?? 0].total_like_count = (self.arr_homeData[indexPath?.row ?? 0].total_like_count ?? 0) - 1
                self.tableView.reloadData()
                
            })
        }
        
        
    }
    
    @IBAction func commentAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        let cell = sender.superview?.superview as! InstaTableCell
        cell.txtComment.resignFirstResponder()
        if cell.txtComment.text!.isEmpty{
            self.createAlert(title: "Famous Chair", message: "Enter your comment.")
            return
        }else{
            let message = encode(cell.txtComment.text ?? "")
            InstagramVM.callMakeCommentApi(params: ["post_id":"\(arr_homeData[indexPath?.row ?? 0].id ?? 0)","comment":message], viewController: self, completion: {status in
                if status{
                    self.arr_homeData[indexPath?.row ?? 0].comment_count = (self.arr_homeData[indexPath?.row ?? 0].comment_count ?? 0) + 1
                    cell.txtComment.text = ""
                    self.tableView.reloadData()
                }
            })
        }
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        
        let code = "Famous Chair"
        
        let myWebsite = NSURL(string:"https://webmobril.org/dev/famous_chair")
        
        let appURL = "Click on link to download app : \(myWebsite!)"
        
        let shareStr = String(format: "%@ \n\n %@ \n\n",code,appURL)
        let shareAll = [shareStr] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func hidePostAction(_ sender: UIButton) {
        
        if sender.currentTitle ?? "" == "Delete Post"{
            InstagramVM.callDeletePostApi(params: ["post_id":"\(arr_homeData[hideIndexPath?.row ?? 0].id ?? 0)"], viewController: self, completion: {response in
                if response{
                    InstagramVM.callHomeDataApi(params: [String : Any](), viewController: self) { (responseObject) in
                        self.arr_homeData = responseObject.arr_homeData
                        self.hideDarkView()
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    }
                }
            })
        }else{
        
        InstagramVM.callHidePostApi(params: ["post_id":"\(arr_homeData[hideIndexPath?.row ?? 0].id ?? 0)"], viewController: self, completion: {response in
            if response{
                InstagramVM.callHomeDataApi(params: [String : Any](), viewController: self) { (responseObject) in
                    self.arr_homeData = responseObject.arr_homeData
                    self.hideDarkView()
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }
            }
        })
        }
    }
    
    func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    

}




extension ViewAllPostedController:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count ?? 0 == 200{
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_homeData.count 
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InstaTableCell", for: indexPath) as! InstaTableCell
        let tapCommentGesture = UITapGestureRecognizer(target: self, action: #selector(commentList(_:)))
        cell.commentLabel.addGestureRecognizer(tapCommentGesture)
        let tapLikeGesture = UITapGestureRecognizer(target: self, action: #selector(likeList(_:)))
        cell.likeLabel.addGestureRecognizer(tapLikeGesture)
        let tapShareGesture = UITapGestureRecognizer(target: self, action: #selector(sharePost(_:)))
        cell.likeLabel.addGestureRecognizer(tapShareGesture)
        cell.shareLabel.addGestureRecognizer(tapShareGesture)
        
        
        cell.commentLabel.text =  "\(arr_homeData[indexPath.row].comment_count ?? 0) Comment"
        cell.shareLabel.text =  "\(arr_homeData[indexPath.row].share_count ?? 0) Share"
        cell.likeLabel.text = "\(arr_homeData[indexPath.row].total_like_count ?? 0) Like"
        
        cell.lbl_post_title.text = decode(arr_homeData[indexPath.row].post_title ?? "")
        
        if arr_homeData[indexPath.row].user_like_status ?? 0 == 0{
            cell.likeBtn.setImage(UIImage(named: "Like-icon_50x50"), for: .normal)
            
        }else{
            cell.likeBtn.setImage(UIImage(named: "like_icon"), for: .normal)
        }
        
        if arr_homeData[indexPath.row].thumbnail ?? "" == ""{
            cell.videoPlayerSuperView.isHidden = true
            cell.postImage.isHidden = false
            ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(arr_homeData[indexPath.row].post_path ?? "")"  , imageView: cell.postImage, placeHolder: "")
        }else{
            cell.videoPlayerSuperView.isHidden = false
            let videoUrl = arr_homeData[indexPath.row].post_path  ?? ""
            let videoUrlValue = WebServiceApi.baseImage_url + videoUrl
            cell.avPlayer = AVPlayer(url: URL(string: videoUrlValue)!)
            cell.avPlayerLayer = AVPlayerLayer(player: cell.avPlayer)
            cell.avPlayerLayer?.videoGravity = AVLayerVideoGravity(rawValue: AVLayerVideoGravity.resizeAspectFill.rawValue)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.playerItemDidReachEnd(notification:)),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                   object: cell.avPlayer?.currentItem)
            cell.avPlayer?.volume = 3
            //            cell.avPlayer?.pause()
            cell.avPlayer?.actionAtItemEnd = .none
            cell.avPlayerLayer?.frame = cell.videoPlayerSuperView.bounds
            cell.videoPlayerSuperView.layer.addSublayer((cell.avPlayerLayer)!)
        }
        
        cell.userName.text = arr_homeData[indexPath.row].name ?? ""
        cell.txtComment.delegate = self
       
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(arr_homeData[indexPath.row].profile_image ?? "")"  , imageView: cell.profileImage, placeHolder: "userImg")
        
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(getStringValue(key: UserDefaultKeys.userImage))"  , imageView: cell.userImage, placeHolder: "userImg")
        
        return cell
    }
    
    @objc func playerItemDidReachEnd(notification: Notification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: CMTime.zero)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let indexPaths = self.tableView.indexPathsForVisibleRows
        var cells = [Any]()
        for ip in indexPaths!{
            if let videoCell = self.tableView.cellForRow(at: ip) as? InstaTableCell{
                cells.append(videoCell)
            }
        }
        let cellCount = cells.count
        if cellCount == 0 {return}
        if cellCount == 1{
            print ("visible = \(indexPaths?[0])")
            if visibleIP != indexPaths?[0]{
                visibleIP = indexPaths?[0]
            }
            if let videoCell = cells.last! as? InstaTableCell{
                self.playVideoOnTheCell(cell: videoCell, indexPath: (indexPaths?.last)!)
            }
        }
        if cellCount >= 2 {
            for i in 0..<cellCount{
                let cellRect = self.tableView.rectForRow(at: (indexPaths?[i])!)
                let completelyVisible = self.tableView.bounds.contains(cellRect)
                let intersect = cellRect.intersection(self.tableView.bounds)
                
                let currentHeight = intersect.height
                print("\n \(currentHeight)")
                let cellHeight = (cells[i] as AnyObject).frame.size.height
                if currentHeight > (cellHeight * 0.95){
                    if visibleIP != indexPaths?[i]{
                        visibleIP = indexPaths?[i]
                        print ("visible = \(indexPaths?[i])")
                        if let videoCell = cells[i] as? InstaTableCell{
                            self.playVideoOnTheCell(cell: videoCell, indexPath: (indexPaths?[i])!)
                        }
                    }
                }
                else{
                    if aboutToBecomeInvisibleCell != indexPaths?[i].row{
                        aboutToBecomeInvisibleCell = (indexPaths?[i].row)!
                        if let videoCell = cells[i] as? InstaTableCell{
                            self.stopPlayBack(cell: videoCell, indexPath: (indexPaths?[i])!)
                        }
                        
                    }
                }
            }
        }
    }
    
    func checkVisibilityOfCell(cell : InstaTableCell, indexPath : IndexPath){
        let cellRect = self.tableView.rectForRow(at: indexPath)
        let completelyVisible = self.tableView.bounds.contains(cellRect)
        if completelyVisible {
            self.playVideoOnTheCell(cell: cell, indexPath: indexPath)
        }
        else{
            if aboutToBecomeInvisibleCell != indexPath.row{
                aboutToBecomeInvisibleCell = indexPath.row
                self.stopPlayBack(cell: cell, indexPath: indexPath)
            }
        }
    }
    
    func playVideoOnTheCell(cell : InstaTableCell, indexPath : IndexPath){
        cell.avPlayer?.play()
        //        cell.startPlayback()
        //        if isPaid == true {
        //            if arr_data?.arr_homeData[indexPath.row].thumbnail != "" {
        //                cell.startPlayback()
        //            } else {
        //                cell.stopPlayback()
        //            }
        //        }
    }
    
    func stopPlayBack(cell : InstaTableCell, indexPath : IndexPath){
        cell.avPlayer?.pause()
        //        cell.stopPlayback()
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("end = \(indexPath)")
        if let videoCell = cell as? InstaTableCell{
            //            videoCell.stopPlayback()
            videoCell.avPlayer?.pause()
        }
        
        paused = true
    }
    
   
}

