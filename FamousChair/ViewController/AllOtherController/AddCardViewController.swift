//
//  AddCardViewController.swift
//  FamousChair
//
//  Created by ginger webs on 11/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class AddCardViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
