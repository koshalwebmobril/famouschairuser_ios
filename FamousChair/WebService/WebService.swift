
//
//  WebService.swift

import Foundation
import UIKit
import Alamofire
import NVActivityIndicatorView
import AVKit
import SDWebImage
import AVFoundation

typealias completion = ((Data?)->())

class ApiManager: NSObject {
    
    static let shared = ApiManager()
    
    func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func headerParam() -> HTTPHeaders {
        var headerParam = HTTPHeaders()
        headerParam["Content-Type"] = "application/json"
        headerParam["appVersion"] = getStringValue(key: UserDefaultKeys.appVersion)
        
        if getStringValue(key: UserDefaultKeys.loginToken).count > 0 {
            headerParam["authorization"] = "Bearer \(getStringValue(key: UserDefaultKeys.loginToken))"
        }
        
        return headerParam
    }
    
    //1)--MARK:-- post Api ---------
    func postApiRequest(url:String, dict: [String : Any], runLoader : Bool? = true,viewController:UIViewController, completion:@escaping completion) {
        if isConnectedToInternet() {
            
            LoaderClass.startLoader()
            
            print("param is --->>> \(dict)")
            
            let urlString = "\(BASEURL)\(url)"
            
            print("url is --->>> \(urlString)")
            
            
            AF.request(urlString, method: .post, parameters: dict, encoding: JSONEncoding.default, headers: nil) .responseJSON { responseObject in
                switch(responseObject.result) {
                case .success(_):
                    LoaderClass.stopLoader()
                    print(responseObject.value!)
                    let statusCode = responseObject.response?.statusCode
                    
                    if statusCode == 200 {
                        let result = responseObject.value! as! [String : Any]
                        completion(responseObject.data)
                    } else {
                        let result = responseObject.value! as! [String : Any]
                        self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: result["message"] as! String)
                    }
                    break
                case .failure(_):
                    LoaderClass.stopLoader()
                    viewController.createAlert(title: "Famous Chair", message: responseObject.error?.localizedDescription ?? "Something went wrong")
                    
                    break
                }
            }
        } else {
            viewController.createAlert(title: "Famous Chair", message: "No Internet")

        }
    }
    
    //1)--MARK:-- post header Api ---------
    func postHeaderApiRequest(url:String, dict: [String : Any], runLoader : Bool? = true,viewController:UIViewController, completion:@escaping completion) {
        if isConnectedToInternet() {
            
            LoaderClass.startLoader()
            
            print("param is --->>> \(dict)")
            
            let urlString = "\(BASEURL)\(url)"
            
            print("url is --->>> \(urlString)")
            print("header is --->>> \(headerParam())")
            
            AF.request(urlString, method: .post, parameters: dict, encoding: JSONEncoding.default, headers: headerParam()) .responseJSON { responseObject in
                switch(responseObject.result) {
                case .success(_):
                    LoaderClass.stopLoader()
                    print(responseObject.value!)
                    let statusCode = responseObject.response?.statusCode
                    
                    if statusCode == 200 {
                        let result = responseObject.value! as! [String : Any]
                        completion(responseObject.data)
                    } else {
                        let result = responseObject.value! as! [String : Any]
                       self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: result["message"] as! String)
                    }
                    break
                case .failure(_):
                    LoaderClass.stopLoader()
                    viewController.createAlert(title: "Famous Chair", message: responseObject.error?.localizedDescription ?? "Something went wrong")
                    
                    break
                }
            }
        } else {
            viewController.createAlert(title: "Famous Chair", message: "No Internet")

        }
    }
    
    //1)--MARK:-- post Api ---------
    func putApiRequest(url:String, dict: [String : Any], runLoader : Bool? = true,viewController:UIViewController, completion:@escaping completion) {
        if isConnectedToInternet() {
            
            LoaderClass.startLoader()
            
            print("param is --->>> \(dict)")
            
            let urlString = "\(BASEURL)\(url)"
            
            print("url is --->>> \(urlString)")
            print("header is --->>> \(headerParam())")
            
            AF.request(urlString, method: .put, parameters: dict, encoding: JSONEncoding.default, headers: headerParam()) .responseJSON { responseObject in
                switch(responseObject.result) {
                    
                case .success(_):
                    
                    LoaderClass.stopLoader()
                    
                    print(responseObject.value!)
                    let statusCode = responseObject.response?.statusCode
                    
                    if statusCode == 200 {
                        let result = responseObject.value! as! [String : Any]
                        if let status = result["success"] as? Bool, status == true {
                            completion(responseObject.data)
                        } else {
                            let result = responseObject.value! as! [String : Any]
                            if let message = result["message"] as? String {
                                self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: result["message"] as! String)
//                                self.errorHandling(errorCode: statusCode!, errorMessage: message)
                            } else {
                                self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: "Something went wrong")
                                
                            }
                        }
                    } else {
                        let result = responseObject.value! as! [String : Any]
                        if let message = result["message"] as? String {
                            self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: message)
                        } else {
                            self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: "Something went wrong")
                        }
                    }
                    
                    break
                case .failure(_):
                    LoaderClass.stopLoader()
                    viewController.createAlert(title: "Famous Chair", message: responseObject.error?.localizedDescription ?? "Something went wrong")
                    
                    break
                }
            }
        } else {
            viewController.createAlert(title: "Famous Chair", message: "No Internet")

        }
    }
    
    func getRequestApi(url:String,dict: [String : Any], runLoader : Bool? = true,viewController:UIViewController, completion:@escaping completion) {
        if isConnectedToInternet() {
            
            LoaderClass.startLoader()
            let urlString = "\(BASEURL)\(url)"
            print("url is --->>> \(urlString)")
            
            
            AF.request(urlString, method: .get, encoding: JSONEncoding.default, headers: nil) .responseJSON { responseObject in
                switch(responseObject.result) {
                case .success(_):
                    LoaderClass.stopLoader()
                    print(responseObject.value!)
                    let statusCode = responseObject.response?.statusCode
                    
                    if statusCode == 200 {
                        let result = responseObject.value! as! [String : Any]
                        if let status = result["success"] as? Bool, status == true {
                            completion(responseObject.data)
                        } else {
                            let result = responseObject.value! as! [String : Any]
                            if let message = result["message"] as? String {
                                self.errorHandling(errorCode: statusCode!, viewController: viewController,errorMessage: message)
                            } else {
                                self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: "Something went wrong")
                            }
                        }
                    } else {
                        let result = responseObject.value! as! [String : Any]
                        if let message = result["message"] as? String {
                            self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: message)
                        } else {
                            self.errorHandling(errorCode: statusCode!, viewController: viewController,errorMessage: "Something went wrong")
                        }
                    }
                    
                    break
                case .failure(_):
                    LoaderClass.stopLoader()
                    viewController.createAlert(title: "Famous Chair", message: responseObject.error?.localizedDescription ?? "Something went wrong")
                    
                    break
                }
            }
        } else {
            viewController.createAlert(title: "Famous Chair", message: "No Internet")

        }
    }
    
    
    func getHeaderRequestApi(url:String,dict: [String : Any], runLoader : Bool? = true,viewController:UIViewController, completion:@escaping completion) {
        if isConnectedToInternet() {
            
            LoaderClass.startLoader()
            let urlString = "\(BASEURL)\(url)"
            print("url is --->>> \(urlString)")
             print("param is --->>> \(dict)")
            print("header is --->>> \(headerParam())")
            
            AF.request(urlString, method: .get, encoding: JSONEncoding.default, headers: headerParam()) .responseJSON { responseObject in
                switch(responseObject.result) {
                case .success(_):
                    LoaderClass.stopLoader()
                    print(responseObject.value!)
                    let statusCode = responseObject.response?.statusCode
                    
                    if statusCode == 200 {
                        let result = responseObject.value! as! [String : Any]
                        if let status = result["status"] as? String, status == "success" {
                            completion(responseObject.data)
                        } else {
                            let result = responseObject.value! as! [String : Any]
                            if let message = result["message"] as? String {
                                self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: message)
                            } else {
                                self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: "Something went wrong")
                            }
                        }
                    } else {
                        let result = responseObject.value! as! [String : Any]
                        if let message = result["message"] as? String {
                            self.errorHandling(errorCode: statusCode!, viewController: viewController,errorMessage: message)
                        } else {
                            self.errorHandling(errorCode: statusCode!, viewController: viewController,errorMessage: "Something went wrong")
                        }
                    }
                    
                    break
                case .failure(_):
                    LoaderClass.stopLoader()
                    viewController.createAlert(title: "Famous Chair", message: responseObject.error?.localizedDescription ?? "Something went wrong")
                    
                    break
                }
            }
        } else {
            viewController.createAlert(title: "Famous Chair", message: "No Internet")

        }
    }
    
   
    
    func uploadImageApi(urlmethod: String, parameter: [String: Any], userImage: Data!,imageName: String,viewController:UIViewController,completion:@escaping completion) {
        
        if isConnectedToInternet() {
            
            LoaderClass.startLoader()
            
            print("param ....... \(parameter)")
            
            print(urlmethod)
            
            AF.upload(multipartFormData: { multipartFormData in
                
                for (key, value) in parameter {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                let imageData = userImage
                
                if let data = imageData {
                    multipartFormData.append(data, withName: imageName, fileName: "imageName" + ".jpeg", mimeType: "image/jpeg")
                }
                
            }, to: urlmethod, method: .post, headers: headerParam()).responseJSON { responseObject in
                LoaderClass.stopLoader()
                switch(responseObject.result) {
                case .success(_):
                    print(responseObject.value!)
                    let statusCode = responseObject.response?.statusCode
                    if statusCode == 200 {
                        let result = responseObject.value! as! [String : Any]
                        completion(responseObject.data)
                    } else {
                        let result = responseObject.value! as! [String : Any]
                        self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: result["message"] as! String)
                    }
                    break
                case .failure(_):
                    LoaderClass.stopLoader()
                    viewController.createAlert(title: "Famous Chair", message: responseObject.error?.localizedDescription ?? "Something went wrong")
                    
                    break
                }
            }
        } else {
            viewController.createAlert(title: "Famous Chair", message: "No Internet")
            
        }
    }
    
    
    func uploadVideoAndImageApi(urlmethod: String, parameter: [String: Any], post_path: Data!,imageName: String,thumbnail:String,thumbnail_data:Data! ,viewController:UIViewController,completion:@escaping completion) {
        
        if isConnectedToInternet() {
            
            LoaderClass.startLoader()
            
            print("param ....... \(parameter)")
            
            print(urlmethod)
            
            AF.upload(multipartFormData: { multipartFormData in
                
                for (key, value) in parameter {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                let imageData = post_path
                let thumbnaialData = thumbnail_data
                
                if thumbnail == ""{
                    if let data = imageData {
                        multipartFormData.append(data, withName: imageName, fileName: "post_path" + ".jpeg", mimeType: "image/jpeg")
                    }
                    if let thumbData = thumbnaialData {
                        multipartFormData.append(thumbData, withName: "thumbnail", fileName: "", mimeType: "image/jpeg")
                    }
                }else{
                    if let data = imageData {
                        multipartFormData.append(data, withName: imageName, fileName: "post_path" + ".mp4", mimeType: "video/mp4")
                    }
                    if let thumbData = thumbnaialData {
                        multipartFormData.append(thumbData, withName: "thumbnail", fileName: "thumbnail" + ".jpeg", mimeType: "image/jpeg")
                    }
                    
                }
                
                
                
            }, to: urlmethod, method: .post, headers: headerParam()).responseJSON { responseObject in
                
                LoaderClass.stopLoader()
                switch(responseObject.result) {
                case .success(_):
                    print(responseObject.value!)
                    let statusCode = responseObject.response?.statusCode
                    if statusCode == 200 {
                        let result = responseObject.value! as! [String : Any]
                        completion(responseObject.data)
                    } else {
                        let result = responseObject.value! as! [String : Any]
                        self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: result["message"] as! String)
                    }
                    break
                case .failure(_):
                    LoaderClass.stopLoader()
                    viewController.createAlert(title: "Famous Chair", message: responseObject.error?.localizedDescription ?? "Something went wrong")
                    
                    break
                }
            }
        } else {
            viewController.createAlert(title: "Famous Chair", message: "No Internet")
            
        }
    }
    
    
    
    func getRequestGoliveApi(url:String,dict: [String : Any], runLoader : Bool? = true,viewController:UIViewController, completion:@escaping completion) {
        if isConnectedToInternet() {
            
              LoaderClass.startLoader()
            let urlString = "\(BASEURL)\(url)"
            print("url is --->>> \(urlString)")
            print("header is --->>> \(headerParam())")
            
            AF.request(urlString, method: .get, encoding: JSONEncoding.default, headers: headerParam()) .responseJSON { responseObject in
                switch(responseObject.result) {
                case .success(_):
                   LoaderClass.stopLoader()
                    print(responseObject.value!)
                    let statusCode = responseObject.response?.statusCode

                     if statusCode == 200 {
                        let result = responseObject.value! as! [String : Any]
                            completion(responseObject.data)
                    } else {
                        let result = responseObject.value! as! [String : Any]
                           self.errorHandling(errorCode: statusCode!,viewController: viewController, errorMessage: result["message"] as! String)
                        }
                    break
                case .failure(_):
                     LoaderClass.stopLoader()
                     viewController.createAlert(title: "Famous Chair", message: responseObject.error?.localizedDescription ?? "Something went wrong")
                    break
                }
            }
        } else {
           viewController.createAlert(title: "Famous Chair", message: "No Internet")
        }
    }
    
   
    
    func errorHandling(errorCode: Int,viewController:UIViewController, errorMessage:String) {
        if errorCode == 401 {
            viewController.createAlert(title: "Famous Chair", message: errorMessage)
            
        } else {
            viewController.createAlert(title: "Famous Chair", message: errorMessage)
            
        }
    }
    
    /**
    @description: Method used for generate thumbnail image from video url
    @parameters: video url
    @returns: UIimage
    */
    
   class func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
    
    /**
     @description: Method used for download image from server
     @parameters:  imageUrl,imageView
     @returns:Nil
     */
    
    class func downloadProfileImage(url:String,imageView:UIImageView,placeHolder:String) {
        
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: placeHolder))
        imageView.clipsToBounds = true
    }
}
