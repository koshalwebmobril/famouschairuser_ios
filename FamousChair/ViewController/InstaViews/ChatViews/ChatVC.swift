//
//  ChatVC.swift
//  TaxiIn
//
//  Created by WebMobril on 20/05/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SDWebImage
import AVKit
import Photos
import ImglyKit
import PhotoEditorSDK


class ChatVC: UIViewController,FirebaseContactManagerDelegate,  UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    
    var driverId = Int()
    var userId = Int()
//    var selectedItems = [YPMediaItem]()
    var messages:[Message] = []
    var contact :[Contacts] = []
    var friend: ChatFriend?
    var msgType = ""
    var lastMsg = ""
    var message = ""
    var userName = String()
    var imageURL = ""
    var videoURL = ""
    var thumbnailImage = UIImage()
    var userProfilePic = String()
    //   var imagePicker: ImagePicker!
    var selectImg = UIImage()
    var channelRef: DatabaseReference = Database.database().reference()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textFieldBgView: UIView!
    @IBOutlet weak var messagetextField: UITextField!
    @IBOutlet weak var sendMsgButton: UIButton!
    @IBOutlet weak var lbl_Chat: UILabel!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var BgtextfieldViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bottomSendViewConstraint: NSLayoutConstraint!
    // var ref: DatabaseReference!
    var driver_DeviceToken = String()
    let imagePicker = UIImagePickerController()
    var postImage = String()
    var postUIIage = UIImage()
    var isComingFromShare = String()
    var checkType = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        setStatusBarColor()
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .chatRefresh, object: nil)
        
        registerNibs()
        setString()
        
        imagePicker.delegate = self
        userNameLabel.text = userName
        userId = Int(getStringValue(key: UserDefaultKeys.userId)) ?? 0
        setProfileImage(userImage: profileImage, urlString: userProfilePic)
        
        FireBaseManager.defaultManager.messageManagerDelegate = self
        FireBaseManager.defaultManager.messages = []
        FireBaseManager.defaultManager.fetchMessages(node: getMsgNode())   //"167-323"
        
        FireBaseManager.defaultManager.contactManagerDelegate = self
        FireBaseManager.defaultManager.contacts = []
        FireBaseManager.defaultManager.fetchContacts()  /* for  for getting userDetails */
        
    }
    
    
    
    
    func setProfileImage(userImage: UIImageView, urlString: String) {
        if urlString != "" {
            let userProfileImage = WebServiceApi.baseImage_url + urlString
            userImage.sd_setImage(with: URL(string: userProfileImage), placeholderImage:UIImage(named: "userImg"), options: .highPriority, completed: { (image, error, cache, url) in
                userImage.image = image
            })
        } else {
            userImage.image = UIImage(named: "userImg")
        }
    }
    
    @IBAction func audioButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func videoButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func uploadImage(_ sender: UIButton){
        let configuration = Configuration { builder in
            // Setup global colors
            builder.theme.backgroundColor = .white
            builder.theme.menuBackgroundColor = UIColor.lightGray
            self.customizeCameraController(builder)
            self.customizePhotoEditorViewController(builder)
        }
        
        let cameraViewController = CameraViewController(configuration: configuration)
        cameraViewController.modalPresentationStyle = .fullScreen
        cameraViewController.locationAccessRequestClosure = { locationManager in
            locationManager.requestWhenInUseAuthorization()
        }
        
        // Set a global tint color, that gets inherited by all views
        if let window = UIApplication.shared.delegate?.window! {
            window.tintColor = .baseColor
        }
        cameraViewController.cancelBlock = {
            self.dismiss(animated: true, completion: nil)
        }
        cameraViewController.completionBlock = { [unowned cameraViewController] image, url in
            if let image = image {
                let photo = Photo(image: image)
                let photoEditModel = cameraViewController.photoEditModel
                cameraViewController.present(self.createCustomizedPhotoEditViewController(with: photo, configuration: configuration, and: photoEditModel), animated: true, completion: nil)
            }
            if let data = url{
                let photoEditModel = cameraViewController.photoEditModel
                let videoEditor = VideoEditViewController(videoAsset: Video(url: data), configuration: configuration, photoEditModel: photoEditModel)
                videoEditor.modalPresentationStyle = .fullScreen
                videoEditor.delegate = self
                cameraViewController.present(videoEditor, animated: true, completion: nil)
            }
        }
        
        cameraViewController.dataCompletionBlock = { [unowned cameraViewController] data in
            if let data = data {
                let photo = Photo(data: data)
                let photoEditModel = cameraViewController.photoEditModel
                cameraViewController.present(self.createCustomizedPhotoEditViewController(with: photo, configuration: configuration, and: photoEditModel), animated: true, completion: nil)
            }
        }
        
        present(cameraViewController, animated: true, completion: nil)
    }
    fileprivate func customizeCameraController(_ builder: ConfigurationBuilder) {
        builder.configureCameraViewController { options in
            // Enable/Disable some features
            options.cropToSquare = false
            options.showFilterIntensitySlider = false
            options.tapToFocusEnabled = true
            options.showCancelButton = true
            //        options.allowedRecordingModes = [.photo,.video]
            
            // Use closures to customize the different view elements
            options.cameraRollButtonConfigurationClosure = { button in
                button.layer.borderWidth = 2.0
                button.layer.borderColor = UIColor.baseColor.cgColor
            }
            
            options.timeLabelConfigurationClosure = { label in
                label.textColor = .baseColor
            }
            
            options.recordingModeButtonConfigurationClosure = { button, _ in
                button.setTitleColor(UIColor.gray, for: .normal)
                button.setTitleColor(.baseColor, for: .selected)
            }
            
        }
    }
    
    fileprivate func customizePhotoEditorViewController(_ builder: ConfigurationBuilder) {
        // Customize the main editor
        builder.configurePhotoEditViewController { options in
            options.titleViewConfigurationClosure = { titleView in
                if let titleLabel = titleView as? UILabel {
                    titleLabel.text = "Selfie-Editor"
                }
            }
            
            options.actionButtonConfigurationClosure = { cell, _ in
                cell.contentTintColor = UIColor.baseColor
            }
        }
    }
    private func createCustomizedPhotoEditViewController(with photo: Photo, configuration: Configuration, and photoEditModel: PhotoEditModel) -> PhotoEditViewController {
        let photoEditViewController = PhotoEditViewController(photoAsset: photo, configuration: configuration, photoEditModel: photoEditModel)
        photoEditViewController.modalPresentationStyle = .fullScreen
        photoEditViewController.view.tintColor = UIColor(red: 0.11, green: 0.44, blue: 1.00, alpha: 1.00)
        photoEditViewController.toolbar.backgroundColor = UIColor.gray
        photoEditViewController.delegate = self
        
        return photoEditViewController
    }
    
    fileprivate func thumbnailImageForFileUrl(_ fileUrl: URL) -> UIImage? {
        let asset = AVAsset(url: fileUrl)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        imageGenerator.maximumSize = CGSize(width: 200, height: 200)
        
        do {
            
            let thumbnailCGImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60), actualTime: nil)
            return UIImage(cgImage: thumbnailCGImage)
            
        } catch let err {
            print(err)
        }
        
        return nil
    }
    
    //MARK:- Set Message Data.
    func setMsgData(){
        
        let createdAt = NSDate().timeIntervalSince1970
        let nodeStr = getMsgNode()
        
        FireBaseManager.defaultManager.getFriendUreadCount(node: nodeStr, friendId: "\(driverId)") { (unreadCount) in
            let messageItem: NSMutableDictionary = [
                
                "fileType": "",
                "fileUrl":"",
                "videoUrl":self.videoURL,
                "videoDuration":0,
                "forUid_status":"\(self.driverId)_unread",
                "time":"\(NSInteger(createdAt))",
                "picture":self.imageURL,
                "from":getStringValue(key: UserDefaultKeys.userId),
                "message": self.message,
                "type":self.msgType,
                "fcm_token": getStringValue(key: UserDefaultKeys.fcmToken) ,
                "device_token":self.driver_DeviceToken,
                "reciever_id":"\(self.driverId)"
                
            ]
            
            let messageRecent: NSMutableDictionary = [
                
                "\(self.driverId)_unread":unreadCount+1,
                "from":getStringValue(key: UserDefaultKeys.userId),
                "sender_image":getStringValue(key: UserDefaultKeys.userImage),
                "sender_name":getStringValue(key: UserDefaultKeys.name),
                "message":self.lastMsg,
                "videoUrl":self.videoURL,
                "picture":self.imageURL,
                "my_id":getStringValue(key: UserDefaultKeys.userId),
                "type":self.msgType,
                "time":"\(NSInteger(createdAt))",
                "reciever_id":"\(self.driverId)" ,
                "reciever_image":self.userProfilePic ,
                "reciever_name":self.userName ,
                "fcm_token": getStringValue(key: UserDefaultKeys.fcmToken) ,
                "device_token":self.driver_DeviceToken
            ]
            
            FireBaseManager.defaultManager.sendNewMsg(atNode: nodeStr, detailDict: messageItem as NSMutableDictionary, recentDict: messageRecent as NSMutableDictionary)
            
            let dict = ["title": getStringValue(key: UserDefaultKeys.name),
                        "message": self.lastMsg,
                        "username": getStringValue(key: UserDefaultKeys.name),
                        "uid": "\(getStringValue(key: UserDefaultKeys.userId))",
                "image":getStringValue(key: UserDefaultKeys.userImage),
                "type": "chat",
                "fcm_token": getStringValue(key: UserDefaultKeys.fcmToken) ,
                "device_token":self.driver_DeviceToken
                ] as [String : Any]
            
            let notsender = PushNotificationSender()
            notsender.sendPushNotification(to: self.driver_DeviceToken, title: getStringValue(key: UserDefaultKeys.name), body: self.lastMsg, dict: dict)
            
            
            self.channelRef.child("messages").child(nodeStr).queryOrdered(byChild: "from").queryEqual(toValue: "\(getStringValue(key: UserDefaultKeys.userId))").observe(.childChanged, with: { (snapshot) -> Void in
                
                DispatchQueue.main.async {
                    let msgData = snapshot.value as! Dictionary<String, AnyObject>
                    let key = snapshot.key
                    let reqIndex = self.messages.firstIndex { (message) -> Bool in
                        return message.node == key
                    }
                    guard let index = reqIndex else {return}
                    self.messages[index].readStatus = msgData["forUid_status"] as? String ?? ""
                    UIView.performWithoutAnimation {
                        let indexPath = IndexPath(row: reqIndex!, section: 0)
                        self.tableView.reloadRows(at: [indexPath], with: .none)
                    }
                    
                }
            })
        }
    }
    
    
    func didSelect(image: UIImage?) {
        LoaderClass.startLoader()
        let imageName = NSUUID().uuidString
        let nodeStr = getMsgNode()
        //  let sender = UserDefaults.standard.object(forKey: "user_id") as? Int ?? 0//user_Name
        let senderImg = ""
        
        var fullName = ""
        if let firstName = UserDefaults.standard.object(forKey: "name") as? String{
            fullName = "\(firstName)"
        }
        
        let sender = getStringValue(key: UserDefaultKeys.userId)
        let uSerId = driverId
        
        let riversRef = FireBaseManager.defaultManager.msgRef.child(nodeStr).childByAutoId()
        FireBaseManager.defaultManager.uploadImageOnFirebase(node: nodeStr, image: image!) { (Image) in
            print(Image)
            LoaderClass.stopLoader()
            let createdAt = NSDate().timeIntervalSince1970
            let timestamp  = "\(NSInteger(createdAt))"
            let messageItem: NSMutableDictionary = [
                "forUid_status":"\(uSerId)_unread",
                "time": timestamp,
                "from":"\(sender)",
                "sender_name":getStringValue(key: UserDefaultKeys.name),
                "sender_image": getStringValue(key: UserDefaultKeys.userImage),
                "reciever_name":self.userName,
                "reciever_image": self.userProfilePic,
                "fcm_token": getStringValue(key: UserDefaultKeys.fcmToken) ,
                "device_token":self.driver_DeviceToken,
                "picture": Image,
                "type": "Image",
                "message": "Image",
                "reciever_id":"\(uSerId)",
                "message_id": "\(riversRef.key ?? "")"
            ]
            FireBaseManager.defaultManager.sendNewMsg(atNode: nodeStr, detailDict: messageItem as NSMutableDictionary, recentDict: messageItem as NSMutableDictionary)
            let dict = ["title": getStringValue(key: UserDefaultKeys.name),
                        "message": self.lastMsg,
                        "username": getStringValue(key: UserDefaultKeys.name),
                        "uid": "\(getStringValue(key: UserDefaultKeys.userId))",
                "image":getStringValue(key: UserDefaultKeys.userImage),
                "type": "chat",
                "fcm_token": getStringValue(key: UserDefaultKeys.fcmToken) ,
                "device_token":self.driver_DeviceToken
                ] as [String : Any]
            
            let notsender = PushNotificationSender()
            notsender.sendPushNotification(to: self.driver_DeviceToken, title: getStringValue(key: UserDefaultKeys.name), body: self.lastMsg, dict: dict)
        }
    }
    
    
    func setString() {
        messagetextField.placeholder = "Enter message"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setUI()
    }
    
    @objc func handleKeyboard(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyBoardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                let isKeyboardShowing = notification.name == UIWindow.keyboardWillShowNotification
                self.BgtextfieldViewBottomConstraint.constant = isKeyboardShowing ? keyBoardHeight  - 80 : 10
                // bottomSendViewConstraint.constant = 10
                tableView.contentInset = isKeyboardShowing ? UIEdgeInsets(top: 0, left: 0, bottom: keyBoardHeight, right: 0) : UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                self.scrollAtBottom()
                UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                    self.view.layoutIfNeeded()
                }) { (completed) in
                    
                }
            }
        }
    }
    
    func setUI(){
        tableView.setNeedsUpdateConstraints()
        tableView.layoutIfNeeded()
        
        sendMsgButton.layer.cornerRadius = 5.0
        sendMsgButton.layer.masksToBounds = true
        
        messagetextField.setLeftPaddingPoints(15)
        messagetextField.setRightPaddingPoints(50)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        updateMsgStatus()
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //        IQKeyboardManager.shared.enable = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .chatRefresh, object: nil)
        
        
        
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        registerNibs()
        setString()
        imagePicker.delegate = self
        userNameLabel.text = userName
        userId = Int(getStringValue(key: UserDefaultKeys.userId)) ?? 0
        
        setProfileImage(userImage: profileImage, urlString: userProfilePic)
        
        FireBaseManager.defaultManager.messageManagerDelegate = self
        FireBaseManager.defaultManager.messages = []
        FireBaseManager.defaultManager.fetchMessages(node: getMsgNode())   //"167-323"
        
        FireBaseManager.defaultManager.contactManagerDelegate = self
        FireBaseManager.defaultManager.contacts = []
        FireBaseManager.defaultManager.fetchContacts()  /* for  for getting userDetails */
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    @IBAction func tappedMessageSendBtn(_ sender: Any) {
        guard let message = self.messagetextField.text else { return }
        if !message.trimmingCharacters(in: .whitespaces).isEmpty{
            //Text Message
            self.msgType = "Text"
            self.lastMsg = message
            self.message = message
            self.sendMessage()
            self.messagetextField.text = ""
            messagetextField.resignFirstResponder()
        }
        
        
    }
    
    
    
    
    func sendMessage() {
        
        let createdAt = NSDate().timeIntervalSince1970
        let nodeStr = getMsgNode() // "167-323"
        let sender = Int(getStringValue(key: UserDefaultKeys.userId)) ?? 0
        let uSerId = driverId
        let senderImg = ""
        
        
        let messageItem: NSMutableDictionary = [
            "forUid_status":"\(uSerId)_unread",
            "time":"\(NSInteger(createdAt))",
            "from":"\(sender)",
            "sender_name":getStringValue(key: UserDefaultKeys.name),
            "sender_image": getStringValue(key: UserDefaultKeys.userImage),
            "reciever_name":userName,
            "reciever_image": userProfilePic,
            "message": self.message,
            "type":self.msgType,
            "fcm_token": getStringValue(key: UserDefaultKeys.fcmToken) ,
            "device_token":self.driver_DeviceToken,
            "reciever_id":"\(uSerId)"
            
            
        ]
        
        FireBaseManager.defaultManager.sendNewMsg(atNode: nodeStr, detailDict: messageItem as NSMutableDictionary, recentDict: messageItem as NSMutableDictionary)
        let dict = ["title": getStringValue(key: UserDefaultKeys.name),
                    "message": self.lastMsg,
                    "username": getStringValue(key: UserDefaultKeys.name),
                    "uid": "\(getStringValue(key: UserDefaultKeys.userId))",
            "image":senderImg,
            "type": "chat",
            "fcm_token": getStringValue(key: UserDefaultKeys.fcmToken) ,
            "device_token":self.driver_DeviceToken,
            ] as [String : Any]
        
        let notsender = PushNotificationSender()
        notsender.sendPushNotification(to: self.driver_DeviceToken, title: getStringValue(key: UserDefaultKeys.name), body: self.lastMsg, dict: dict)
        
        
        
    }
    
    
    
    @IBAction func actiomBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func registerNibs() {
        tableView.separatorStyle = .none
        tableView.register(UINib.init(nibName: "SenderCell", bundle : nil), forCellReuseIdentifier: "senderCell")
        tableView.register(UINib.init(nibName: "RecieverCell", bundle : nil), forCellReuseIdentifier: "recieverCell")
    }
    
    func scrollAtBottom() {
        if messages.count > 0 {
            let lastIndex = IndexPath(row: messages.count-1, section: 0)
            //print(lastIndex)
            tableView.scrollToRow(at: lastIndex, at: .bottom, animated: false)
        }
    }
    
    //MARK:- Create Node.
    func getMsgNode() -> String{
        
        var nodeStr = ""
        let num2 = Int(getStringValue(key: UserDefaultKeys.userId)) ?? 0
        let num1 = driverId
        
        
        if(num1<num2){
            nodeStr = "\(num1)-\(num2)"
        }else{
            nodeStr = "\(num2)-\(num1)"
        }
        return nodeStr
    }
    
    
    func updateMsgStatus(){
        let nodeStr = getMsgNode()
        //        let sender = UserDefault.standard.getCurrentUserId() ?? 0
        
        let sender = userId
        
        channelRef.child("messages").child(nodeStr).queryOrdered(byChild: "forUid_status").queryEqual(toValue: "\(sender)_unread").observe(.childAdded, with: { (snapshot) -> Void in
            if self.navigationController?.topViewController is ChatVC{
                if let messageData = snapshot.value as? Dictionary<String, Any>{
                    if let id = messageData["from"] as? String{
                        if id != "\(sender)" {
                            let updateNode = self.channelRef.child("messages").child(nodeStr).child(snapshot.key)
                            updateNode.updateChildValues(["forUid_status" : "\(sender)_read"])
                            
                        }
                    }
                }
            }
        })
        
        channelRef.child("messages").child(nodeStr).observe(.childRemoved) { (snapshot) in
            if let index = self.messages.firstIndex(where: { (message) -> Bool in
                return message.node == snapshot.key
            }){
                
                self.messages.remove(at: index)
                self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                
            }
        }
        
    }
    
    
    
}

extension ChatVC : FirebaseMessageManagerDelegate {
    func fireBaseManagerDidCompleteFetchingMessages() {
        messages = FireBaseManager.defaultManager.messages
        if messages.count > 0 {
            self.tableView.backgroundView = nil
        }else{
            self.tableView.backgroundView = CommonTableViewPlaceholderView.instantiate(NSLocalizedString("No Messages", comment: "No Messages"), image: UIImage(named:"no-data"))
        }
        tableView.reloadData()
        scrollAtBottom()
    }
    
    
    
}


extension ChatVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == messagetextField{
            textField.resignFirstResponder()
        }
        return true
    }
}

extension ChatVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
        
        let myId = getStringValue(key: UserDefaultKeys.userId)
        if messages[indexPath.row].id == "\(myId)" {
            if messages[indexPath.row].msgType == "Text" {
                let senderCell = tableView.dequeueReusableCell(withIdentifier: "senderCell") as? SenderCell
                senderCell?.senderMessageLabel.text = messages[indexPath.row].msgText
                let receiver = messages[indexPath.row].reciever_id
                let readStatus = messages[indexPath.row].readStatus
                senderCell?.tickMsgImageView.image = readStatus == "\(receiver)_read" ? UIImage(named: "Image") : UIImage(named: "Image-1")
                let userImages = defalut.string(forKey: "profile_img")  ?? ""
                setProfileImage(userImage: (senderCell?.userProfile)!, urlString: userImages)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                let date = dateFormatter.date(from: messages[indexPath.row].timeStamp!)
                dateFormatter.dateFormat = "HH:mm"
                let date24 = dateFormatter.string(from: date!)
                senderCell?.timeLabel.text = date24
                let string = messages[indexPath.row].msgText ?? ""
                let  newString = string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url: URL? = URL(string: newString ?? "")
                let urlExtension: String? = url?.pathExtension
                if urlExtension == "mp4" {
                    senderCell?.senderMessageLabel.textColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
                } else {
                    senderCell?.senderMessageLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
                
                senderCell?.selectionStyle = UITableViewCell.SelectionStyle.none
                cell = senderCell!
            } else if messages[indexPath.row].msgType == "Image" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderImageTableViewCell", for: indexPath) as! SenderImageTableViewCell
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                let date = dateFormatter.date(from: messages[indexPath.row].timeStamp!)
                dateFormatter.dateFormat = "HH:mm"
                let date24 = dateFormatter.string(from: date!)
                cell.timeLabel.text = date24
                cell.senderImage.image = selectImg
                cell.btn_thumbnail.isHidden = true
                SDWebImageManager.shared.loadImage(with: URL(string: messages[indexPath.row].imgURL ?? ""), options: .refreshCached, context: nil, progress: nil) { (image, isDownloader, error, cache, url, isTrue)  in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)  {
                        if image != nil {
                            cell.senderImage.image = image
                        }
                    }
                }
                return cell
            }else if messages[indexPath.row].msgType == "Video" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderImageTableViewCell", for: indexPath) as! SenderImageTableViewCell
                cell.btn_thumbnail.isHidden = false
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                let date = dateFormatter.date(from: messages[indexPath.row].timeStamp!)
                dateFormatter.dateFormat = "HH:mm"
                let date24 = dateFormatter.string(from: date!)
                cell.timeLabel.text = date24
                cell.senderImage.image = selectImg
                SDWebImageManager.shared.loadImage(with: URL(string: messages[indexPath.row].imgURL ?? ""), options: .refreshCached, context: nil, progress: nil) { (image, isDownloader, error, cache, url, isTrue)  in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)  {
                        if image != nil {
                            cell.senderImage.image = image
                        }
                    }
                }
                return cell
            }
            return cell
        } else {
            if messages[indexPath.row].msgType == "Text" {
                let recieverCell = tableView.dequeueReusableCell(withIdentifier: "recieverCell") as? RecieverCell
                recieverCell?.recieverMessageLabel.text = messages[indexPath.row].msgText
                //            recieverCell?.timeLabel.text = messages[indexPath.row].timeStamp
                setProfileImage(userImage: (recieverCell?.userProfileImage)!, urlString: userProfilePic)
                let string = messages[indexPath.row].msgText ?? ""
                let  newString = string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url: URL? = URL(string: newString ?? "")
                let urlExtension: String? = url?.pathExtension
                if urlExtension == "mp4" {
                    recieverCell?.recieverMessageLabel.textColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
                } else {
                    recieverCell?.recieverMessageLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                let date = dateFormatter.date(from: messages[indexPath.row].timeStamp!)
                dateFormatter.dateFormat = "HH:mm"
                let date24 = dateFormatter.string(from: date!)
                recieverCell?.timeLabel.text = date24
                
                
                recieverCell?.selectionStyle = UITableViewCell.SelectionStyle.none
                cell = recieverCell!
            } else if messages[indexPath.row].msgType == "Image"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverImageTableViewCell", for: indexPath) as! ReceiverImageTableViewCell
                cell.btn_thumbnail.isHidden = true
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                let date = dateFormatter.date(from: messages[indexPath.row].timeStamp!)
                dateFormatter.dateFormat = "HH:mm"
                let date24 = dateFormatter.string(from: date!)
                cell.timeLabel.text = date24
                cell.receiverImage.image = selectImg
                SDWebImageManager.shared.loadImage(with: URL(string: messages[indexPath.row].imgURL ?? ""), options: .refreshCached, context: nil, progress: nil) { (image, isDownloader, error, cache, url, isTrue)  in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)  {
                        if image != nil {
                            cell.receiverImage.image = image
                        }
                    }
                }
                return cell
            }else if  messages[indexPath.row].msgType == "Video" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverImageTableViewCell", for: indexPath) as! ReceiverImageTableViewCell
                cell.btn_thumbnail.isHidden = false
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                let date = dateFormatter.date(from: messages[indexPath.row].timeStamp!)
                dateFormatter.dateFormat = "HH:mm"
                let date24 = dateFormatter.string(from: date!)
                cell.timeLabel.text = date24
                cell.receiverImage.image = selectImg
                SDWebImageManager.shared.loadImage(with: URL(string: messages[indexPath.row].imgURL ?? ""), options: .refreshCached, context: nil, progress: nil) { (image, isDownloader, error, cache, url, isTrue)  in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)  {
                        if image != nil {
                            cell.receiverImage.image = image
                        }
                    }
                }
                return cell
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if messages[indexPath.row].msgType == "Text" {
            let string = messages[indexPath.row].msgText ?? ""
            let  newString = string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let url: URL? = URL(string: newString ?? "")
            let urlExtension: String? = url?.pathExtension
            if urlExtension == "mp4" {
                let fileUrl = URL(fileURLWithPath: string)
                self.ShowVideo(videoUrl: string ?? "")
            }
        } else if messages[indexPath.row].msgType == "Image" {
            let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageViewPopUpVC") as! ImageViewPopUpVC
            Vc.modalPresentationStyle = .fullScreen
            Vc.imgUrl = messages[indexPath.row].imgURL ?? ""
            Vc.isFromChat = true
            self.present(Vc, animated: true, completion: nil)
        }else{
            self.ShowVideo(videoUrl: messages[indexPath.row].videoUrl)
        }
    }
    
    func ShowVideo(videoUrl: String) {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        }
        catch {
            
        }
        let videoURL = URL(string: videoUrl)
        let player = AVPlayer(url: videoURL!)
        let playerVC = AVPlayerViewController()
        playerVC.videoGravity = AVLayerVideoGravity(rawValue: AVLayerVideoGravity.resizeAspect.rawValue)
        playerVC.player = player
        
        self.navigationController?.present(playerVC, animated: true, completion: {
            playerVC.player?.play()
        })
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetHighestQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
}



extension ChatVC: PhotoEditViewControllerDelegate, VideoEditViewControllerDelegate  {
    
    
    func shouldAddToSelection(indexPath: IndexPath, numSelections: Int) -> Bool {
        return true// indexPath.row != 2
    }
    
    func videoEditViewController(_ videoEditViewController: VideoEditViewController, didFinishWithVideoAt url: URL?) {
        
        if let urlStr = url{
            
            ApiManager.getThumbnailImageFromVideoUrl(url: urlStr) { (thumbnailImage) in
                if let image = thumbnailImage{
                    LoaderClass.startLoader()
                    let data = NSData(contentsOf: urlStr)!
                    print("File size before compression: \(Double(data.length / 1048576)) mb")
                    let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mp4")
                    self.compressVideo(inputURL: urlStr, outputURL: compressedURL) { (exportSession) in
                        
                        guard let session = exportSession else {
                            return
                        }
                        
                        switch session.status {
                        case .unknown:
                            break
                        case .waiting:
                            break
                        case .exporting:
                            break
                        case .completed:
                            guard let compressedData = NSData(contentsOf: compressedURL) else {
                                return
                            }
                            print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                            
                            FireBaseManager.defaultManager.uploadFile(node: self.getMsgNode(), url: compressedURL as URL, fileType: .video) { (firebaseVideoUrl) in
                                FireBaseManager.defaultManager.uploadImageOnFirebase(node: self.getMsgNode(), image: image) { (Image) in
                                    LoaderClass.stopLoader()
                                    self.msgType = "Video"
                                    self.lastMsg = "Video"
                                    self.message = "Video"
                                    self.imageURL = Image
                                    self.videoURL = firebaseVideoUrl
                                    self.setMsgData()
                                }
                            }
                        case .failed:
                            break
                        case .cancelled:
                            break
                        @unknown default:
                            print("Compression failed.........")
                        }
                    }
                }
                self.dismiss(animated: true, completion: nil)
            }
            
            print("saved")
        }
        
        print("saved")
    }
    
    func videoEditViewControllerDidFailToGenerateVideo(_ videoEditViewController: VideoEditViewController) {
        print("edit")
    }
    
    func videoEditViewControllerDidCancel(_ videoEditViewController: VideoEditViewController) {
        videoEditViewController.dismiss(animated: true, completion: nil)
        print("cancel")
    }
    
    func photoEditViewController(_ photoEditViewController: PhotoEditViewController, didSave image: UIImage, and data: Data) {
        self.msgType = "Image"
        self.lastMsg = "Image"
        self.didSelect(image: image)
        //        self.thumb_name = ""
        //        self.thumb_data = Data()
        //        self.isImageSelected = true
        //        self.imgView_placeholder.image = image
        //        self.imgView_placeholder.contentMode = .scaleAspectFill
        //        self.post_data = data
        self.dismiss(animated: true, completion: nil)
        print("saved")
    }
    
    func photoEditViewControllerDidFailToGeneratePhoto(_ photoEditViewController: PhotoEditViewController) {
        print("edit")
    }
    
    func photoEditViewControllerDidCancel(_ photoEditViewController: PhotoEditViewController) {
        photoEditViewController.dismiss(animated: true, completion: nil)
        print("cancel")
    }
}
