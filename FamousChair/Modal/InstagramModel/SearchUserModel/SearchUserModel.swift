//
//  SearchModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 29/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import SwiftyJSON
class SearchUserModel: NSObject {
    let error : String?
    let message : String?
    var result = [SearchUserData]()
    init(response:JSON) {
        self.error = response["status"].stringValue
        self.message = response["message"].stringValue
        for item in response["data"].arrayValue{
            let value = SearchUserData.init(item: item)
            self.result.append(value)
        }
    }
}
class SearchUserData :NSObject {
    let name : String?
    let image : String?
    let userId :String?
    init(item:JSON) {
        self.name = item["name"].stringValue
        self.image = item["profile_image"].stringValue
        self.userId = item["id"].stringValue
       
    }
}
