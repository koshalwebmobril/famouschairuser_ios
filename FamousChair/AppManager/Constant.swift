//
//  Constant.swift
//  FamousChair
//
//  Created by ginger webs on 01/07/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

 let defalut = UserDefaults.standard
struct WebServiceApi {
    static let base_url = "https://webmobril.org/dev/famous_chair/api/"
    static let baseImage_url = "https://webmobril.org/dev/famous_chair/" 
    
    static let login = base_url + "login"
    static let signUp = base_url + "register"
    static let forgotPassword = base_url + "forgot_password"
    static let Otp = base_url + "verify_otp"
    static let resetPassword = base_url + "reset_password"
    static let staticData = base_url + "static_data"
    static let contactUS = base_url + "contact_us"
    static let home_url = base_url + "home"
    static let getProfile_url = base_url + "get_profile"
    static let updateProfile_url = base_url + "update_profile"
    static let getAllServices_url = base_url + "get_all_service"
    static let search_url = base_url + "search_service"
    static let getProviderServices_url = base_url + "get_provider_services"
    static let serviceDetail_url = base_url + "service_details"
    static let socialLogin = base_url + "social_login"
    static let bookServices = base_url + "booking"
    static let bookingList = base_url + "get_bookings"
    static let transactionHistory = base_url + "transaction_history"
    static let getNotifications = base_url + "get_notifications"
    static let bookingDetails = base_url + "get_booking_details"
    static let addReview = base_url + "add_review"
    static let cancelBooking = base_url + "cancel_booking"
    static let generate_paypal_url = base_url + "generate_paypal_token"
    
    //Mark:- Instagram related apis
    
    static let homeData_url = base_url + "home_data"
    static let post_share_url = base_url + "post_share"
    static let following_url = base_url + "following"
    static let unfollowing_url = base_url + "unfollowing_users"
    static let like_url = base_url + "like"
    static let unlike_url = base_url + "unlike"
    static let get_my_profile_url = base_url + "get_my_profile"
    static let makecomments_url = base_url + "makecomments"
    static let getcomments_url = base_url + "getcomments"
    static let like_user_url = base_url + "like_user"
    static let hidepost_url = base_url + "hidepost"
    static let search_people_url = base_url + "search_people"
    static let following_list_url = base_url + "following_list"
    static let follower_list_url = base_url + "follower_list"
    static let edit_comment_url = base_url + "edit_comment"
    static let delete_comment_url = base_url + "delete_comment"
    static let delete_post_url = base_url + "delete_post"
    static let share_post_url = base_url + "share_post"
    static let profile_wall_url = base_url + "profile_wall"
    static let report_post_url = base_url + "report_post"
    static let delete_posted_post_url = base_url + "delete_post"
    
    
    static let go_live_url = "https://webmobril.org/dev/famous_chair/agora/RtcTokenBuilderSample.php" //"https://webmobril.org/dev/agora/RtcTokenBuilderSample.php"
    static let getliveid_url = base_url + "getliveid"
    static let end_live_url = base_url + "end_live_streaming"
    static let getlive_user_url = base_url + "getlive_user"
    static let postInsta_notification_url = base_url + "notification"
    
}
