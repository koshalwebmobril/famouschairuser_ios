//
//  ApiConstants.swift
//  Wingmen
//
//  Created by Sandip Gill on 3/11/20.
//  Copyright © 2020 apptunix. All rights reserved.
//

import UIKit




let BASEURL = ""


let downloadAppUrl = ""


//MARK:- Getter/Setter

let userDefault = UserDefaults.standard

func saveBoolValue(status : String, key : String) {
    userDefault.set(status, forKey: key)
}

func getBoolValue(key : String) -> Bool {
    return userDefault.bool(forKey: key)
}

func saveStringValue(value : String, key : String) {
    userDefault.set(value, forKey: key)
}

func getStringValue(key : String) -> String {
    return (userDefault.value(forKey: key) == nil) ? "" : userDefault.value(forKey: key) as! String
}

func saveDictionaryValue(value:Any, withKey key:String) {
    userDefault.set(value, forKey: key)
}

func getDictionaryValue(withKey key:String) -> Any {
    return userDefault.value(forKey: key) as Any
}

struct UserDefaultKeys {
    static let email = "email"
    static let password = "password"
    static let userData = "userData"
    static let name = "name"
    static let isLoggedIn = "isLoggedIn"
    static let userId = "userId"
    static let userImage = "userImage"
    static let loginToken = "loginToken"
    static let deviceId = "deviceId"
    
    static let fcmToken = "fcm_token"
    

    static let isLanguageSelected = "isLanguageSelected"
    static let appVersion = "appVersion"
    static let languageCode = "languageCode"
    
    static let cartDict = "cartDict"
    static let cartArray = "cartArray"
    static let badgeCount = "badgeCount"
    static let notification = "notification"

}


extension Notification.Name {
    static let userListRefresh = Notification.Name("userListRefresh")
    static let likeRefresh = Notification.Name("likeRefresh")
    static let chatRefresh = Notification.Name("chatRefresh")
    static let openChatWindow = Notification.Name("openChatWindow")
    static let LiveUserRefersh = Notification.Name("LiveUserRefersh")
    static let userremoved_add = Notification.Name("userremoved_add")
    static let movie_created = Notification.Name("movie_created")
}
