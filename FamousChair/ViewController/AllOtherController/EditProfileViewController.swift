//
//  EditProfileViewController.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 04/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    var imagePicker = UIImagePickerController()
    var profileDict : ProfileModal?
    
    
    var userProfile = String()
    var name = String()
    var email = String()
    var isComefrom = Bool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        setUIData()
    }
    func setUIData(){
        if name != ""{
            ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(userProfile)", imageView: profileImgView!,placeHolder: "Image")
            txtName.text = name
            emailLabel.text = email
        }else{
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(profileDict?.result?.profile_image ?? "")", imageView: profileImgView!,placeHolder: "Image")
        txtName.text = profileDict?.result?.name ?? ""
        emailLabel.text = profileDict?.result?.email ?? ""
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if txtName.text!.isEmpty{
            self.createAlert(title: "Famous Chair", message: "Please enter full name.")
        }else{
            let parameter = ["name":txtName.text ?? ""]
            ProfileVM.callUpdateApi(param: parameter, imageData: (profileImgView.image?.jpegData(compressionQuality: 0.8)) ?? Data(), imageName: "profile_image", viewController: self)
        }
    }
    
    @IBAction func cameraBtnAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
extension EditProfileViewController :UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        profileImgView.image = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
}
