//
//  BookingList.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 18/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
import SwiftyJSON
class BookingList : NSObject {
    let error : String?
    let message :String?
    var arr_upcommingbooking = [BookingData]()
    var arr_prevbooking = [BookingData]()

    
    init(data:JSON) {
        self.error = data["status"].stringValue
        self.message = data["message"].stringValue
        for item in data["data"]["upcoming_booking"].arrayValue{
            let bookingData = BookingData.init(item: item)
            self.arr_upcommingbooking.append(bookingData)
        }
        
        for item in data["data"]["previous_booking"].arrayValue{
            let bookingData = BookingData.init(item: item)
            self.arr_prevbooking.append(bookingData)
        }
        
    }
    
}
