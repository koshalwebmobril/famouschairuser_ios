

import Foundation
struct SignUpModal: Codable {
	let status : String?
    let message : String?
    let token : String?
	let result : Result?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case result = "data"
        case message = "message"
        case token = "token"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		result = try values.decodeIfPresent(Result.self, forKey: .result)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        token = try values.decodeIfPresent(String.self, forKey: .token)
	}

}
