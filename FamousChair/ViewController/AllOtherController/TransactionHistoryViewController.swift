//
//  TransactionHistoryViewController.swift
//  FamousChair
//
//  Created by ginger webs on 09/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class TransactionHistoryViewController: UIViewController {
    
    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var arr_transaction : TransactionModal?

    override func viewDidLoad() {
        super.viewDidLoad()

        TransactionVM.callTransactionHistoryServicesApi(params: [String : Any](), viewController: self) { (responseObject) in
            self.arr_transaction = responseObject
            if self.arr_transaction?.arr_history.count ?? 0 > 0{
                self.tableView.isHidden = false
                self.blankView.isHidden = true
            }else{
                self.tableView.isHidden = true
                self.blankView.isHidden = false
            }
            
            self.tableView.reloadData()
        }
    }
    
     override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(true, animated: true)

     }


    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension TransactionHistoryViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_transaction?.arr_history.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath) as! TransactionTableViewCell
        cell.providerLabel.text = arr_transaction?.arr_history[indexPath.row].providerName ?? ""
        cell.serviceLabel.text = arr_transaction?.arr_history[indexPath.row].serviceName ?? ""
        cell.amountLabel.text = "$\(arr_transaction?.arr_history[indexPath.row].price ?? "")"
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date  = formatter.date(from:  arr_transaction?.arr_history[indexPath.row].date ?? "")
        formatter.dateFormat = "d MMM YYYY"
        cell.dateLabel.text = formatter.string(from: date ?? Date())
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
}
