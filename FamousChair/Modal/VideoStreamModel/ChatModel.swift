//
//  ChatModel.swift
//  zipHawk
//
//  Created by WebMobril on 02/03/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import Foundation
import UIKit

class Contacts {
    
    var activeNow: String?
    var deviceToken: String?
    var name: String?
    var email: String?
    var image: String?
    var mobile: String?
    var id: String?
    var rollNo: String
    var isFriend: Bool
    
    init(activeNow: String?, deviceToken: String?, name: String?, email: String?, image: String?, mobile: String?, id: String?, rollNo: String, isFriend: Bool) {
        
        self.activeNow = activeNow
        self.deviceToken = deviceToken
        self.name = name
        self.email = email
        self.image = image
        self.mobile = mobile
        self.id = id
        self.rollNo = rollNo
        self.isFriend = isFriend
    }
}


class Message {
    var audioUrl: String
    var audioDuration: Int
    var msgText: String?
    var id: String?
    var reciever_id: String
    var timeStamp: String?
    var msgType: String?
    var imgURL: String?
    var senderName: String?
    var node: String?
    var readStatus: String
    var videoUrl: String
    var videoDuration: Int
    var urlName: String
    var completeUrl: String
    var urlDescription: String
    var fileUrl: String
    var fileType: String
    
    init(msgText: String?, senderId: String?, timeStamp: String?, msgType: String?, imgURL: String?, senderName: String?, node: String?, readStatus: String, reciever_id: String, audioUrl: String, audioDuration: Int, videoUrl: String, videoDuration: Int, urlName: String, completeUrl: String, urlDescription: String, fileUrl: String, fileType: String ) {
        
        self.msgText = msgText
        self.id = senderId
        self.timeStamp = timeStamp
        self.msgType = msgType
        self.imgURL = imgURL
        self.senderName = senderName
        self.node = node
        self.readStatus = readStatus
        self.reciever_id = reciever_id
        self.audioUrl = audioUrl
        self.audioDuration = audioDuration
        self.videoUrl = videoUrl
        self.videoDuration = videoDuration
        self.urlName = urlName
        self.completeUrl = completeUrl
        self.urlDescription = urlDescription
        self.fileUrl = fileUrl
        self.fileType = fileType
    }
    
}


class RecentChats{
    var myUnreadCount: Int
    var friendUnreadCount: Int
    var myId: String?
    var msgType: String?
    var lastMsg: String?
    var time: String?
    var senderId: String?
    var senderImg: String?
    var senderName: String?
    var recieverId: String?
    var recieverImg: String?
    var recieverName: String?
    var device_token: String?
    var fcm_token: String?
    var node: String?
    var isBlocked: Bool
    
    init(myUnreadCount: Int, friendUnreadCount: Int, myId: String?, msgType: String?, lastMsg: String?, time: String?, senderId: String?, senderImg: String?, senderName: String?, recieverId: String?, recieverImg: String?, recieverName: String?, node: String?,device_token: String?,fcm_token: String?, isBlocked: Bool) {
        
        self.myUnreadCount = myUnreadCount
        self.friendUnreadCount = friendUnreadCount
        self.myId = myId
        self.msgType = msgType
        self.lastMsg = lastMsg
        self.time = time
        self.senderId = senderId
        self.senderImg = senderImg
        self.senderName = senderName
        self.recieverId = recieverId
        self.recieverImg = recieverImg
        self.recieverName = recieverName
        self.node = node
        self.isBlocked = isBlocked
        self.device_token = device_token
        self.fcm_token = fcm_token
    }
}


class ChatFriend {
    var id: String
    var name: String
    var image: String
    
    init(id: String, name: String, image: String){
        self.id = id
        self.name = name
        self.image = image
    }
}


enum FileType: CaseIterable {
case audio, video, pdf, doc, docx, ppt, pptx

var fileExtension: String {
    switch self {
    case .audio: return "mp3"
    case .video: return "mov"
    case .pdf: return "pdf"
    case .doc: return "doc"
    case .docx: return "docx"
    case .ppt: return "ppt"
    case .pptx: return "pptx"
    }
}
    
    var contentType: String {
        switch self {
        case .audio: return "audio/mp3"
        case .video: return "video/mov"
        case .pdf: return "file/pdf"
        case .doc: return "file/doc"
        case .docx: return "file/docx"
        case .ppt: return "file/ppt"
        case .pptx: return "file/pptx"
        }
    }
    
    var icon: UIImage? {
        switch self {
        case .audio: return nil
        case .video: return nil
        case .pdf: return #imageLiteral(resourceName: "pdf")
        case .doc, .docx: return #imageLiteral(resourceName: "doc")
        case .ppt, .pptx: return #imageLiteral(resourceName: "ppt")
        }
    }
}

