//
//  FirebaseManager.swift
//  zipHawk
//
//  Created by Sandeep Kumar on 02/03/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//


import Foundation
import Firebase
import FirebaseStorage
import FirebaseDatabase

typealias Completion = (Bool) -> Void
typealias onCompletion = (String) -> Void
typealias UserCompletion = ([String:Any]?) -> Void
typealias UserOnlineStatusCompletion = (Bool?, NSInteger?) -> Void

@objc protocol FirebaseContactManagerDelegate {
    @objc optional func fireBaseManagerDidCompleteFetchingContacts()
}
@objc protocol FirebaseMessageManagerDelegate {
    @objc optional func fireBaseManagerDidCompleteFetchingMessages()
}
@objc protocol FirebaseRecentChatsManagerDelegate {
    @objc optional func fireBaseManagerDidCompleteFetchingRecentChats()
}
class FireBaseManager: NSObject {
    // Can't init is singleton
    
    static let defaultManager = FireBaseManager()
    var contactManagerDelegate: FirebaseContactManagerDelegate?
    var messageManagerDelegate: FirebaseMessageManagerDelegate?
    var recentChatsManagerDelegate: FirebaseRecentChatsManagerDelegate?
    var contacts:[Contacts] = []
    var messages:[Message] = []
    var recentChats:[RecentChats] = []
//
//    var requestedContacts:[ConnectionRequests] = []
    static let databaseRoot = Database.database().reference()
   //  static let databaseChats = databaseRoot.child("messages")
    var userStatusRef: DatabaseReference = databaseRoot.child("UserOnlineStatus")
    
    var usersRef: DatabaseReference = databaseRoot.child("Users")
    var msgRef: DatabaseReference = databaseRoot.child("messages")
    var liveRef: DatabaseReference = databaseRoot.child("live_message")
    var recentsRef: DatabaseReference = databaseRoot.child("recents")
    var driverRef: DatabaseReference = databaseRoot.child("Drivers")
    var requestsRef: DatabaseReference = databaseRoot.child("request")
    var friendsRef: DatabaseReference = databaseRoot.child("friends")
    static let storageRoots = Storage.storage().reference()
    
//    static let storageImage = storageRoots.child("message_imaes")
    var storageRef: StorageReference = storageRoots.child("message_imaes")
//    var storageRef: StorageReference = Storage.storage().reference(forURL: "gs://welldone-155a9.appspot.com")
    var isChannelPresent = false
    var isMessagePresent: Bool?
    
    func updateUserProfileImage(id: String, imageStr: String) {
        
        usersRef.child(id).updateChildValues(["user_image":imageStr])
    }
    

    
    //Function to check whether a user exists or not under "Users" node on Firebase.
    func checkUserExistence(studentId: String, onCompletion: @escaping Completion) -> (){
        usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.hasChild(studentId){
                onCompletion(true)
                print("rooms exist")
            }else{
                onCompletion(false)
                print("room doesn't exist")
            }
            
        })
    }
    
    //Function to check whether a user exists or not under "requests" node on Firebase.
    func checkRequestExistence(friendId: String, onCompletion: @escaping (Bool, String)->()) -> (){
        let myId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
        Database.database().reference().child("request").child("\(myId)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild(friendId){
                debugPrint(snapshot)
                var reqType = ""
                if let array = snapshot.children.allObjects as? [DataSnapshot] {
                    for child in array {
                        let id = child.key
                        let type = child.childSnapshot(forPath: "type").value as? String ?? ""
                        if id == friendId {
                            reqType = type
                            break
                        }
                    }
                }
                onCompletion(true, reqType)
                print("Request exist")
            }else{
                onCompletion(false,"")
                print("Request doesn't exist")
            }
        })
    }
    
    //Function to check whether a user exists or not under "friends" node on Firebase.
    func checkFriendExistence(friendId: String, onCompletion: @escaping Completion) -> (){
        let myId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
        Database.database().reference().child("friends").child("\(myId)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild(friendId){
                onCompletion(true)
            }else{
                onCompletion(false)
            }
        })
    }
    
    func sendConnectionRequest(friendId: String){
        let myId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
        let time = Date().timeIntervalSince1970
        Database.database().reference().child("request").child(friendId).child("\(myId)").setValue(["type":"received", "time":"\(NSInteger(time))"])
        Database.database().reference().child("request").child("\(myId)").child(friendId).setValue(["type":"sent", "time":"\(NSInteger(time))"])
    }
    
    func acceptConnectionRequest(friendId: String) {
        self.cancelConnectionRequest(friendId: friendId) { (response) in
            if let response = response {
                if let error = response["error"] as? String , error == ""{
                    let myId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
                    let time = Date().timeIntervalSince1970
                    Database.database().reference().child("friends").child(friendId).child("\(myId)").setValue(["time":"\(NSInteger(time))"])
                    Database.database().reference().child("friends").child("\(myId)").child(friendId).setValue(["time":"\(NSInteger(time))"])
                }
            }
        }
    }
    
    func cancelConnectionRequest(friendId: String, onCompletion: @escaping UserCompletion) -> (){
        let myId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
        Database.database().reference().child("request").child("\(myId)").child(friendId).removeValue { (error, ref) in
            debugPrint("Deleted at - \(ref)")
            if error == nil {
                Database.database().reference().child("request").child(friendId).child("\(myId)").removeValue { (error, ref) in
                    debugPrint("Deleted at - \(ref)")
                    onCompletion(["error": error != nil ? error!.localizedDescription : ""])
                }
            }
        }
    }
    
    func fetchAllConnectionRequests(onCompletion: @escaping UserCompletion) -> (){
        let studentId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
        Database.database().reference().child("request").child("\(studentId)").observe(.value) { (snapshot) in
            if let requests = snapshot.value, !(requests is NSNull) {
                if let requestsData = snapshot.value as? Dictionary<String, Any> {
                    onCompletion(requestsData)
                }else{
                    onCompletion(nil)
                }
            }else{
                onCompletion(nil)
            }
        }
    }
    

    //Function to create a new node under "Users" node on Firebase.
    func createNewUser(studentId: String, detailDict: NSDictionary){
        usersRef.child(studentId).setValue(detailDict)
    }
    
    /** Gets the Contacts object for the specified user id */
    func getUser(_ userID: String, completion: @escaping (Contacts) -> Void) {
        usersRef.child(userID).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            let activeNow = snapshot.childSnapshot(forPath: "active_now").value as? String ?? ""
            let deviceToken = snapshot.childSnapshot(forPath: "device_token").value as? String ?? ""
            let name = snapshot.childSnapshot(forPath: "user_name").value as? String ?? ""
            let email = snapshot.childSnapshot(forPath: "user_email").value as? String ?? ""
            let image = snapshot.childSnapshot(forPath: "user_image").value as? String ?? ""
            let mobile = snapshot.childSnapshot(forPath: "user_mobile").value as? String ?? ""
            let rollNo = snapshot.childSnapshot(forPath: "roll_no").value as? String ?? ""
            
            let id = snapshot.key
            completion(Contacts(activeNow: activeNow, deviceToken: deviceToken, name: name, email: email, image: image, mobile: mobile, id: id, rollNo: rollNo, isFriend: false))
        })
    }
    
   
    
    func getFriendUreadCount(node: String, friendId: String, onCompletion: @escaping (Int)->())->() {
        self.recentsRef.child(node).observeSingleEvent(of: .value) { (snapshot) in
            if let data = snapshot.value, !(data is NSNull) {
                if let data = data as? Dictionary<String, Any> {
                    let key = "\(friendId)_unread"
                    let unreadCount = data[key] as? Int ?? 0
                    onCompletion(unreadCount)
                }else{
                    onCompletion(0)
                }
            }else{
                onCompletion(0)
            }
        }
    }
    
    func getMyTotalUnreadCount(onCompletion: @escaping (Int)->())->() {
        let studentId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
        var unreadCOunt = 0
        self.recentsRef.observe(.childAdded, with: { (snapshot) -> Void in
            if let channelData = snapshot.value, !(channelData is NSNull){
                if let channelData = snapshot.value as? Dictionary<String, Any>{
                    let node = snapshot.key//channelData["node"] as? String ?? ""
                    let ids = node.components(separatedBy: "-")
                    if ids.contains("\(studentId)") {
                        let myUnreadCount = channelData["\(studentId)_unread"] as? Int ?? 0
                        unreadCOunt = unreadCOunt+myUnreadCount
                        onCompletion(unreadCOunt)
                    }
                }
            }
        })
        
        self.recentsRef.observe(.value, with: { (snapshot) -> Void in
            var unreadCOunt = 0
            if let channelData = snapshot.value, !(channelData is NSNull){
                if let array = snapshot.children.allObjects as? [DataSnapshot] {
                    for child in array {
                        let node = child.key
                        let ids = node.components(separatedBy: "-")
                        if ids.contains("\(studentId)") {
                            let myUnreadCount = child.childSnapshot(forPath: "\(studentId)_unread").value as? Int ?? 0
                            unreadCOunt = unreadCOunt+myUnreadCount
                            onCompletion(unreadCOunt)
                        }
                    }
                }
            }
        })
        
    }
    
    //Function to set data for each message under "Messages" node on Firebase.
    func sendNewMsg(atNode: String, detailDict: NSMutableDictionary, recentDict: NSMutableDictionary){
        
        let messageRef = msgRef.child(atNode).childByAutoId()
        detailDict.setValue(messageRef.key, forKey: "message_id")
        messageRef.setValue(detailDict)
        
//        guard let recentDict = recentDict else{return}
        let channelRef = recentsRef.child(atNode)
        recentDict.setValue(channelRef.key, forKey: "node")
        channelRef.setValue(recentDict)
    }
    
    //Function to set data for each message under "Messages" node on Firebase.
    func sendLiveNewMsg(atNode: String, detailDict: NSMutableDictionary, recentDict: NSMutableDictionary?){
        let messageRef = liveRef.child(atNode).childByAutoId()
        detailDict.setValue(messageRef.key, forKey: "Live_ChatId")
        messageRef.setValue(detailDict)
    }
    
    //Function to upload image in the storage section of Firebase and return the image URL.
    func uploadImageOnFirebase(node: String, image: UIImage , onCompletion: @escaping onCompletion) -> (){
        
        let data1 = image.jpegData(compressionQuality: 1)!
        debugPrint("Uncompressed KB - \(Double(data1.count) / 1000.0)")
        
        let createdAt = NSDate().timeIntervalSince1970
        
        let riversRef = storageRef.child(node).child("images.jpg-\(createdAt)")
        
        // Metadata contains file metadata such as size, content-type, and download URL.
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        DispatchQueue.global().async(execute: {
            let data = image.jpegData(compressionQuality: 0)!
            debugPrint("Compressed KB - \(Double(data.count) / 1000.0)")
            riversRef.putData(data, metadata: metadata) { (metadata, error) in
                DispatchQueue.main.async(execute: {
                    if let error = error {
                        print("Error uploading photo: \(error)")
                        return
                    }
                    riversRef.downloadURL(completion: { (downloadURL, error) in
                        if let url = downloadURL {
                            onCompletion(url.absoluteString)
                        }
                    })
                })
            }
        })
    }
    

    
    //Function to upload audio in the storage section of Firebase and return the audio URL.
    func uploadAudioOnFirebase(node: String, data: Data , onCompletion: @escaping onCompletion) -> (){
        let createdAt = NSDate().timeIntervalSince1970
        let riversRef = storageRef.child(node).child("\(NSInteger(createdAt))audio.mp3")
        // Metadata contains file metadata such as size, content-type, and download URL.
        let metadata = StorageMetadata()
        metadata.contentType = "audio/mp3"
        DispatchQueue.global().async(execute: {
            riversRef.putData(data, metadata: metadata) { (metadata, error) in
                DispatchQueue.main.async(execute: {
                    if let error = error {
                        print("Error uploading audio: \(error)")
                        return
                    }
                    riversRef.downloadURL(completion: { (downloadURL, error) in
                        if let url = downloadURL {
                            onCompletion(url.absoluteString)
                        }
                    })
                })
            }
        })
    }
    
    //Function to fetch all the friends.
    func fetchContacts(){
        let studentId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
        Database.database().reference().child("friends").child("\(studentId)").observe(DataEventType.value, with: { (snapshot) in
            if let array = snapshot.children.allObjects as? [DataSnapshot]{
                for child in array {
                    let id = child.key
                    self.getUser(id, completion: { (user) in
                        if !self.contacts.contains(where: { (contact) -> Bool in
                            return contact.id == id
                        }){
                            self.contacts.append(user)
                            if self.contactManagerDelegate != nil{
                                self.contactManagerDelegate?.fireBaseManagerDidCompleteFetchingContacts!()
                            }
                        }
                    })
                }
            }
        })
       
    }
    
    func fetchAllUsers() {
        usersRef.observe(.childAdded, with: { (snapshot) in
            
            if let messageData = snapshot.value, !(messageData is NSNull) {
                let messageData = snapshot.value as! Dictionary<String, Any>
                
                let activeNow = messageData["active_now"] as? String ?? ""
                let deviceToken = messageData["device_token"] as? String ?? ""
                let name = messageData["user_name"] as? String ?? ""
                let email = messageData["user_email"] as? String ?? ""
                let image = messageData["user_image"] as? String ?? ""
                let mobile = messageData["user_mobile"] as? String ?? ""
                let rollNo = messageData["roll_no"] as? String ?? ""
                
                let studentId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
                
                if snapshot.key != "\(studentId)"{
                    
                    self.contacts.append(Contacts(activeNow: activeNow, deviceToken: deviceToken, name: name, email: email, image: image, mobile: mobile, id: snapshot.key, rollNo: rollNo, isFriend: false))
                }
                if self.contactManagerDelegate != nil{
                    self.contactManagerDelegate?.fireBaseManagerDidCompleteFetchingContacts!()
                }
            }
        })
    }
    
    //Fetch a new message.
    func fetchMessages(node: String) {
        
        msgRef.child(node).observe(.childAdded, with: { (snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                guard let messageData = snapshot.value as? Dictionary<String, Any> else {return}
                
                //print("messageData\(messageData)")
                let audioUrl = messageData["audioUrl"] as? String ?? ""
                let audio_duration = messageData["audio_duration"] as? Int ?? 0
                let timeStamp:String = String(format: "%@", messageData["time"] as! CVarArg)
                let imgURL = messageData["picture"]
                let senderId = messageData["from"]
                let reciever_id = messageData["reciever_id"] as? String ?? ""
                let senderName = ""//messageData["sendername"]
                let msgText = messageData["message"]
                let msgType = messageData["type"]
                let node = messageData["message_id"] as? String ?? ""
                let readStatus = messageData["forUid_status"] as? String ?? ""
                let videoUrl = messageData["videoUrl"] as? String ?? ""
                let videoDuration = messageData["videoDuration"] as? Int ?? 0
                let urlName = messageData["urlName"] as? String ?? ""
                let completeUrl = messageData["completeUrl"] as? String ?? ""
                let urlDescription = messageData["url_Description"] as? String ?? ""
                let fileUrl = messageData["fileUrl"] as? String ?? ""
                let fileType = messageData["fileType"] as? String ?? ""
                let dateString = self.convertTimeStampToDateString(timeStamp: (timeStamp as NSString).integerValue)
                
                self.isMessagePresent = false
                
                if self.messages.count != 0{
                    for index in 0..<self.messages.count {
                        if node == self.messages[index].node{
                            self.isMessagePresent = true
                            break
                        }
                    }
                    if self.isMessagePresent == false{
                        self.messages.append(Message(msgText: msgText as? String, senderId: senderId as? String, timeStamp: dateString , msgType: msgType as? String, imgURL: imgURL as? String, senderName: senderName, node: node, readStatus: readStatus, reciever_id: reciever_id, audioUrl: audioUrl, audioDuration: audio_duration, videoUrl: videoUrl, videoDuration: videoDuration, urlName: urlName, completeUrl: completeUrl, urlDescription: urlDescription, fileUrl: fileUrl, fileType: fileType))
                    }
                }else{
                    self.messages.append(Message(msgText: msgText as? String, senderId: senderId as? String, timeStamp: dateString , msgType: msgType as? String, imgURL: imgURL as? String, senderName: senderName, node: node, readStatus: readStatus, reciever_id: reciever_id, audioUrl: audioUrl, audioDuration: audio_duration, videoUrl: videoUrl, videoDuration: videoDuration, urlName: urlName, completeUrl: completeUrl, urlDescription: urlDescription, fileUrl: fileUrl, fileType: fileType))
                }
                if self.messageManagerDelegate != nil {
                    self.messageManagerDelegate?.fireBaseManagerDidCompleteFetchingMessages!()
                }
            }
        })
    }
    
    
    //MARK:
    //MARK: fetch Messages
    


    
    func fetch_Live_Messages(node: String) {
        liveRef.child(node).observe(.childAdded, with: { (snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                guard let messageData = snapshot.value as? Dictionary<String, Any> else {return}
                //print("messageData\(messageData)")
                let audioUrl = messageData["audioUrl"] as? String ?? ""
                let audio_duration = messageData["audio_duration"] as? Int ?? 0
                let timeStamp:String = ""
                let imgURL = messageData["user_img"]
                let senderId = messageData["from"]
                let reciever_id = messageData["reciever_id"] as? String ?? ""
                let senderName = messageData["name"] as? String ?? ""
                let msgText = messageData["message"]
                let msgType = messageData["type"]
                let node = messageData["message_id"] as? String ?? ""
                let readStatus = messageData["forUid_status"] as? String ?? ""
                let videoUrl = messageData["videoUrl"] as? String ?? ""
                let videoDuration = messageData["videoDuration"] as? Int ?? 0
                let urlName = messageData["urlName"] as? String ?? ""
                let completeUrl = messageData["completeUrl"] as? String ?? ""
                let urlDescription = messageData["url_Description"] as? String ?? ""
                let fileUrl = messageData["fileUrl"] as? String ?? ""
                let fileType = messageData["fileType"] as? String ?? ""
                let dateString = self.convertTimeStampToDateString(timeStamp: (timeStamp as NSString).integerValue)
                
                self.isMessagePresent = false
                
              
                        self.messages.append(Message(msgText: msgText as? String, senderId: senderId as? String, timeStamp: dateString , msgType: msgType as? String, imgURL: imgURL as? String, senderName: senderName, node: node, readStatus: readStatus, reciever_id: reciever_id, audioUrl: audioUrl, audioDuration: audio_duration, videoUrl: videoUrl, videoDuration: videoDuration, urlName: urlName, completeUrl: completeUrl, urlDescription: urlDescription, fileUrl: fileUrl, fileType: fileType))
                    
                
                if self.messageManagerDelegate != nil{
                    self.messageManagerDelegate?.fireBaseManagerDidCompleteFetchingMessages!()
                }
            }
        })
    }

    
    func deleteOldChat(node: String) {
        print(node)
        let ref = msgRef.child(node)
        ref.removeValue()
        ref.removeAllObservers()


    }
    
    func deleteLiveOldChat(node: String) {
        print(node)
        let ref = liveRef.child(node)
        ref.removeValue()
        ref.removeAllObservers()


    }
    
    //Fetch the users whom we have contacted.
    func fetchRecentChats(){
//        LoaderClass.startLoader()
        let studentId = getStringValue(key: UserDefaultKeys.userId)
        self.recentsRef.queryOrdered(byChild: "from").queryEqual(toValue: "\(studentId)").observe(.childAdded, with: { (snapshot) -> Void in
            if let channelData = snapshot.value, !(channelData is NSNull){
                NotificationCenter.default.post(name: NSNotification.Name("fetchShowDataWithBlank"), object: nil)
                guard let channelData = snapshot.value as? Dictionary<String, Any> else {return}
                let node = channelData["node"] as? String ?? ""
                let ids = node.components(separatedBy: "-")
                let frndId = "\(studentId)" == ids.first ? ids.last! : ids.first!
                
                let myUnreadCount = channelData["\(studentId)_unread"] as? Int ?? 0
                let frndUnreadCount = channelData["\(frndId)_unread"] as? Int ?? 0
                
                let timeStamp = channelData["time"] as? String ?? ""
                let lastMsg = channelData["message"] as? String ?? ""
                let msgType = channelData["type"] as? String ?? ""
                let senderName = channelData["sender_name"] as? String ?? ""
                let senderId = channelData["from"] as? String ?? ""
                let senderImg = channelData["sender_image"] as? String ?? ""
                let recieverName = channelData["reciever_name"] as? String ?? ""
                let recieverId = channelData["reciever_id"] as? String ?? ""
                let recieverImg = channelData["reciever_image"] as? String ?? ""
                let myId = channelData["my_id"] as? String ?? ""
                let imgUrl = channelData["picture"] as? String ?? ""
                let device_token = channelData["device_token"] as? String ?? ""
                let fcm_token = channelData["fcm_token"] as? String ?? ""
                
                
                let time = self.convertTimeStampToDateString(timeStamp: (timeStamp as NSString).integerValue)
                
                if self.recentChats.count != 0{
                    for index in 0..<self.recentChats.count {
                        if node == self.recentChats[index].node{
                            self.isChannelPresent = true
                            break
                        }else{
                            self.isChannelPresent = false
                        }
                    }
                    if self.isChannelPresent == false{
                        self.recentChats.append(RecentChats(myUnreadCount: myUnreadCount, friendUnreadCount: frndUnreadCount, myId: myId, msgType: msgType, lastMsg: lastMsg, time: time, senderId: senderId, senderImg: senderImg, senderName: senderName, recieverId: recieverId, recieverImg: recieverImg, recieverName: recieverName, node: node, device_token: device_token, fcm_token: fcm_token, isBlocked: false))
                    }
                    if self.recentChatsManagerDelegate != nil{
                        self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                    }
                }else{
                    self.recentChats.append(RecentChats(myUnreadCount: myUnreadCount, friendUnreadCount: frndUnreadCount, myId: myId, msgType: msgType, lastMsg: lastMsg, time: time, senderId: senderId, senderImg: senderImg, senderName: senderName, recieverId: recieverId, recieverImg: recieverImg, recieverName: recieverName, node: node, device_token: device_token, fcm_token: fcm_token, isBlocked: false))
                    if self.recentChatsManagerDelegate != nil{
                        self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                    }
                }
                
                
            }
        })
       
        self.recentsRef.queryOrdered(byChild: "reciever_id").queryEqual(toValue: "\(studentId)").observe(.childAdded, with: { (snapshot) -> Void in
            if let channelData = snapshot.value, !(channelData is NSNull){
                
                NotificationCenter.default.post(name: NSNotification.Name("fetchShowDataWithBlank"), object: nil)
                
                guard let channelData = snapshot.value as? Dictionary<String, Any> else {return}
                let node = channelData["node"] as? String ?? ""
                let ids = node.components(separatedBy: "-")
                let frndId = "\(studentId)" == ids.first ? ids.last! : ids.first!
                
                let myUnreadCount = channelData["\(studentId)_unread"] as? Int ?? 0
                let frndUnreadCount = channelData["\(frndId)_unread"] as? Int ?? 0
                
                let timeStamp = channelData["time"] as? String ?? ""
                let lastMsg = channelData["message"] as? String ?? ""
                let msgType = channelData["type"] as? String ?? ""
                let senderName = channelData["sender_name"] as? String ?? ""
                let senderId = channelData["from"] as? String ?? ""
                let senderImg = channelData["sender_image"] as? String ?? ""
                let recieverName = channelData["reciever_name"] as? String ?? ""
                let recieverId = channelData["reciever_id"] as? String ?? ""
                let recieverImg = channelData["reciever_image"] as? String ?? ""
                let myId = channelData["my_id"] as? String ?? ""
                let imgUrl = channelData["picture"] as? String ?? ""
                let device_token = channelData["device_token"] as? String ?? ""
                let fcm_token = channelData["fcm_token"] as? String ?? ""
                let time = self.convertTimeStampToDateString(timeStamp: (timeStamp as NSString).integerValue)
                
                if self.recentChats.count != 0{
                    for index in 0..<self.recentChats.count {
                        if node == self.recentChats[index].node{
                            
                            self.isChannelPresent = true
                            break
                        }
                    }
                    if self.isChannelPresent == false{
                        self.recentChats.append(RecentChats(myUnreadCount: myUnreadCount, friendUnreadCount: frndUnreadCount,myId: myId, msgType: msgType, lastMsg: lastMsg, time: time, senderId: senderId, senderImg: senderImg, senderName: senderName, recieverId: recieverId, recieverImg: recieverImg, recieverName: recieverName, node: node, device_token: device_token, fcm_token: fcm_token, isBlocked: false))
                        if self.recentChatsManagerDelegate != nil{
                            self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                        }
                    }
                    
                }else{
                    self.recentChats.append(RecentChats(myUnreadCount: myUnreadCount, friendUnreadCount: frndUnreadCount,myId: myId, msgType: msgType, lastMsg: lastMsg, time: time, senderId: senderId, senderImg: senderImg, senderName: senderName, recieverId: recieverId, recieverImg: recieverImg, recieverName: recieverName, node: node, device_token: device_token, fcm_token: fcm_token, isBlocked: false))
                    if self.recentChatsManagerDelegate != nil{
                        self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                    }
                }
                
                if self.recentChatsManagerDelegate != nil{
                    self.recentChatsManagerDelegate?.fireBaseManagerDidCompleteFetchingRecentChats!()
                }
            }
        })
        
    }
    
    //MARK:- fetch Online, Offline, Typing.
    func observeOnlineStatus(userId: String, onCompletion: @escaping UserOnlineStatusCompletion) -> () {
        usersRef.child(userId).observe(.value, with: { (snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                if let messageData = snapshot.value as? Dictionary<String, Any> {
                    if let status = messageData["active_now"] as? String {
                        if status == "true" {
                            onCompletion(true, nil)
                        }else{
                            let time = (status as NSString).integerValue
                            onCompletion(nil, time)
                        }
                    }else{
                        onCompletion(nil, nil)
                    }
                }
            }
        })
        
    }
    
    func getUserDeviceToken(userId: String, onCompletion: @escaping onCompletion) -> () {
        driverRef.child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
            if let messageData = snapshot.value, !(messageData is NSNull) {
                if let messageData = snapshot.value as? Dictionary<String, Any>{
                    if let deviceToken = messageData["device_token"] as? String {
                        onCompletion(deviceToken)
                    }
                }
            }
        })
    }
    
    //Function to check whether a user is Blocked or not.
    func checkFriendIsBlocked(friendId: String, onCompletion: @escaping Completion) -> (){
        let myId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
        Database.database().reference().child("blocked").child("\(myId)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild(friendId){
                onCompletion(true)
            }else{
                onCompletion(false)
            }
        })
    }
    

    
    func blockUser(friendId: String){
        let myId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
        let time = Date().timeIntervalSince1970
        Database.database().reference().child("blocked").child("\(myId)").child(friendId).setValue(["time":"\(NSInteger(time))"])
    }
    
    func unblockUser(friendId: String){
        let myId = UserDefaults.standard.object(forKey: "StudentId") as? Int ?? 0
        Database.database().reference().child("blocked").child("\(myId)").child(friendId).removeValue()
    }
    
    //Convert timestamp to date string.
    func convertTimeStampToDateString(timeStamp: NSInteger) -> String{
        
        let time = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        //let calendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.long
        formatter.timeStyle = DateFormatter.Style.medium
        formatter.dateFormat = "hh:mm a"
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        let dateString = formatter.string(from: time)
        return dateString
       
    }
    
    
    
    //Mark : -Upload video on firebase
    
    func uploadFile(node: String, url: URL, fileType: FileType , onCompletion: @escaping onCompletion) -> () {
        let createdAt = NSDate().timeIntervalSince1970
        let riversRef = storageRef.child(node).child("\(NSInteger(createdAt)).\(fileType.fileExtension)")
        do {
            let metadata = StorageMetadata()
            metadata.contentType = fileType.contentType
            DispatchQueue.global().async(execute: {
                
                riversRef.putFile(from: url, metadata: metadata) { (_, error) in
                    if let err = error {
                        print("Failed to upload movie:", err)
                        return
                    }
                    
                    riversRef.downloadURL { (downloadUrl, error) in
                        if let err = error {
                            print("Failed to get download url:", err)
                            return
                        }
                        
                        guard let downloadUrl = downloadUrl else { return }
                        onCompletion(downloadUrl.absoluteString)
                    }
                    
                }
            })
        }catch{
            debugPrint("Error File URL - \(error.localizedDescription)")
        }
    }
}
