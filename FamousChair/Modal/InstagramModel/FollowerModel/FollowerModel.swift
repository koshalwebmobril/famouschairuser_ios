//
//  FollowerModel.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 29/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import SwiftyJSON
class FollowerModel: NSObject {
    let error : String?
    let message : String?
    var result = [FollowerData]()
    init(response:JSON) {
        self.error = response["status"].stringValue
        self.message = response["message"].stringValue
        for item in response["data"].arrayValue{
            let value = FollowerData.init(item: item)
            self.result.append(value)
        }
    }
}
class FollowerData :NSObject {
    var id :Int?
    var userId :Int?
    var name : String?
    var image : String?
    var login_user_id:Int?
    var following_status : Int?
    
    init(item:JSON) {
        self.id = item["id"].intValue
        self.userId = item["user_id"].intValue
        self.login_user_id = item["login_user_id"].intValue
        self.name = item["name"].stringValue
        self.image = item["profile_image"].stringValue
        self.following_status = item["following_status"].intValue
        
    }
}
