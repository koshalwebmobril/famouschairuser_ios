//
//  RecentChatCell.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 04/11/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class RecentChatCell: UITableViewCell {

    @IBOutlet weak var lastMsgLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
