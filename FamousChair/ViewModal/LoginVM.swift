//
//  LoginVM.swift
//  FamousChair
//
//  Created by ginger webs on 01/07/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKLoginKit
import Firebase
import GoogleSignIn

class LoginVM: NSObject {
  //  static let shared = LoginVM()
    var controller = LoginViewController()
    var constant = WebServiceApi()
    
    //MARKL: Validations
    
    func validations(email : String, password: String,controller:UIViewController) {
        if email == "" {
            controller.createAlert(title:"Famous Chair" , message: "Please enter email address")
            
        } else if !email.isValidEmail() {
            controller.createAlert(title:"Famous Chair" , message: "Please enter valid email address")
            
        } else if password == "" {
            controller.createAlert(title:"Famous Chair" , message: "Please enter Password")
        } else {
            let param = ["email" : email, "password" : password, "device_token" : getStringValue(key: UserDefaultKeys.fcmToken), "device_type" : "2","user_type":"2"]
            hitLoginApi(param: param,viewController:controller)
        }
    }
}

func setRoot() {
     UserDefaults.standard.setValue("true", forKey: "isLoggedIn")
    let tabBarController = homeStoryBoard.instantiateViewController(withIdentifier: "CustomTabBarController1") as! CustomTabBarController
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
     appdelegate.window!.rootViewController = tabBarController
}

extension LoginVM {
    
    func hitLoginApi(param : [String : String],viewController:UIViewController) {
        ApiManager.shared.postApiRequest(url: WebServiceApi.login, dict: param,viewController: viewController) { (response) in
          
            if let responseData = response {
                let loginData = try! JSONDecoder().decode(LoginModal.self, from: responseData)
                if loginData.status == "success" {
                    saveStringValue(value: "", key: "social")
                    saveStringValue(value: loginData.token ?? "", key: UserDefaultKeys.loginToken)
                    saveStringValue(value: "\(loginData.result?.id ?? 0)", key: UserDefaultKeys.userId)
                    saveStringValue(value: "\(loginData.result?.image ?? "")", key: UserDefaultKeys.userImage)
                    saveStringValue(value: "\(loginData.result?.name ?? "")", key: UserDefaultKeys.name)
                    setRoot()
                } else {
                    viewController.createAlert(title: "Famous Chair", message: loginData.message ?? "")
            }
        }
    }
}
    
    func socialLogin(param : [String : String],photoLink: String) {
        ApiManager.shared.postApiRequest(url: WebServiceApi.socialLogin, dict: param,viewController: controller) { (response) in
            if let responseData = response {
                let loginData = try! JSONDecoder().decode(LoginModal.self, from: responseData)
                if loginData.status == "success" {
                    saveStringValue(value: "social", key: "social")
                    saveStringValue(value: loginData.token ?? "", key: UserDefaultKeys.loginToken)
                    saveStringValue(value: "\(loginData.result?.id ?? 0)", key: UserDefaultKeys.userId)
                    saveStringValue(value: "\(loginData.result?.image ?? "")", key: UserDefaultKeys.userImage)
                    saveStringValue(value: "\(loginData.result?.name ?? "")", key: UserDefaultKeys.name)
                    setRoot()
                   
                } else {
                    self.controller.createAlert(title: "Famous Chair", message: loginData.message ?? "")

                }
                
            }
        }
    }
    
}
extension LoginVM {
    func facebookLogin() {
        let login = LoginManager()
        login.logOut()

        login.logIn(permissions: ["public_profile", "email"], from: controller) { (result, error) in
            
            if error != nil {
                self.controller.createAlert(title: "Famous Chair", message: error?.localizedDescription ?? "Something went wrong")


                print("Process error")
            } else if (result?.isCancelled)!{
                print("Cancelled")
            } else {
                LoaderClass.startLoader()
                

                self.getFBUserData()
                print("Logged in")
            }
        }
    }
    
    //function is fetching the user data
    func getFBUserData() {
        if((AccessToken.current) != nil) {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name,first_name, last_name, gender, picture.type(large), email"]).start(completionHandler: {(connection, result, error) -> Void in
                LoaderClass.stopLoader()
                
                if (error == nil) {
                    //                    LoaderClass.stopLoader()
                    print(result!)
                    
                    let dict = result as! [String : AnyObject]
                    print(dict)
                    
                    let deviceID = getStringValue(key: UserDefaultKeys.fcmToken)
                    
                    var firstName = ""
                    var lastName = ""
                    var socialId = ""
                    var email = ""
                    
                    if let fName = dict["first_name"] {
                        firstName = fName as! String
                    }
                    if let lName = dict["last_name"] {
                        lastName = lName as! String
                    }
                    if let id = dict["id"] {
                        socialId = id as! String
                    }
                    if let emailId = dict["email"] {
                        email = emailId as! String
                    }
                    
                    
                    
                    var photoLink = ""
                    if let picture = dict["picture"] as? [String : Any] {
                        if let picData = picture["data"] as? [String : Any] {
                            if let url = picData["url"] as? String {
                                photoLink = url
                            }
                        }
                    }
                    
                    let param = ["email" : email,
                             "name" : "\(firstName)" + " \(lastName)",
                    "user_type" : "2",
                    "social_token" : socialId,
                    "device_type" : "2",
                    "profile_image":photoLink,
                    "mobile":"",
                    "device_token" : getStringValue(key: UserDefaultKeys.fcmToken)] as [String : String]
                    
                    self.socialLogin(param: param, photoLink: photoLink)
                }
            })
        }
    }
}

extension LoginVM : GIDSignInDelegate {
    
    func googleLogin() {
        GIDSignIn.sharedInstance().delegate = self
        
        GIDSignIn.sharedInstance()?.presentingViewController = controller
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if let error = error {
            // ...
            self.controller.createAlert(title: "Famous Chair", message: "The user cancelled the sign-in flow.")
            
            return
        }
        
        let userId = user.userID  ?? ""            // For client-side use only!
        let idToken = user.authentication.idToken  ?? ""// Safe to send to the server
        let fullName = user.profile.name ?? ""
        let givenName = user.profile.givenName ?? ""
        let familyName = user.profile.familyName ?? ""
        let email = user.profile.email ?? ""
       
        var photoLink = ""
        
        if user.profile.hasImage {
            photoLink = user.profile.imageURL(withDimension: 100)!.absoluteString
        }
        
        print(userId, idToken, fullName, givenName, familyName,email,photoLink)
        
        
        let deviceID = getStringValue(key: UserDefaultKeys.fcmToken)
        //  let deviceID =  deviceToken
        
        let param = ["email" : email,
                     "name" : fullName,
                     "user_type" : "2",
                     "social_token" : idToken,
                     "device_type" : "2",
                     "profile_image":photoLink,
                     "mobile":"",
                     "device_token" : getStringValue(key: UserDefaultKeys.fcmToken)] as [String : String]
        
        self.socialLogin(param: param, photoLink: photoLink )
        
        
        // ...
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
        
        self.controller.createAlert(title: "Famous Chair", message: error.localizedDescription)
        
    }
    
}
