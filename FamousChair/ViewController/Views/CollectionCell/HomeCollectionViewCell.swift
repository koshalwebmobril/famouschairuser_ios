//
//  HomeCollectionViewCell.swift
//  FamousChair
//
//  Created by ginger webs on 06/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var category_name: UILabel!
    @IBOutlet weak var category_imageView: UIImageView!
}
