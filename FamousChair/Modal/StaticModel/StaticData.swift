//
//  StaticData.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 03/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import Foundation
struct StaticData :Codable {
    var content : String?
    
    enum Codingkeys:String, CodingKey{
        case content = "content"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.content = try values.decodeIfPresent(String.self, forKey: .content)
    }
}
