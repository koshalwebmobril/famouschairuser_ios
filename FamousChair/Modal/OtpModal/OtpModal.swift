

import Foundation
struct OtpModal : Codable {
	let status : String?
	let message : String?
	let result : otpData?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case result = "result"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		result = try values.decodeIfPresent(otpData.self, forKey: .result)
	}

}
