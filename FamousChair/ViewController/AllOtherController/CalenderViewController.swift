//
//  CalenderViewController.swift
//  FamousChair
//
//  Created by ginger webs on 10/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import FSCalendar
import SwiftyJSON

class CalenderViewController: UIViewController {
    
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var serviceNameLabel: UILabel!
    var dayLabel = ["Morning","Afternoon","Evening"]
    var serviceId = Int()
    var providerId = Int()
    var selectedDate = String()
    var selectedTime = String()
    var selectedTag = Int()
    var selectedIndex = IndexPath()
    var providerNameStr = String()
    
    var calenderData : CalenderModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        selectedDate = formatter.string(from: Date())
        
        let parameter = ["service_id":"\(serviceId)","availability_date":"\(selectedDate)"]
        ProviderVM.callServicesDetailsApi(params: parameter, viewController: self) { (responseObject) in
            self.calenderData = responseObject
            if self.calenderData?.service_availibility.count ?? 0 > 0{
                self.calenderData?.service_availibility[0].arr_morning = self.sortTime(data: self.calenderData?.service_availibility[0].arr_morning ?? [Time]())
                self.calenderData?.service_availibility[0].arr_afternoon = self.sortTime(data: self.calenderData?.service_availibility[0].arr_afternoon ?? [Time]())
                self.calenderData?.service_availibility[0].arr_evening = self.sortTime(data: self.calenderData?.service_availibility[0].arr_evening ?? [Time]())
                self.setUIData()
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }else{
                 self.createAlert(title: "Famous-Chair", message: "Booking time-slot is not available.")
            }
        }
    }
    
    func sortTime(data:[Time])->[Time]{
        var time_array = [String]()
        var data_array = [Time]()
        for (index,_) in data.enumerated(){
            let convertTime = timeConversion24(time12: data[index].time ?? "")
            time_array.append(convertTime)
        }
        
        time_array = time_array.sorted(by: <)
        for item in time_array{
            let time = Time(time: timeConversion12(time24: item))
            data_array.append(time)
        }
        
        return data_array
    }
    
    func setUIData(){
        serviceNameLabel.text = calenderData?.serviceData.service_name ?? ""
        serviceTitleLabel.text = providerNameStr
        priceLabel.text = String(format: "$%.2f and up 1 Hours", calenderData?.serviceData.price ?? 0.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    @IBAction func backButtonACtion(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func addServiceButtonAction(_ sender: Any) {
        let Vc = homeStoryBoard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    
}

extension CalenderViewController: UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegateFlowLayout {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dayLabel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalenderTableViewCell", for: indexPath) as!  CalenderTableViewCell
        cell.timeLabel.text = dayLabel[indexPath.row]
        if indexPath.row == 0{
            cell.timeCollectionView.tag = 100
            if calenderData?.service_availibility[0].arr_morning.count ?? 0 > 0{
                cell.messageLabel.isHidden = true
            }else{
                cell.messageLabel.isHidden = false
            }
        }else if indexPath.row == 1{
            cell.timeCollectionView.tag = 200
            if calenderData?.service_availibility[0].arr_afternoon.count ?? 0 > 0 {
                cell.messageLabel.isHidden = true
            }else{
                cell.messageLabel.isHidden = false
            }
        }else{
            cell.timeCollectionView.tag = 300
            if calenderData?.service_availibility[0].arr_evening.count ?? 0 > 0 {
                cell.messageLabel.isHidden = true
            }else{
                cell.messageLabel.isHidden = false
            }
        }
        cell.timeCollectionView.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if calenderData?.service_availibility.count ?? 0 > 0{
        if calenderData?.service_availibility[0].arr_morning.count ?? 0 > 0{
            return UITableView.automaticDimension
        }
        if calenderData?.service_availibility[0].arr_afternoon.count ?? 0 > 0 {
            return UITableView.automaticDimension
        }
        if calenderData?.service_availibility[0].arr_evening.count ?? 0 > 0 {
            return UITableView.automaticDimension
        }
        }
        return 0
    }
    
}

extension CalenderViewController: UICollectionViewDelegate, UICollectionViewDataSource ,FSCalendarDelegate {

func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    let selectDate  = formatter.string(from: date)
    let todayDate = formatter.string(from: Date())
    if date > Date() {
    
    selectedDate = selectDate
        let parameter = ["service_id":"\(serviceId)","availability_date":"\(selectedDate)"]
        ProviderVM.callServicesDetailsApi(params: parameter, viewController: self) { (responseObject) in
            self.calenderData = responseObject
            if self.calenderData?.service_availibility.count ?? 0 > 0{
                self.calenderData?.service_availibility[0].arr_morning = self.sortTime(data: self.calenderData?.service_availibility[0].arr_morning ?? [Time]())
                self.calenderData?.service_availibility[0].arr_afternoon = self.sortTime(data: self.calenderData?.service_availibility[0].arr_afternoon ?? [Time]())
                self.calenderData?.service_availibility[0].arr_evening = self.sortTime(data: self.calenderData?.service_availibility[0].arr_evening ?? [Time]())
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }else{
                self.tableView.isHidden = true
                self.createAlert(title: "Famous-Chair", message: "Booking time-slot is not available.")
            }
            
        }
    }else{
        if selectDate == todayDate{
            selectedDate = selectDate
            let parameter = ["service_id":"\(serviceId)","availability_date":"\(selectedDate)"]
            ProviderVM.callServicesDetailsApi(params: parameter, viewController: self) { (responseObject) in
                self.calenderData = responseObject
                if self.calenderData?.service_availibility.count ?? 0 > 0{
                    self.calenderData?.service_availibility[0].arr_morning = self.sortTime(data: self.calenderData?.service_availibility[0].arr_morning ?? [Time]())
                    self.calenderData?.service_availibility[0].arr_afternoon = self.sortTime(data: self.calenderData?.service_availibility[0].arr_afternoon ?? [Time]())
                    self.calenderData?.service_availibility[0].arr_evening = self.sortTime(data: self.calenderData?.service_availibility[0].arr_evening ?? [Time]())
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }else{
                    self.tableView.isHidden = true
                    self.createAlert(title: "Famous-Chair", message: "Booking time-slot is not available.")
                }
            }
        }else{
        self.createAlert(title: "Famous Chair", message: "You can't select past date or time")
        }
    }
}
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if calenderData?.service_availibility.count ?? 0 > 0{
        if collectionView.tag == 100{
            return calenderData?.service_availibility[0].arr_morning.count ?? 0
        }else if collectionView.tag == 200{
            return calenderData?.service_availibility[0].arr_afternoon.count ?? 0
        }else{
            return calenderData?.service_availibility[0].arr_evening.count ?? 0
        }
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var superCell = UICollectionViewCell()
                if collectionView.tag == 100{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalenderCollectionViewCell", for: indexPath) as! CalenderCollectionViewCell
                    cell.timeLabel.text = calenderData?.service_availibility[0].arr_morning[indexPath.row].time ?? ""
                    cell.timeLabel.backgroundColor = UIColor.lightGray
                    if selectedTag == collectionView.tag{
                        if selectedIndex == indexPath{
                            cell.timeLabel.backgroundColor = UIColor.systemBlue
                        }
                    }
                    superCell = cell
                }else if collectionView.tag == 200{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalenderCollectionViewCell", for: indexPath) as! CalenderCollectionViewCell
                    cell.timeLabel.text = calenderData?.service_availibility[0].arr_afternoon[indexPath.row].time ?? ""
                    cell.timeLabel.backgroundColor = UIColor.lightGray
                    if selectedTag == collectionView.tag{
                        if selectedIndex == indexPath{
                            cell.timeLabel.backgroundColor = UIColor.systemBlue
                        }
                    }
                    superCell = cell
                }else{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalenderCollectionViewCell", for: indexPath) as! CalenderCollectionViewCell
                    cell.timeLabel.text = calenderData?.service_availibility[0].arr_evening[indexPath.row].time ?? ""
                    cell.timeLabel.backgroundColor = UIColor.lightGray
                    if selectedTag == collectionView.tag{
                        if selectedIndex == indexPath{
                            cell.timeLabel.backgroundColor = UIColor.systemBlue
                        }
                    }
                    superCell = cell
                }
                return superCell

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedTag = collectionView.tag
        selectedIndex = indexPath
        if collectionView.tag == 100{
            selectedTime = calenderData?.service_availibility[0].arr_morning[indexPath.row].time ?? ""
        }else if collectionView.tag == 200{
            selectedTime = calenderData?.service_availibility[0].arr_afternoon[indexPath.row].time ?? ""
        }else{
            selectedTime = calenderData?.service_availibility[0].arr_evening[indexPath.row].time ?? ""
        }
        self.tableView.reloadData()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dates = formatter.string(from: Date())
        let arr = dates.components(separatedBy: " ")
        let currentDate = arr[0]
        let time = arr[1]
        
        if selectedDate == currentDate{
            if timeConversion24(time12: selectedTime) <= time {
                self.createAlert(title: "Famous Chair", message: "You can't select past date or time.")
            }else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                vc.priceStr = String(format: "%.2f", calenderData?.serviceData.price ?? 0.0)
                vc.serviceStr = serviceNameLabel.text ?? ""
                vc.dateStr = selectedDate
                vc.providerNameStr = self.providerNameStr
                vc.timeStr = selectedTime
                vc.providerId = self.providerId
                vc.serviceId = self.serviceId
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            vc.priceStr = String(format: "%.2f", calenderData?.serviceData.price ?? 0.0)
            vc.serviceStr = serviceNameLabel.text ?? ""
            vc.dateStr = selectedDate
            vc.providerNameStr = self.providerNameStr
            vc.timeStr = selectedTime
            vc.providerId = self.providerId
            vc.serviceId = self.serviceId
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    func timeConversion24(time12: String) -> String {
        
        var dateAsString = String()
        
        if time12.contains("AM") {
            dateAsString = time12.components(separatedBy: "AM")[0]
            dateAsString.append(" AM")
        }else{
            dateAsString = time12.components(separatedBy: "PM")[0]
            dateAsString.append(" PM")
        }
        let oldFormat = DateFormatter()
                oldFormat.dateFormat = "hh:mm a"
         
         let formatter = DateFormatter()
         formatter.dateFormat = "HH:mm"
         
        let date = oldFormat.date(from: dateAsString)
        if let date = date {
         dateAsString = formatter.string(from: date)
        }
        return dateAsString
    }
    

func timeConversion12(time24: String) -> String {
        
        var dateAsString = time24
        
//        if time12.contains("AM") {
//            dateAsString = time12.components(separatedBy: "AM")[0]
//            dateAsString.append(" AM")
//        }else{
//            dateAsString = time12.components(separatedBy: "PM")[0]
//            dateAsString.append(" PM")
//        }
        let oldFormat = DateFormatter()
                oldFormat.dateFormat = "HH:mm"
         
         let formatter = DateFormatter()
         formatter.dateFormat = "hh:mm a"
         
        let date = oldFormat.date(from: dateAsString)
        if let date = date {
         dateAsString = formatter.string(from: date)
        }
        return dateAsString
   
}
}
