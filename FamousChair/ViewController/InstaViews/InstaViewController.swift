//
//  InstaViewController.swift
//  FamousChair
//
//  Created by Sandeep Kumar on 02/10/2020.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import AVKit
import AgoraRtcKit
import SwiftyJSON
protocol RoleVCDelegate: NSObjectProtocol {
    func roleVC(_ vc: InstaViewController, didSelect role: AgoraClientRole)
}

class InstaViewController: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var lbl_topline: UILabel!
    @IBOutlet weak var btn_report_height: NSLayoutConstraint!
    @IBOutlet weak var btn_report: UIButton!
    @IBOutlet weak var btn_hidePost: UIButton!
    @IBOutlet weak var txtCharge: UITextField!
    @IBOutlet weak var addLabel: UILabel!
    @IBOutlet weak var priceTxtHieghtConstraint: NSLayoutConstraint!
    @IBOutlet weak var addHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var publicRadio: UIButton!
    @IBOutlet weak var privateRadio: UIButton!
    @IBOutlet weak var goliveView: UIView!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var darkView: UIView!
    @IBOutlet weak var tableView: UITableView!
    private let refreshControl = UIRefreshControl()
    
    var paused: Bool = false
    var aboutToBecomeInvisibleCell = -1
    var isPaid = Bool()
    var visibleIP : IndexPath?
    var hideIndexPath :IndexPath?
    var arr_data : InstaHomeModel?
    var liveStatus = ""
    var userListArray = [UserListLiveData]()
    let defaults = UserDefaults.standard
    
    //Agora Live chat.................
    private lazy var agoraKit: AgoraRtcEngineKit = {
        let engine = AgoraRtcEngineKit.sharedEngine(withAppId: KeyCenter.AppId, delegate: nil)
        engine.setLogFilter(AgoraLogFilter.info.rawValue)
        engine.setLogFile(FileCenter.logFilePath())
        return engine
    }()
    
    private var settings = Settings()
    weak var delegate: RoleVCDelegate?
    //  .........................................
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveDataItem(_:)), name:Notification.Name( "LiveUserRefersh"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(joinLiveChannel(_:)), name:Notification.Name( "joinLiveStreaming"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(likePostNotification(_:)), name:Notification.Name( "likePost"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(followPostNotification(_:)), name:Notification.Name( "followPost"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(commentPostNotification(_:)), name:Notification.Name( "commentPost"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(senderNotificationAction(_:)), name: .openChatWindow, object: nil)
        
        tableView.isUserInteractionEnabled = true
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refreshHomeData), for: .valueChanged)
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        self.settings.roomName = KeyCenter.ChannelName
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideDarkView))
        darkView.addGestureRecognizer(tapGesture)
    }
    
    @objc func senderNotificationAction(_ sender:Notification){
        let recentData = JSON(sender.userInfo!)
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        Vc.userName = recentData["username"].stringValue
        Vc.userProfilePic = recentData["image"].stringValue
        Vc.driverId = recentData["uid"].intValue
        Vc.driver_DeviceToken = recentData["fcm_token"].stringValue
        self.navigationController?.pushViewController(Vc, animated: true)
        
    }
    
    @objc func likePostNotification(_ sender:Notification){
        let data = JSON(sender.userInfo!)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewAllPostedController") as! ViewAllPostedController
        vc.userId = getStringValue(key: UserDefaultKeys.userId)
        vc.postId = data["redirect_id"].intValue
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func followPostNotification(_ sender:Notification){
        let data = JSON(sender.userInfo!)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        vc.str_UserId = data["redirect_id"].stringValue
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func commentPostNotification(_ sender:Notification){
        let data = JSON(sender.userInfo!)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CommentedListVC") as! CommentedListVC
        vc.postId = data["redirect_id"].stringValue
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        InstagramVM.callHomeDataApi(params: [String : Any](), viewController: self) { (responseObject) in
            self.arr_data = responseObject
            if self.arr_data?.arr_homeData.count ?? 0 > 0{
                self.messageLabel.isHidden = true
            }else{
                self.messageLabel.isHidden = false
            }
            self.tableView.isHidden = false
            
            InstagramVM.getLiveUserApi(param: [String : Any](), viewController: self) { (responseObject) in
                self.userListArray = responseObject.data ?? [UserListLiveData]()
                
                self.refreshControl.endRefreshing()
                self.tableView.reloadData()
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name("stopPlayer"), object: nil)
    }
    @IBAction func notificationAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        vc.isFromInstagram = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc private func refreshHomeData() {
        
        InstagramVM.callHomeDataApi(params: [String : Any](), viewController: self) { (responseObject) in
            self.arr_data = responseObject
            self.tableView.isHidden = false
            InstagramVM.getLiveUserApi(param: [String : Any](), viewController: self) { (responseObject) in
                self.userListArray = responseObject.data ?? [UserListLiveData]()
                self.refreshControl.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func onDidReceiveDataItem(_ notification:Notification) {
        InstagramVM.getLiveUserApi(param: [String : Any](), viewController: self) { (responseObject) in
            self.userListArray = responseObject.data ?? [UserListLiveData]()
            self.refreshControl.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    @objc func joinLiveChannel(_ notification:Notification) {
        let swiftyJson = JSON(notification.userInfo!)
        let settingsVC = SettingsViewController()
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "LiveRoomViewController") as! LiveRoomViewController
        objVC.dataSource = self
        settingsVC.delegate = self
        let tocken = swiftyJson["channel_token"].stringValue
        let channelName = swiftyJson["channel_name"].stringValue
        SessionManager.sharedInstance.str_ChannelName = channelName
        SessionManager.sharedInstance.str_tocken = tocken
        UserDefaults.standard.set(tocken, forKey: "liveTocken")
        UserDefaults.standard.set(channelName, forKey: "channelName")
        objVC.usersName = swiftyJson["name"].stringValue
        objVC.userImage = getStringValue(key: UserDefaultKeys.userImage)
        objVC.profileImg = swiftyJson["profile_image"].stringValue
        objVC.isComeFrom = "User"
        settingsVC.dataSource = self
        objVC.hidesBottomBarWhenPushed = true
        self.roleVC(self, didSelect: .audience)
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @objc func hideDarkView(){
        darkView.isHidden = true
        optionView.isHidden = true
        goliveView.isHidden = true
    }
    
    
    @objc func commentList(_ sender :UITapGestureRecognizer){
        let position = sender.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CommentedListVC") as! CommentedListVC
        vc.postId = "\(arr_data?.arr_homeData[indexPath?.row ?? 0].id ?? 0)"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func likeList(_ sender :UITapGestureRecognizer){
        let position = sender.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LikedUserListVC") as! LikedUserListVC
        vc.postId = "\(arr_data?.arr_homeData[indexPath?.row ?? 0].id ?? 0)"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func sharePost(_ sender :UITapGestureRecognizer){
        let code = "Famous Chair"
        
        let myWebsite = NSURL(string:"https://webmobril.org/dev/famous_chair")
        
        let appURL = "Click on link to download app : \(myWebsite!)"
        
        let shareStr = String(format: "%@ \n\n %@ \n\n",code,appURL)
        let shareAll = [shareStr] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)    }
    
    @objc func userProfileAction(_ sender :UITapGestureRecognizer){
        let position = sender.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        vc.str_UserId = "\(arr_data?.arr_homeData[indexPath?.row ?? 0].posted_userId ?? 0)"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func plusBtnAction(_ sender: Any) {
        darkView.isHidden = false
        goliveView.isHidden = false
    }
    
    
    @IBAction func searchAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InstaSearchViewController") as! InstaSearchViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func menuButtonAction(_ sender: UIButton) {
        
        darkView.isHidden = false
        optionView.isHidden = false
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        hideIndexPath = indexPath!
        if "\(arr_data?.arr_homeData[indexPath?.row ?? 0].posted_userId ?? 0)" == getStringValue(key: UserDefaultKeys.userId){
            btn_report.isHidden = true
            lbl_topline.isHidden = true
            btn_hidePost.setTitle("Delete Post", for: .normal)
            btn_hidePost.setImage(UIImage(named: "delete_icon"), for: .normal)
            btn_report_height.constant = -10
        }else{
            btn_report.isHidden = false
            lbl_topline.isHidden = false
            btn_hidePost.setTitle("Hide Post", for: .normal)
            btn_hidePost.setImage(UIImage(named: "Hide-post-icon2_100x100"), for: .normal)
            btn_report_height.constant = 50
        }
        
    }
    
    @IBAction func addNewPostAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewPostVC") as! AddNewPostVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func likeBtnAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        if arr_data?.arr_homeData[indexPath?.row ?? 0].user_like_status ?? 0 == 0{
            InstagramVM.callLikeApi(params: ["post_id":"\(arr_data?.arr_homeData[indexPath?.row ?? 0].id ?? 0)","like":"1"], viewController: self, completion: {self.arr_data?.arr_homeData[indexPath?.row ?? 0].user_like_status = 1
                self.arr_data?.arr_homeData[indexPath?.row ?? 0].total_like_count = (self.arr_data?.arr_homeData[indexPath?.row ?? 0].total_like_count ?? 0) + 1
                self.tableView.reloadData()
                
            })
        }else{
            InstagramVM.callUnLikeApi(params: ["post_id":"\(arr_data?.arr_homeData[indexPath?.row ?? 0].id ?? 0)"], viewController: self, completion: {self.arr_data?.arr_homeData[indexPath?.row ?? 0].user_like_status = 0
                self.arr_data?.arr_homeData[indexPath?.row ?? 0].total_like_count = (self.arr_data?.arr_homeData[indexPath?.row ?? 0].total_like_count ?? 0) - 1
                self.tableView.reloadData()
                
            })
        }
        
        
    }
    
    @IBAction func commentAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)
        let cell = sender.superview?.superview as! InstaTableCell
        cell.txtComment.resignFirstResponder()
        if cell.txtComment.text!.isEmpty{
            self.createAlert(title: "Famous Chair", message: "Enter your comment.")
            return
        }else{
            let message = encode(cell.txtComment.text ?? "")
            InstagramVM.callMakeCommentApi(params: ["post_id":"\(arr_data?.arr_homeData[indexPath?.row ?? 0].id ?? 0)","comment":message], viewController: self, completion: {status in
                if status{
                    self.arr_data?.arr_homeData[indexPath?.row ?? 0].comment_count = (self.arr_data?.arr_homeData[indexPath?.row ?? 0].comment_count ?? 0) + 1
                    cell.txtComment.text = ""
                    self.tableView.reloadData()
                }
            })
        }
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        
        let code = "Famous Chair"
        
        let myWebsite = NSURL(string:"https://webmobril.org/dev/famous_chair")
        
        let appURL = "Click on link to download app : \(myWebsite!)"
        
        let shareStr = String(format: "%@ \n\n %@ \n\n",code,appURL)
        let shareAll = [shareStr] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    @IBAction func privateAction(_ sender: Any) {
        publicRadio.isSelected = false
        privateRadio.isSelected = true
        priceTxtHieghtConstraint.constant = 30
        addHeightContraint.constant = 30
        txtCharge.isHidden = false
        addLabel.isHidden = false
        liveStatus = "private"
        
    }
    
    
    @IBAction func publicAction(_ sender: Any) {
        publicRadio.isSelected = true
        privateRadio.isSelected = false
        priceTxtHieghtConstraint.constant = 0.0
        addHeightContraint.constant = 0.0
        txtCharge.isHidden = true
        addLabel.isHidden = true
        liveStatus = "public"
        
        
    }
    
    @IBAction func chatAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecentChatVC") as! RecentChatVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func submitLiveAction(_ sender: Any) {
        
        if liveStatus == ""{
            self.createAlert(title: "Alert!", message: "Please select live status public or private.")
        }else{
            darkView.isHidden = true
            goliveView.isHidden = true
            InstagramVM.callGoliveApi(params: [String : Any](), type: self.liveStatus, viewController: self) { (boolValue) in
                let settingsVC = SettingsViewController()
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LiveRoomViewController") as! LiveRoomViewController
                vc.dataSource = self
                settingsVC.delegate = self
                settingsVC.dataSource = self
                vc.isComeFrom = "Self"
                vc.hidesBottomBarWhenPushed = true
                self.roleVC(self, didSelect: .broadcaster)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    @IBAction func reportPostAction(_ sender: UIButton) {
        InstagramVM.callReportPostApi(params: ["post_id":"\(arr_data?.arr_homeData[hideIndexPath?.row ?? 0].id ?? 0)"], viewController: self, completion: {response in
            if response{
                self.hideDarkView()
            }
        })
        
    }
    
    @IBAction func hidePostAction(_ sender: UIButton) {
        
        if sender.currentTitle ?? "" == "Delete Post"{
            InstagramVM.callDeletePostApi(params: ["post_id":"\(arr_data?.arr_homeData[hideIndexPath?.row ?? 0].id ?? 0)"], viewController: self, completion: {response in
                if response{
                    InstagramVM.callHomeDataApi(params: [String : Any](), viewController: self) { (responseObject) in
                        self.arr_data = responseObject
                        self.hideDarkView()
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    }
                }
            })
        }else{
            
            InstagramVM.callHidePostApi(params: ["post_id":"\(arr_data?.arr_homeData[hideIndexPath?.row ?? 0].id ?? 0)"], viewController: self, completion: {response in
                if response{
                    InstagramVM.callHomeDataApi(params: [String : Any](), viewController: self) { (responseObject) in
                        self.arr_data = responseObject
                        self.hideDarkView()
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    }
                }
            })
        }
    }
    
    func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    
}

extension InstaViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count ?? 0 == 200{
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userListArray.count + 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var superCell = UICollectionViewCell()
        if indexPath.row == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainProfileCell", for: indexPath) as! InstaCollectionViewCell
            ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(getStringValue(key: UserDefaultKeys.userImage))", imageView: cell.profileImage, placeHolder: "userImg")
            superCell = cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InstaCollectionViewCell", for: indexPath) as! InstaCollectionViewCell
            cell.nameLabel.text = userListArray[indexPath.row - 1].name ?? ""
            ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(userListArray[indexPath.row - 1].profile_img ?? "")", imageView: cell.userImage, placeHolder: "userImg")
            superCell = cell
        }
        return superCell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
            vc.str_UserId = getStringValue(key: UserDefaultKeys.userId)
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let settingsVC = SettingsViewController()
            let objVC = self.storyboard?.instantiateViewController(withIdentifier: "LiveRoomViewController") as! LiveRoomViewController
            objVC.dataSource = self
            settingsVC.delegate = self
            let tocken = userListArray[indexPath.row - 1].token
            let channelName = userListArray[indexPath.row - 1].channelname
            SessionManager.sharedInstance.str_ChannelName = userListArray[indexPath.row - 1].channelname ?? "Sandeep"
            SessionManager.sharedInstance.str_tocken = userListArray[indexPath.row - 1].token ?? "Sandeep"
            UserDefaults.standard.set(tocken, forKey: "liveTocken")
            UserDefaults.standard.set(channelName, forKey: "channelName")
            objVC.usersName = userListArray[indexPath.row - 1].name ?? ""
            objVC.userImage = getStringValue(key: UserDefaultKeys.userImage)//userListArray[indexPath.row - 1].profile_img ?? ""
            objVC.profileImg = userListArray[indexPath.row - 1].profile_img ?? ""
            objVC.postId = "\(userListArray[indexPath.row - 1].id ?? 0)"
            objVC.postUsrId = "\(userListArray[indexPath.row - 1].following_id ?? 0)"
            objVC.isComeFrom = "User"
            settingsVC.dataSource = self
            objVC.hidesBottomBarWhenPushed = true
            self.roleVC(self, didSelect: .audience)
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
}


extension InstaViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_data?.arr_homeData.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InstaTableCell", for: indexPath) as! InstaTableCell
        let tapCommentGesture = UITapGestureRecognizer(target: self, action: #selector(commentList(_:)))
        cell.commentLabel.addGestureRecognizer(tapCommentGesture)
        let tapLikeGesture = UITapGestureRecognizer(target: self, action: #selector(likeList(_:)))
        cell.likeLabel.addGestureRecognizer(tapLikeGesture)
        let tapShareGesture = UITapGestureRecognizer(target: self, action: #selector(sharePost(_:)))
        cell.likeLabel.addGestureRecognizer(tapShareGesture)
        cell.shareLabel.addGestureRecognizer(tapShareGesture)
        
        let tapProfileGesture = UITapGestureRecognizer(target: self, action: #selector(userProfileAction(_:)))
        cell.profileImage.addGestureRecognizer(tapProfileGesture)
        
        cell.commentLabel.text =  "\(arr_data?.arr_homeData[indexPath.row].comment_count ?? 0) Comment"
        cell.shareLabel.text =  "\(arr_data?.arr_homeData[indexPath.row].share_count ?? 0) Share"
        cell.likeLabel.text = "\(arr_data?.arr_homeData[indexPath.row].total_like_count ?? 0) Like"
        
        cell.lbl_post_title.text = decode(arr_data?.arr_homeData[indexPath.row].post_title ?? "") 
        
        if arr_data?.arr_homeData[indexPath.row].user_like_status ?? 0 == 0{
            cell.likeBtn.setImage(UIImage(named: "Like-icon_50x50"), for: .normal)
            
        }else{
            cell.likeBtn.setImage(UIImage(named: "like_icon"), for: .normal)
        }
        
        if arr_data?.arr_homeData[indexPath.row].thumbnail ?? "" == ""{
            cell.videoPlayerSuperView.isHidden = true
            cell.postImage.isHidden = false
            ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(arr_data?.arr_homeData[indexPath.row].post_path ?? "")"  , imageView: cell.postImage, placeHolder: "")
        }else{
            cell.videoPlayerSuperView.isHidden = false
            let videoUrl = arr_data?.arr_homeData[indexPath.row].post_path  ?? ""
            let videoUrlValue = WebServiceApi.baseImage_url + videoUrl
            cell.avPlayer = AVPlayer(url: URL(string: videoUrlValue)!)
            cell.avPlayerLayer = AVPlayerLayer(player: cell.avPlayer)
            cell.avPlayerLayer?.videoGravity = AVLayerVideoGravity(rawValue: AVLayerVideoGravity.resizeAspectFill.rawValue)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.playerItemDidReachEnd(notification:)),
                                                   name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                   object: cell.avPlayer?.currentItem)
            cell.avPlayer?.volume = 3
            //            cell.avPlayer?.pause()
            cell.avPlayer?.actionAtItemEnd = .none
            cell.avPlayerLayer?.frame = cell.videoPlayerSuperView.bounds
            cell.videoPlayerSuperView.layer.addSublayer((cell.avPlayerLayer)!)
        }
        
        cell.userName.text = arr_data?.arr_homeData[indexPath.row].name ?? ""
        cell.txtComment.delegate = self
        
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(arr_data?.arr_homeData[indexPath.row].profile_image ?? "")"  , imageView: cell.profileImage, placeHolder: "userImg")
        
        ApiManager.downloadProfileImage(url: "\(WebServiceApi.baseImage_url)\(getStringValue(key: UserDefaultKeys.userImage))"  , imageView: cell.userImage, placeHolder: "userImg")
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileCell") as! InstaTableCell
        cell.collectionView.reloadData()
        
        
        return cell
    }
    
    
    
    
    
    @objc func playerItemDidReachEnd(notification: Notification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: CMTime.zero)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let indexPaths = self.tableView.indexPathsForVisibleRows
        var cells = [Any]()
        for ip in indexPaths!{
            if let videoCell = self.tableView.cellForRow(at: ip) as? InstaTableCell{
                cells.append(videoCell)
            }
        }
        let cellCount = cells.count
        if cellCount == 0 {return}
        if cellCount == 1{
            print ("visible = \(indexPaths?[0])")
            if visibleIP != indexPaths?[0]{
                visibleIP = indexPaths?[0]
            }
            if let videoCell = cells.last! as? InstaTableCell{
                self.playVideoOnTheCell(cell: videoCell, indexPath: (indexPaths?.last)!)
            }
        }
        if cellCount >= 2 {
            for i in 0..<cellCount{
                let cellRect = self.tableView.rectForRow(at: (indexPaths?[i])!)
                let completelyVisible = self.tableView.bounds.contains(cellRect)
                let intersect = cellRect.intersection(self.tableView.bounds)
                
                let currentHeight = intersect.height
                print("\n \(currentHeight)")
                let cellHeight = (cells[i] as AnyObject).frame.size.height
                if currentHeight > (cellHeight * 0.95){
                    if visibleIP != indexPaths?[i]{
                        visibleIP = indexPaths?[i]
                        print ("visible = \(indexPaths?[i])")
                        if let videoCell = cells[i] as? InstaTableCell{
                            self.playVideoOnTheCell(cell: videoCell, indexPath: (indexPaths?[i])!)
                        }
                    }
                }
                else{
                    if aboutToBecomeInvisibleCell != indexPaths?[i].row{
                        aboutToBecomeInvisibleCell = (indexPaths?[i].row)!
                        if let videoCell = cells[i] as? InstaTableCell{
                            self.stopPlayBack(cell: videoCell, indexPath: (indexPaths?[i])!)
                        }
                        
                    }
                }
            }
        }
    }
    
    func checkVisibilityOfCell(cell : InstaTableCell, indexPath : IndexPath){
        let cellRect = self.tableView.rectForRow(at: indexPath)
        let completelyVisible = self.tableView.bounds.contains(cellRect)
        if completelyVisible {
            self.playVideoOnTheCell(cell: cell, indexPath: indexPath)
        }
        else{
            if aboutToBecomeInvisibleCell != indexPath.row{
                aboutToBecomeInvisibleCell = indexPath.row
                self.stopPlayBack(cell: cell, indexPath: indexPath)
            }
        }
    }
    
    func playVideoOnTheCell(cell : InstaTableCell, indexPath : IndexPath){
        cell.avPlayer?.play()
        //        cell.startPlayback()
        //        if isPaid == true {
        //            if arr_data?.arr_homeData[indexPath.row].thumbnail != "" {
        //                cell.startPlayback()
        //            } else {
        //                cell.stopPlayback()
        //            }
        //        }
    }
    
    func stopPlayBack(cell : InstaTableCell, indexPath : IndexPath){
        cell.avPlayer?.pause()
        //        cell.stopPlayback()
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("end = \(indexPath)")
        if let videoCell = cell as? InstaTableCell{
            //            videoCell.stopPlayback()
            videoCell.avPlayer?.pause()
        }
        
        paused = true
    }
    
    
    
    
    
}


extension InstaViewController: LiveVCDataSource {
    func liveVCNeedSettings() -> Settings {
        return settings
    }
    
    func liveVCNeedAgoraKit() -> AgoraRtcEngineKit {
        return agoraKit
    }
}

extension InstaViewController: SettingsVCDelegate {
    func settingsVC(_ vc: SettingsViewController, didSelect dimension: CGSize) {
        settings.dimension = dimension
    }
    
    func settingsVC(_ vc: SettingsViewController, didSelect frameRate: AgoraVideoFrameRate) {
        settings.frameRate = frameRate
    }
}

extension InstaViewController: SettingsVCDataSource {
    func settingsVCNeedSettings() -> Settings {
        return settings
    }
}

extension InstaViewController: RoleVCDelegate {
    func roleVC(_ vc: InstaViewController, didSelect role: AgoraClientRole) {
        settings.role = role
    }
    
    
}
