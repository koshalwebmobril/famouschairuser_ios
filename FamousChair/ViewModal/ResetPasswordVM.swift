//
//  LoginVM.swift
//  FamousChair
//
//  Created by ginger webs on 01/07/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit
import Alamofire

class ResetPasswordVM: NSObject {
  //  static let shared = ResetPasswordVM()
    var controller = ResetPasswordViewController()
    var constant = WebServiceApi()
    
    //MARKL: Validations
    
    func validationOnScreen(email: String, password: String, confirmPassword: String,isFromProfile:String) {
    if email  == "" {
        controller.createAlert(title: "Famous Chair", message: "Please enter email address")
            
        } else if !email.isValidEmail() {
        controller.createAlert(title: "Famous Chair", message: "Please enter valid email address")
           
        }  else if password  == "" {
        controller.createAlert(title: "Famous Chair", message: "Please enter Password")
            
        } else if password.count < 6 {
        controller.createAlert(title: "Famous Chair", message: "Password should be atleast 6 characters long.")
                  
        } else if confirmPassword == "" {
        controller.createAlert(title: "Famous Chair", message: "Please enter confirm password")
            
        }  else if password != confirmPassword {
        controller.createAlert(title: "Famous Chair", message: "New password and confirm password doesn't match")
            
        } else {
            let param = ["email" : email, "new_password" : password, "confirm_password" : confirmPassword]
        resetApi(param: param,isFrom:isFromProfile)
        }
    }

}


extension ResetPasswordVM {
    
    func resetApi(param : [String : String],isFrom:String) {
            ApiManager.shared.postApiRequest(url: WebServiceApi.resetPassword, dict: param,viewController:controller ) { (response) in
            if let responseData = response {
                let resetData = try! JSONDecoder().decode(resetModal.self, from: responseData)
                if resetData.status == "success" {
                    UserDefaults.standard.setValue("false", forKey: "isLoggedIn")
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let login = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    let navigationController = UINavigationController(rootViewController: login)
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    appdelegate.window!.rootViewController = navigationController
//                    if isFrom == ""{
//                        let Vc = mainStoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//                        self.controller.navigationController?.pushViewController(Vc, animated: true)
//                    }else{
//                        UserDefaults.standard.removeObject(forKey: "isLoggedIn")
//                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                        let login = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//                        let navigationController = UINavigationController(rootViewController: login)
//                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                        appdelegate.window!.rootViewController = navigationController
//                    }
                } else {
                    self.controller.createAlert(title: "Famous Chair", message: resetData.message ?? "")
                   
            }
        }
    }
}
}
