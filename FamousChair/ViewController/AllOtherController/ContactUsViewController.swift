//
//  ContactUsViewController.swift
//  FamousChair
//
//  Created by ginger webs on 11/06/20.
//  Copyright © 2020 ginger webs. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var txtSubject: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
     override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(true, animated: true)

     }
    

    @IBAction func submitAction(_ sender: Any) {
        ContactVM().validationOnScreen(subject: txtSubject.text ?? "", message: txtMessage.text ?? "", viewController: self)
    }
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

}
